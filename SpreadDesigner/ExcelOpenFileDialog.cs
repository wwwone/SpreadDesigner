﻿    using FarPoint.Excel;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Windows.Forms;

namespace SpreadDesigner
{
    internal class ExcelOpenFileDialog : FileDialogBase
    {
        private CheckedListComboBox comboBoxFlags;
        private OpenFileDialog openDialog;
        private ExcelOpenFileDialogNative openFileDialogNative;

        public ExcelOpenFileDialog()
        {
            this.AddControl();
            this.openDialog = new OpenFileDialog();
        }

        [DebuggerStepThrough, CompilerGenerated]
        private void onComboBoxFlags_CheckStateChanged(object sender, EventArgs args)
        {
            this.comboBoxFlags_CheckStateChanged(RuntimeHelpers.GetObjectValue(sender), (ItemCheckEventArgs) args);
        }

        private void AddControl()
        {
            IEnumerator enumerator = null;
            this.comboBoxFlags = new CheckedListComboBox();
            this.comboBoxFlags.DisplayText = common.rm.GetString("ExcelOpenFileDialog.comboBoxFlags.Text");
            List<CheckedListComboBoxItem> array = new List<CheckedListComboBoxItem>();
            try
            {
                enumerator = Enum.GetValues(typeof(ExcelOpenFlags)).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    ExcelOpenFlags flags = (ExcelOpenFlags)enumerator.Current;
                    if (flags != ExcelOpenFlags.NoFlagsSet)
                    {
                        CheckedListComboBoxItem item = new CheckedListComboBoxItem(flags.ToString(), false) {
                            Tag = flags
                        };
                        array.Add(item);
                    }
                }
            }
            finally
            {
                if (enumerator is IDisposable)
                {
                    (enumerator as IDisposable).Dispose();
                }
            }
            CheckedListComboBox.Sort(array);
            this.comboBoxFlags.Items.AddRange(array.ToArray());
            this.comboBoxFlags.CheckStateChanged += new ItemCheckEventHandler(this.comboBoxFlags_CheckStateChanged);
            base.ExtendedControl = this.comboBoxFlags;
        }

        private void comboBoxFlags_CheckStateChanged(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0)
            {
                if (e.NewValue == CheckState.Checked)
                {
                  this.comboBoxFlags.DisplayText = common.rm.GetString("ExcelOpenFileDialog.SelectAll");
                }
                else if (e.NewValue == CheckState.Unchecked)
                {
                  this.comboBoxFlags.DisplayText = common.rm.GetString("ExcelOpenFileDialog.comboBoxFlags.Text");
                }
                else if (e.NewValue == CheckState.Indeterminate)
                {
                  this.comboBoxFlags.DisplayText = common.rm.GetString("ExcelOpenFileDialog.SelectSome");
                }
            }
        }

        public override void Dispose()
        {
          this.comboBoxFlags.SelectedIndexChanged -= new EventHandler(this.onComboBoxFlags_CheckStateChanged);
          base.Dispose();
        }

        public override FileDialog Dialog
        {
            get
            {
                return this.openDialog;
            }
        }

        protected override FileDialogBase.FileDialogNative DialogNative
        {
            get
            {
                if (this.openFileDialogNative == null)
                {
                    this.openFileDialogNative = new ExcelOpenFileDialogNative(base.ExtendedControl, this.IsVistaDialog());
                }
                return this.openFileDialogNative;
            }
        }

        public ExcelOpenFlags OpenFlags
        {
            get
            {
                IEnumerator enumerator = null;
                ExcelOpenFlags noFlagsSet = ExcelOpenFlags.NoFlagsSet;
                try
                {
                    enumerator = this.comboBoxFlags.Items.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        CheckedListComboBoxItem current = (CheckedListComboBoxItem) enumerator.Current;
                        if ((current.CheckState == CheckState.Checked) && (current.Tag != null))
                        {
                            noFlagsSet = ((ExcelOpenFlags)current.Tag) | noFlagsSet;
                        }
                    }
                }
                finally
                {
                    if (enumerator is IDisposable)
                    {
                        (enumerator as IDisposable).Dispose();
                    }
                }
                return noFlagsSet;
            }
        }

        public class ExcelOpenFileDialogNative : FileDialogBase.FileDialogNative
        {
            private int addWidth;
            private IntPtr comboFileNameHandle;
            private IntPtr mComboExtensionsHandle;
            private int minimizeWidth;
            private IntPtr openButtonHandle;

            public ExcelOpenFileDialogNative(Control sourceControl, bool isVistaDialog) : base(sourceControl, isVistaDialog)
            {
            }

            protected override bool FileDialogEnumWindowCallBack(IntPtr hwnd, int lParam)
            {
                StringBuilder param = new StringBuilder(0x100);
                Win32.GetClassName(hwnd, param, param.Capacity);
                int dlgCtrlID = Win32.GetDlgCtrlID(hwnd);
                switch (((OpenDialogControlsID) dlgCtrlID))
                {
                    case OpenDialogControlsID.ButtonOpen:
                        this.openButtonHandle = hwnd;
                        if (base.controlToSize == null)
                        {
                            base.controlToSize = new NativeButton(this);
                        }
                        base.controlToSize.ReleaseHandle();
                        base.controlToSize.AssignHandle(hwnd);
                        return true;

                    case OpenDialogControlsID.ComboFileName:
                        if (param.ToString().ToLower() == "comboboxex32")
                        {
                            this.comboFileNameHandle = hwnd;
                        }
                        return true;

                    case OpenDialogControlsID.ComboFileType:
                        this.mComboExtensionsHandle = hwnd;
                        base.nextControlHandle = hwnd;
                        return true;
                }
                return true;
            }

            protected internal override void SizingAndMovingAddedControl()
            {
                if (base.sourceControl != null)
                {
                    RECT rect3 = new RECT();
                    RECT rect2 = new RECT();
                    RECT rect = new RECT();
                    Point point = new Point();
                    Win32.GetClientRect(base.openDialogHandle, ref rect3);
                    Win32.GetClientRect(this.comboFileNameHandle, ref rect2);
                    Win32.GetClientRect(this.openButtonHandle, ref rect);
                    PointStruct lpPoint = new PointStruct((int) rect2.left, (int) rect2.top);
                    PointStruct struct4 = new PointStruct((int) rect3.left, (int) rect3.top);
                    PointStruct struct2 = new PointStruct((int) rect.left, (int) rect.top);
                    Win32.ClientToScreen(base.openDialogHandle, ref struct4);
                    Win32.ClientToScreen(this.openButtonHandle, ref struct2);
                    Win32.ClientToScreen(this.comboFileNameHandle, ref lpPoint);
                    int width = (int) rect2.Width;
                    if (base.isVistaDialog)
                    {
                        point = new Point(lpPoint.x - struct4.x, struct2.y - struct4.y);
                    }
                    else
                    {
                        point = new Point((lpPoint.x - struct4.x) - 1, (lpPoint.y - struct4.y) + ((int) Math.Round((double) (2.5 * rect2.Height))));
                        width += 2;
                    }
                    if (rect.Height != base.sourceControl.Height)
                    {
                        if (base.sourceControl is ComboBox)
                        {
                            ComboBox sourceControl = (ComboBox) base.sourceControl;
                            sourceControl.ItemHeight = (int) Math.Round(Math.Floor((double) (((double) ((base.isVistaDialog ? ((int) rect.Height) : ((int) rect2.Height)) * sourceControl.ItemHeight)) / ((double) sourceControl.Height))));
                        }
                        else
                        {
                            base.sourceControl.Height = base.isVistaDialog ? ((int) rect.Height) : ((int) rect2.Height);
                        }
                    }
                    if (!point.Equals(base.sourceControl.Location))
                    {
                        base.sourceControl.Location = point;
                    }
                    if (width != base.sourceControl.Width)
                    {
                        base.sourceControl.Width = width;
                        this.addWidth = ((int) rect3.Width) - width;
                    }
                }
            }
        }
    }

    internal enum OpenDialogControlsID
    {
      ButtonCancel = 2,
      ButtonHelp = 0x40e,
      ButtonOpen = 1,
      CheckBoxReadOnly = 0x410,
      ComboFileName = 0x47c,
      ComboFileType = 0x470,
      ComboFolder = 0x471,
      DefaultView = 0x461,
      GroupFolder = 0x440,
      LabelFileName = 0x442,
      LabelFileType = 0x441,
      LabelLookIn = 0x443,
      LeftToolBar = 0x4a0
    }
}

