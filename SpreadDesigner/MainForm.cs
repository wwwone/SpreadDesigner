﻿using System;
using System.Drawing;
using C1.Win.C1Ribbon;
using FarPoint.Win.Spread;
using FarPoint.Win.Spread.Model;
using FarPoint.Win.Spread.CellType;
using System.Windows.Forms;
using FarPoint.Win.Spread.Chart;
using FarPoint.Win.Chart;
using System.Drawing.Drawing2D;
using FarPoint.Win.Spread.DrawingSpace;
using System.Collections;
using FarPoint.Win.Spread.Design;
using System.Globalization;
using FarPoint.Win.Spread.UndoRedo;
using System.Collections.Generic;
using FarPoint.Win;

//using FarPoint.Win.Spread.Design;

namespace SpreadDesigner
{
  public partial class SpreadDesigner : C1RibbonForm
  {

    public SpreadDesigner()
    {
      if (common.rm != null)
      {
        rm = common.rm;
      }
      else
      {
        common.LoadResourceManagerForStrings();
        rm = common.rm;
      }

      InitializeComponent();

      InitializeRightClick();

      InitializeLocalUI();

      InitializeGroupEditing();

      InitializeGroupFont();

      InitializeGroupClipBoard();

      InitializeGroupStyle();

      InitializeGroupText();

      InitializeGroupChart();

      InitializeGroupCellType();

      InitializeViewTab();

      InitializePageLayoutTab();

      InitializeDrawingToolsTab();

      InitializeChartToolsTab();

      InitializeSparklinesTab();

      InitializeQat();

      InitializeUndoRedoEvent();

      InitializeTableTab();

      SynUndoReDoState();
      InitTableContextMenu();//
      fpSpread1.ContextMenu = contextMenu1;
      fpSpread1.ActiveSheet.OperationMode = FarPoint.Win.Spread.OperationMode.Normal;
      fpSpread1.ActiveSheet.SelectionPolicy = FarPoint.Win.Spread.Model.SelectionPolicy.MultiRange;
      fpSpread1.SelectionBlockOptions = FarPoint.Win.Spread.SelectionBlockOptions.Cells | SelectionBlockOptions.Columns
          | SelectionBlockOptions.Rows | SelectionBlockOptions.Sheet;
      fpSpread1.GetActionMap().Put("ClearRange", new ClearRangeUndoAction());
      fpSpread1.GetInputMap(InputMapMode.WhenAncestorOfFocused).Put(new Keystroke(Keys.Delete, Keys.None), "ClearRange");

      #region Default
      rtDrawingTools.Visible = false;
      rtSparkline.Visible = false;
      rtTable.Visible = false;
      rtChartTools.Visible = false;
      nameBox1.Attach(fpSpread1);
      #endregion
      font_SelectionChanged();
      cellLock_selectionChange();
    }

    #region Private variables

    System.Resources.ResourceSet rm = null;

    private FarPoint.Win.Spread.DrawingSpace.PSObject psObjMenu = null;

    private FarPoint.Win.Spread.DrawingSpace.PSShape cellNoteObj;

    private int sheetTabContextMenu_Sheet;

    private ArrayList _pluginCellTypeSettings;

    private ArrayList _pluginAssemblyNames;

    private const string ASSEMBLY_NAME_ATTRIBUTE = "AssemblyName";
    private const string IPLUGIN_INTERFACE = "FarPoint.Win.Spread.CellType.IPluginCellTypeSetting";
    #endregion

    #region Context Menu

    void contextMenu1_Popup(object sender, EventArgs e)
    {
      if (fpSpread1.ActiveSheet == null) return;

      object selectedobjects = GetSelectedObjects(fpSpread1.ActiveSheet.GetSelection(0));
      if (HaveActiveCell() && selectedobjects is FarPoint.Win.Spread.Cell)
        SetupContextMenu(false, false, false);
      else if (selectedobjects is FarPoint.Win.Spread.SheetView)
        SetupContextMenu(true, true, false);
      else if (fpSpread1.ActiveSheet.RowCount > 0 && selectedobjects is FarPoint.Win.Spread.Row)
        SetupContextMenu(true, false, false);
      else if (fpSpread1.ActiveSheet.ColumnCount > 0 && selectedobjects is FarPoint.Win.Spread.Column)
        SetupContextMenu(false, true, false);
      else
      {
        for (int i = 0; i < contextMenu1.MenuItems.Count - 1; i++)
        {
          contextMenu1.MenuItems[i].Visible = false;
        }
      }

      isContextMenuSetup = false;
      menuContext_UnHide.Enabled = false;
      menuContext_UnHideSelection.Enabled = false;
      menuContext_Lock.Enabled = !(fpSpread1.ActiveSheet == null);

      if (selectedobjects is FarPoint.Win.Spread.Column || selectedobjects is FarPoint.Win.Spread.Row)
      {
        if (selectedobjects is FarPoint.Win.Spread.Column)
        {
          if (fpSpread1.ActiveSheet.ColumnHeader.RowCount <= 0)
          {
            menuContext_Headers.Visible = false;
          }
        }
        else if (selectedobjects is FarPoint.Win.Spread.Row)
        {
          if (fpSpread1.ActiveSheet.RowHeader.ColumnCount <= 0)
          {
            menuContext_Headers.Visible = false;
          }
        }

        if (menuContext_RowHeight.Visible == true)
        {
          for (int i = 0; i < fpSpread1.ActiveSheet.RowCount - 1; i++)
          {
            if (fpSpread1.ActiveSheet.Rows[i].Visible == false)
            {
              menuContext_UnHide.Enabled = true;
              break;
            }
          }
          foreach (CellRange range in fpSpread1.ActiveSheet.GetSelections())
          {
            for (int i = 0; i < range.RowCount; i++)
            {
              if (!fpSpread1.ActiveSheet.GetRowVisible(range.Row + i))
              {
                menuContext_UnHideSelection.Enabled = true;
                break;
              }
            }
          }
        }
        else
        {
          for (int i = 0; i < fpSpread1.ActiveSheet.ColumnCount - 1; i++)
          {
            if (fpSpread1.ActiveSheet.Columns[i].Visible == false)
            {
              menuContext_UnHide.Enabled = true;
              break;
            }
          }
          foreach (CellRange range in fpSpread1.ActiveSheet.GetSelections())
          {
            for (int i = 0; i < range.ColumnCount; i++)
            {
              if (!fpSpread1.ActiveSheet.GetColumnVisible(range.Column + i))
              {
                menuContext_UnHideSelection.Enabled = true;
                break;
              }
            }
          }
        }
      }
      else if (selectedobjects is FarPoint.Win.Spread.Cell)
      {
        if (sender is System.Windows.Forms.ContextMenu && fpSpread1.ActiveSheet != null)
        {
          if (string.IsNullOrEmpty(fpSpread1.ActiveSheet.ActiveCell.Note))
          {
            menuContext_Sep4.Visible = false;
            menuContext_DeleteNote.Visible = false;
            menuContext_EditNote.Visible = false;
          }
          else
          {
            menuContext_Sep4.Visible = true;
            menuContext_DeleteNote.Visible = true;
            if (fpSpread1.ActiveSheet.ActiveCell.NoteStyle == NoteStyle.PopupStickyNote)
            {
              menuContext_EditNote.Visible = true;
            }
            else
            {
              menuContext_EditNote.Visible = false;
            }
          }
        }
      }
      else
      {
        menuContext_Headers.Visible = false;
      }

      string stringCT = "";
      int j = 0;
      menuContext_ClearCellType.Enabled = true;
      for (j = 0; j < menuContext_Celltypes.MenuItems.Count - 1; j++)
      {
        menuContext_Celltypes.MenuItems[j].Checked = false;
      }
      menuContext_Button.Checked = false;
      menuContext_Checkbox.Checked = false;
      menuContext_Combobox.Checked = false;
      menuContext_Currency.Checked = false;
      menuContext_DateTime.Checked = false;
      menuContext_General.Checked = false;
      menuContext_Hyperlink.Checked = false;
      menuContext_Image.Checked = false;
      menuContext_Label.Checked = false;
      menuContext_Mask.Checked = false;
      menuContext_MultiOption.Checked = false;
      menuContext_Number.Checked = false;
      menuContext_Percent.Checked = false;
      menuContext_Progress.Checked = false;
      menuContext_Slider.Checked = false;
      menuContext_Text.Checked = false;
      menuContext_RichText.Checked = false;
      menuContext_RegEx.Checked = false;
      menuContext_ListBox.Checked = false;
      menuContext_MultiColumnComboBox.Checked = false;
      menuContext_ClearCellType.Enabled = true;
      menuContext_Barcode.Checked = false;
      if (!String.IsNullOrEmpty(stringCT))
      {
        switch (stringCT.ToLower())
        {
          case "barcodecelltype":
            menuContext_Barcode.Checked = true;
            break;
          case "colorpickercelltype":
            menuContext_ColorPicker.Checked = true;
            break;
          case "buttoncelltype":
            menuContext_Button.Checked = true;
            break;
          case "checkboxcelltype":
            menuContext_Checkbox.Checked = true;
            break;
          case "comboboxcelltype":
            menuContext_Combobox.Checked = true;
            break;
          case "currencycelltype":
            menuContext_Currency.Checked = true;
            break;
          case "datetimecelltype":
            menuContext_DateTime.Checked = true;
            break;
          case "generalcelltype":
            menuContext_General.Checked = true;
            break;
          case "hyperlinkcelltype":
            menuContext_Hyperlink.Checked = true;
            break;
          case "imagecelltype":
            menuContext_Image.Checked = true;
            break;
          case "labelcelltype":
            menuContext_Label.Checked = true;
            break;
          case "listboxcelltype":
            menuContext_ListBox.Checked = true;
            break;
          case "multicolumncomboboxcelltype":
            menuContext_MultiColumnComboBox.Checked = true;
            break;
          case "maskcelltype":
            menuContext_Mask.Checked = true;
            break;
          case "multioptioncelltype":
            menuContext_MultiOption.Checked = true;
            break;
          case "numbercelltype":
            menuContext_Number.Checked = true;
            break;
          case "percentcelltype":
            menuContext_Percent.Checked = true;
            break;
          case "progresscelltype":
            menuContext_Progress.Checked = true;
            break;
          case "slidercelltype":
            menuContext_Slider.Checked = true;
            break;
          case "textcelltype":
            menuContext_Text.Checked = true;
            break;
          case "richtextcelltype":
            menuContext_RichText.Checked = true;
            break;
          case "regularexpressioncelltype":
            menuContext_RegEx.Checked = true;
            break;
          case "heterogeneous_selection":
            menuContext_ClearCellType.Enabled = true;
            break;
          case "gcdatetimecelltype":
            menuContext_GcDate.Checked = true;
            break;
          case "gctextboxcelltype":
            menuContext_GcTextBox.Checked = true;
            break;
          case "":
            menuContext_ClearCellType.Enabled = false;
            break;
        }
      }
      else
      {
        menuContext_ClearCellType.Enabled = false;
      }

      if (string.IsNullOrEmpty(stringCT))
      {
        if (stringCT.IndexOf("InputMan.") >= 0)
        {
          string celltypeName = GetPluginCellTypeName(stringCT);
          for (j = 0; j < menuContext_Celltypes.MenuItems.Count - 1; j++)
          {
            MenuItem menuItem = menuContext_Celltypes.MenuItems[j];
            if (celltypeName.Equals(menuItem.Text))
            {
              menuItem.Checked = true;
            }
          }
        }
      }

      if (fpSpread1.ActiveSheet.ActiveCell.Sparkline != null)
        this.menuContext_Sparkline.Visible = true;
      else
        this.menuContext_Sparkline.Visible = false;
//
      SheetView sheetView = fpSpread1.ActiveSheet;

      TableView tableView = null;

      tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);

      if (tableView == null)
      {
        this.menuContext_Sep_Table1.Visible = false;
        this.menuContext_Table.Visible = false;

        this.menuContext_Sep_Table.Visible = false;
        this.menuContext_TableInsert.Visible = false;
        this.menuContext_TableDelete.Visible = false;
        this.menuContext_TableSelect.Visible = false;
      }
      else
      {
        this.menuContext_Sep_Table1.Visible = true;
        this.menuContext_Table.Visible = true;
        this.menuContext_Sep_Table.Visible = true;
        this.menuContext_TableInsert.Visible = true;
        
        this.menuContext_TableInsertRowToAbove.Enabled = true;
        this.menuContext_TableInsertRowToBelow.Enabled = true;
        this.menuContext_TableInsertColumnToLeft.Enabled = true;
        this.menuContext_TableInsertColumnToRight.Enabled = true;
        this.menuContext_TableDeleteRow.Enabled = true;
        this.menuContext_TableDeleteColumn.Enabled = true;
        
        int row = sheetView.ActiveRowIndex;
        int column = sheetView.ActiveColumnIndex;
        int adjustRow = 1;
        if (tableView.TotalRowVisible)
        {
          adjustRow += 1;
        }
        if (row == tableView.DataRange.Row + tableView.DataRange.RowCount - adjustRow)
        {
          this.menuContext_TableInsertRowToBelow.Visible = true;
        }
        else
        {
          this.menuContext_TableInsertRowToBelow.Visible = false;
        }

        if (column == tableView.DataRange.Column + tableView.DataRange.ColumnCount - 1)
        {
          this.menuContext_TableInsertColumnToRight.Visible = true;
        }
        else
        {
          this.menuContext_TableInsertColumnToRight.Visible = false;
        }
        this.menuContext_TableDelete.Visible = true;
        this.menuContext_TableSelect.Visible = true;
      }
      
      if ((tableView != null))
      {
        CellRange cellRange = sheetView.GetSelection(0);
        if ((tableView.DataRange.Intersects(cellRange.Row, cellRange.Column, cellRange.RowCount, cellRange.ColumnCount) && !tableView.DataRange.Contains(cellRange)))
        {
          
          bool isCanInsertAbove = false;
          isCanInsertAbove = cellRange.Column >= tableView.DataRange.Column && cellRange.Column + cellRange.ColumnCount <= tableView.DataRange.Column + tableView.DataRange.ColumnCount;
          isCanInsertAbove = isCanInsertAbove && ((tableView.HeaderRowVisible && cellRange.Row > tableView.DataRange.Row) | (!tableView.HeaderRowVisible && cellRange.Row >= tableView.DataRange.Row));
          this.menuContext_TableInsertRowToAbove.Enabled = isCanInsertAbove;
          this.menuContext_TableInsertRowToBelow.Enabled = false;

          bool isCanInsertLeft = false;
          isCanInsertLeft = cellRange.Row >= tableView.DataRange.Row && cellRange.Row + cellRange.Row <= tableView.DataRange.Row + tableView.DataRange.RowCount;
          isCanInsertLeft = isCanInsertLeft && cellRange.Column >= tableView.DataRange.Column;
          this.menuContext_TableInsertColumnToLeft.Enabled = isCanInsertLeft;

          this.menuContext_TableInsertColumnToRight.Enabled = false;
          this.menuContext_TableDeleteRow.Enabled = false;
          this.menuContext_TableDeleteColumn.Enabled = false;
          
          return;
        }
      }

      bool isDisableAllInsert = sheetView.GetSelections().Length > 1;
      if (isDisableAllInsert)
      {
        this.menuContext_TableInsertRowToAbove.Enabled = false;
        this.menuContext_TableInsertRowToBelow.Enabled = false;
        this.menuContext_TableInsertColumnToLeft.Enabled = false;
        this.menuContext_TableInsertColumnToRight.Enabled = false;
        this.menuContext_TableDeleteRow.Enabled = false;
        this.menuContext_TableDeleteColumn.Enabled = false;
        return;
      }
      else
      {
        if ((tableView != null))
        {
          CellRange cellRange = sheetView.GetSelection(0);
          //Begin delete table data row state
          bool needEnableTableDeleteRow = true;
          needEnableTableDeleteRow = (cellRange.Row == tableView.DataRange.Row && cellRange.RowCount == tableView.DataRange.RowCount);
          
          needEnableTableDeleteRow = needEnableTableDeleteRow | (tableView.HeaderRowVisible && tableView.TotalRowVisible && tableView.DataRange.Row < cellRange.Row && ((tableView.DataRange.RowCount > 3 && cellRange.RowCount == 1) | cellRange.RowCount > 1));
          needEnableTableDeleteRow = needEnableTableDeleteRow | (tableView.HeaderRowVisible && !tableView.TotalRowVisible && tableView.DataRange.Row < cellRange.Row && ((tableView.DataRange.RowCount > 2 && cellRange.RowCount == 1) | cellRange.RowCount > 1));
          
          needEnableTableDeleteRow = needEnableTableDeleteRow | (!tableView.HeaderRowVisible && tableView.TotalRowVisible && tableView.DataRange.Row <= cellRange.Row);
          needEnableTableDeleteRow = needEnableTableDeleteRow | (!tableView.HeaderRowVisible && !tableView.TotalRowVisible);
          needEnableTableDeleteRow = needEnableTableDeleteRow | (tableView.TotalRowVisible && cellRange.RowCount == 1 && cellRange.Row == tableView.DataRange.Row + tableView.DataRange.RowCount - 1);
          this.menuContext_TableDeleteRow.Enabled = needEnableTableDeleteRow;
        
          if ((cellRange.Row == tableView.DataRange.Row && tableView.HeaderRowVisible) | (cellRange.RowCount + cellRange.Row == tableView.DataRange.Row + tableView.DataRange.RowCount && tableView.TotalRowVisible))
          {
            this.menuContext_TableInsertRowToAbove.Enabled = false;
          }
          if ((tableView.HeaderRowVisible && cellRange.Row == tableView.DataRange.Row && ((!tableView.TotalRowVisible && cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount) | (tableView.TotalRowVisible && cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount - 1 && cellRange.RowCount == tableView.DataRange.RowCount - 1))))
          {
            this.menuContext_TableInsertRowToBelow.Enabled = false;
          }
        }
        
      }
//

      EditMenuEnable();
    }

    bool isContextMenuSetup = false;
    private void SetupContextMenu(bool RowHeader, bool ColumnHeader, bool SheetTab)
    {
      isContextMenuSetup = true;

      if (object.Equals(fpSpread1.ContextMenu, sheetTabContextMenu)) return;

      menuContext_SheetSettings.Visible = false;
      menuContext_SheetPrintOptions.Visible = false;
      menuContext_SheetSkinDesigner.Visible = false;
      menuContext_SpreadSkinDesigner.Visible = false;
      menuContext_Sep4.Visible = false;
      menuContext_EditNote.Visible = false;
      menuContext_DeleteNote.Visible = false;

      if (ColumnHeader == true && RowHeader == true)
      {
        menuContext_Celltypes.Visible = false;
        menuContext_Sep1.Visible = false;
        menuContext_Cut.Visible = false;
        menuContext_Copy.Visible = false;
        menuContext_Paste.Visible = false;
        menuContext_Sep2.Visible = false;
        menuContext_Insert.Visible = false;
        menuContext_Delete.Visible = false;
        menuContext_ColumnWidth.Visible = false;
        menuContext_RowHeight.Visible = false;
        menuContext_AutoFit.Visible = false;
        menuContext_Sep3.Visible = false;
        menuContext_Hide.Visible = false;
        menuContext_UnHide.Visible = false;
        menuContext_Span.Visible = false;
        menuContext_Borders.Visible = false;
        menuContext_Headers.Visible = false;
        menuContext_SheetSettings.Visible = true;
        menuContext_SheetPrintOptions.Visible = true;
        menuContext_SheetSkinDesigner.Visible = true;

        menuContext_SpreadSkinDesigner.Visible = true;
      }
      else if (ColumnHeader == true)
      {
        menuContext_Celltypes.Visible = true;
        menuContext_Sep1.Visible = true;
        menuContext_Cut.Visible = true;
        menuContext_Copy.Visible = true;
        menuContext_Paste.Visible = true;
        menuContext_Sep2.Visible = true;
        menuContext_Insert.Visible = true;
        menuContext_Delete.Visible = true;
        menuContext_Sep3.Visible = true;
        menuContext_ColumnWidth.Visible = true;
        menuContext_AutoFit.Visible = true;
        menuContext_RowHeight.Visible = false;
        menuContext_Hide.Visible = true;
        menuContext_UnHide.Visible = true;
        //  (Hide header editing function)
        menuContext_Headers.Visible = false;
        menuContext_Span.Visible = false;
        menuContext_Borders.Visible = false;
      }
      else if (RowHeader == true)
      {
        menuContext_Celltypes.Visible = true;
        menuContext_Sep1.Visible = true;
        menuContext_Cut.Visible = true;
        menuContext_Copy.Visible = true;
        menuContext_Paste.Visible = true;
        menuContext_Sep2.Visible = true;
        menuContext_Insert.Visible = true;
        menuContext_Delete.Visible = true;
        menuContext_Sep3.Visible = true;
        menuContext_ColumnWidth.Visible = false;
        menuContext_RowHeight.Visible = true;
        menuContext_AutoFit.Visible = true;
        menuContext_Hide.Visible = true;
        menuContext_UnHide.Visible = true;
        //  (Hide header editing function)
        menuContext_Headers.Visible = false;
        menuContext_Span.Visible = false;
        menuContext_Borders.Visible = false;
      }
      else
      {
        menuContext_Celltypes.Visible = true;
        menuContext_Sep1.Visible = true;
        menuContext_Cut.Visible = true;
        menuContext_Copy.Visible = true;
        menuContext_Paste.Visible = true;
        menuContext_Sep2.Visible = true;
        menuContext_Insert.Visible = false;
        menuContext_Delete.Visible = false;
        menuContext_Sep3.Visible = true;
        menuContext_ColumnWidth.Visible = false;
        menuContext_RowHeight.Visible = false;
        menuContext_AutoFit.Visible = false;
        menuContext_Hide.Visible = false;
        menuContext_UnHide.Visible = false;
        menuContext_Headers.Visible = false;
        menuContext_Span.Visible = true;
        menuContext_Borders.Visible = true;
      }

      if (fpSpread1.ActiveSheet != null && fpSpread1.ActiveSheet.ActiveCell != null && string.IsNullOrEmpty(fpSpread1.ActiveSheet.ActiveCell.Note) == false)
      {
        menuContext_Sep4.Visible = true;
        menuContext_DeleteNote.Visible = true;
        if (fpSpread1.ActiveSheet.ActiveCell.NoteStyle == NoteStyle.PopupStickyNote)
        {
          menuContext_EditNote.Visible = true;
        }
      }

    }

    private object GetSelectedObjects(FarPoint.Win.Spread.Model.CellRange range)
    {
      if (range == null) return null;

      if (range.Row == -1 && range.Column == -1 && range.RowCount == -1 && range.ColumnCount == -1)
      {
        return fpSpread1.ActiveSheet;
      }

      if (range.Row != -1 && range.RowCount != -1 && range.Column != -1 && range.ColumnCount != -1)
        return fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1];
      else if (range.Row == -1 && range.RowCount == -1 && range.Column == -1 && range.ColumnCount == -1)
        return fpSpread1.ActiveSheet;
      else if (range.Row == -1 && range.RowCount == -1)
        return fpSpread1.ActiveSheet.Columns[range.Column, range.Column + range.ColumnCount - 1];
      else if (range.Column == -1 && range.ColumnCount == -1)
        return fpSpread1.ActiveSheet.Rows[range.Row, range.Row + range.RowCount - 1];
      else
        return null;

    }

    private bool HaveActiveCell()
    {
      if (fpSpread1.ActiveSheet != null && fpSpread1.ActiveSheet.ActiveCell != null)
        return true;
      else
        return false;

    }

    private string GetPluginCellTypeName(string type)
    {
      string celltype = type.Substring(type.LastIndexOf(".") + 1);
      string celltypeName = "InputMan." + celltype.Substring(0, celltype.IndexOf("CellType"));
      return celltypeName;
    }

    private void EditMenuEnable()
    {
      object selectedobjects = GetSelectedObjects(fpSpread1.ActiveSheet.GetSelection(0));
      menuContext_PasteShape.Visible = false;
      menuContext_PasteChart.Visible = false;

      menuContext_Paste.Enabled = false;
      menuContext_PasteAll.Enabled = false;
      menuContext_PasteData.Enabled = false;
      menuContext_PasteFormatting.Enabled = false;
      menuContext_PasteFormulas.Enabled = false;
      menuContext_PasteAsLink.Enabled = false;
      menuContext_PasteAsString.Enabled = false;
      menuContext_Clear.Enabled = true;
      if (fpSpread1.ActiveSheet == null)
      {
        menuContext_Cut.Enabled = false;
        menuContext_Copy.Enabled = false;
        menuContext_Paste.Enabled = false;
        menuContext_Clear.Enabled = false;
        menuContext_SelectAll.Enabled = false;
      }
      else
      {
        menuContext_SelectAll.Enabled = true;
      }

      if (Clipboard.GetDataObject().GetDataPresent("FarPoint.Win.Spread.ShapeClipboardObject"))
      {
        menuContext_Paste.Enabled = true;
        menuContext_PasteShape.Visible = true;
      }

      if (fpSpread1.ActiveSheet.RowCount == 0 || fpSpread1.ActiveSheet.ColumnCount == 0)
      {
        menuContext_Clear.Enabled = false;
      }

      if (selectedobjects is FarPoint.Win.Spread.Cell || selectedobjects is FarPoint.Win.Spread.Row || selectedobjects is FarPoint.Win.Spread.Column)
      {
        menuContext_Cut.Enabled = true;
        menuContext_Copy.Enabled = true;
        if (Clipboard.GetDataObject() == null)
        {
          menuContext_Paste.Enabled = false;
        }
        else if (Clipboard.GetDataObject().GetDataPresent(typeof(CellInfoRange)))
        {
          menuContext_PasteAll.Enabled = true;
          menuContext_PasteData.Enabled = true;
          menuContext_PasteFormatting.Enabled = true;
          menuContext_PasteFormulas.Enabled = true;
          menuContext_PasteAsLink.Enabled = true;
          menuContext_PasteAsString.Enabled = true;
        }
        else if (Clipboard.GetDataObject().GetDataPresent(typeof(string)))
        {
          menuContext_Paste.Enabled = true;
          menuContext_PasteAll.Enabled = false;
          menuContext_PasteData.Enabled = true;
          menuContext_PasteFormatting.Enabled = false;
          menuContext_PasteFormulas.Enabled = false;
          menuContext_PasteAsLink.Enabled = false;
          menuContext_PasteAsString.Enabled = false;
        }
        else if (Clipboard.GetDataObject().GetDataPresent("FarPoint.Win.Spread.ShapeClipboardObject"))
        {
          menuContext_Paste.Enabled = true;
          menuContext_PasteShape.Visible = true;
        }
        else if (Clipboard.GetDataObject().GetDataPresent("FarPoint.Win.Spread.Chart.SpreadChartClipboardInfo"))
        {
          menuContext_Paste.Enabled = true;
          menuContext_PasteChart.Visible = true;
        }
      }
      else
      {
        menuContext_Cut.Enabled = false;
        menuContext_Copy.Enabled = false;
        menuContext_Paste.Enabled = false;
      }

    }
  

    void menuContext_Mask_Click(object sender, EventArgs e)
    {
      ItemMask_Click(sender, e);
      

    }

    void menuContext_Number_Click(object sender, EventArgs e)
    {
      
      ItemNumber_Click(sender, e);
    }

    void menuContext_Percent_Click(object sender, EventArgs e)
    {
      
      ItemPercent_Click(sender, e);
    }

    void menuContext_MultiOption_Click(object sender, EventArgs e)
    {
      
      ItemMultiOption_Click(sender, e);
    }

    void menuContext_Progress_Click(object sender, EventArgs e)
    {
      
      ItemProgress_Click(sender, e);
    }

    void menuContext_Slider_Click(object sender, EventArgs e)
    {
      
      ItemSlider_Click(sender, e);
    }

    void menuContext_Label_Click(object sender, EventArgs e)
    {
      
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.LabelCellType);
      rbClearCellType.Enabled = true;
      rcbCellType.Text = "Label";
    }

    void menuContext_Hyperlink_Click(object sender, EventArgs e)
    {
      
      ItemHyperLink_Click(sender, e);
    }

    void menuContext_Text_Click(object sender, EventArgs e)
    {
      
      ItemText_Click(sender, e);
    }

    void menuContext_Image_Click(object sender, EventArgs e)
    {
      
      ItemImage_Click(sender, e);
    }

    void menuContext_General_Click(object sender, EventArgs e)
    {
      
      ItemGeneral_Click(sender, e);
    }

    void menuContext_GcTextBox_Click(object sender, EventArgs e)
    {
      ItemGcTextBox_Click(sender, e);
    }

    void menuContext_GcDate_Click(object sender, EventArgs e)
    {
      ItemGcDate_Click(sender, e);
    }

    void menuContext_Copy_Click(object sender, EventArgs e)
    {
      
      btnCopy_Click(sender, e);
    }

    void menuContext_Paste_Click(object sender, EventArgs e)
    {
      
      btnPaste_Click(sender, e);
    }

    void menuContext_PasteAll_Click(object sender, EventArgs e)
    {
      
      rbPasteAll_Click(sender, e);
    }

    void menuContext_PasteData_Click(object sender, EventArgs e)
    {
      
      rbPasteValues_Click(sender, e);
    }

    void menuContext_PasteFormatting_Click(object sender, EventArgs e)
    {
      
      rbPasteFormatting_Click(sender, e);
    }

    void menuContext_Cut_Click(object sender, EventArgs e)
    {
      
      btnCut_Click(sender, e);
    }

    void menuContext_DateTime_Click(object sender, EventArgs e)
    {
      
      ItemDataTime_Click(sender, e);
    }

    void menuContext_PasteAsLink_Click(object sender, EventArgs e)
    {
      
      rbPasteAsLink_Click(sender, e);
    }

    void menuContext_PasteAsString_Click(object sender, EventArgs e)
    {
      
      rbPasteAsString_Click(sender, e);
    }

    void menuContext_Clear_Click(object sender, EventArgs e)
    {
      
      btnClearAll_Click(sender, e);
    }

    void menuContext_SelectAllSheet_Click(object sender, EventArgs e)
    {
      
      btnSheets_Click(sender, e);
    }

    void menuContext_SelectAllCells_Click(object sender, EventArgs e)
    {
      
      btnCells_Click(sender, e);
    }

    void menuContext_SelectAllData_Click(object sender, EventArgs e)
    {
      
      btnData_Click(sender, e);
    }
    void menuContext_SparklineEditGroup_Click(object sender, EventArgs e)
    {
      EditGroupLocationData_Click(sender, e);
    }

    void menuContext_PasteFormulas_Click(object sender, EventArgs e)
    {
      
      rbPasteFormulas_Click(sender, e);
    }

    void menuContext_SparklineEditSingle_Click(object sender, EventArgs e)
    {
      
      EditSingleSparklineData_Click(sender, e);
    }

    void menuContext_Currency_Click(object sender, EventArgs e)
    {
      
      ItemCurrency_Click(sender, e);
    }

    void menuContext_Combobox_Click(object sender, EventArgs e)
    {
      
      ItemComboBox_Click(sender, e);
    }

    void menuContext_SparklineUngroup_Click(object sender, EventArgs e)
    {
      
      unGroup_Click(sender, e);
    }

    void menuContext_SparklineClear_Click(object sender, EventArgs e)
    {
      
      ClearSelectedSparkline_Click(sender, e);
    }

    void menuContext_SparklineGroup_Click(object sender, EventArgs e)
    {
      
      Group_Click(sender, e);
    }

    void menuContext_SparklineClearGroup_Click(object sender, EventArgs e)
    {
      
      clearSparklineGroups_Click(sender, e);
    }

    void menuContext_Borders_Click(object sender, EventArgs e)
    {
      
      FarPoint.Win.Spread.Design.ExternalDialogs.BorderEditor(this.fpSpread1);
    }

    void menuContext_ColumnWidth_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
      input.TextInputInputBox.Top = 30;
      input.TextInputInputBox.Left = 10;
      input.TextInputInputBox.Width = 120;
      input.Text = "Column width";
      input.TextInputPrompt.Text = "Column width：";
      input.NumbersOnly = true;
      input.Icon = this.Icon;
      float columnWidth = 0f;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
     

      input.TextInputInputBox.Text = sheet.GetColumnWidth(cellRanges[0].Column).ToString();
      if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && float.TryParse(input.TextInputInputBox.Text, out columnWidth))
      {
        foreach (CellRange range in this.fpSpread1.ActiveSheet.GetSelections())
        {
          this.fpSpread1.ActiveSheet.Columns[range.Column, range.Column + range.ColumnCount - 1].Width = columnWidth;
        }
      }
    }


    void menuContext_RowHeight_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
      input.TextInputInputBox.Top = 30;
      input.TextInputInputBox.Left = 10;
      input.TextInputInputBox.Width = 120;
      input.Text = "The width of the line";
      input.TextInputPrompt.Text = "The width of the line：";
      input.NumbersOnly = true;
      input.Icon = this.Icon;
      float rowHeight = 0f;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      input.TextInputInputBox.Text = sheet.GetRowHeight(cellRanges[0].Row).ToString();
      if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && float.TryParse(input.TextInputInputBox.Text, out rowHeight))
      {
        foreach (CellRange range in this.fpSpread1.ActiveSheet.GetSelections())
        {
          this.fpSpread1.ActiveSheet.Rows[range.Row, range.Row + range.RowCount - 1].Height = rowHeight;
        }
      }
    }

    void menuContext_Hide_Click(object sender, EventArgs e)
    {
      
      SheetView sheet = this.fpSpread1.ActiveSheet;

      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
      {
        if (range.Row == 0)
          sheet.Columns[range.Column, range.Column + range.ColumnCount - 1].Visible = false;
        else if (range.Column == 0)
          sheet.Rows[range.Row, range.Row + range.RowCount - 1].Visible = false;
      }
    }

    void menuContext_UnHideSelection_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges[0].Column == -1)
      {
        sheet.Rows[cellRanges[0].Row, cellRanges[0].Row + cellRanges[0].RowCount - 1].Visible = true;
      }
      else if (cellRanges[0].Row == -1)
      {
        sheet.Columns[cellRanges[0].Column, cellRanges[0].Column + cellRanges[0].ColumnCount - 1].Visible = true;
      }
    }

    void menuContext_UnHideAllHidden_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      for (int i = 0; i < sheet.RowCount; i++)
      {
        if (sheet.Rows[i].Visible == false) sheet.Rows[i].Visible = true;
      }
      for (int j = 0; j < sheet.ColumnCount; j++)
      {
        if (sheet.Columns[j].Visible == false) sheet.Columns[j].Visible = true;
      }
    }

    void menuContext_UnHideSpecific_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();

      FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
      input.TextInputInputBox.Top = 30;
      input.TextInputInputBox.Left = 10;
      input.TextInputInputBox.Width = 120;
      if (cellRanges[0].Column == -1)
      {
        input.Text = "Show Hidden specified line";
        input.TextInputPrompt.Text = "Please enter the line number：";
        input.TextInputInputBox.Text = FarPoint.Win.Spread.Design.common.ConvertIndexToLetters(fpSpread1.ActiveSheet.ActiveColumnIndex) + (fpSpread1.ActiveSheet.ActiveRowIndex + 1).ToString();
        input.NumbersOnly = false;
        input.Icon = this.Icon;
        if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
        {
          string refstring;
          refstring = input.TextInputInputBox.Text;

          int numVal = Convert.ToInt32(refstring);
          sheet.Rows[numVal - 1].Visible = true;

        }
      }
      else if (cellRanges[0].Row == -1)
      {
        input.Text = "Show Hidden specified column";
        input.TextInputPrompt.Text = "Please enter the column number：";
        input.TextInputInputBox.Text = FarPoint.Win.Spread.Design.common.ConvertIndexToLetters(fpSpread1.ActiveSheet.ActiveColumnIndex) + (fpSpread1.ActiveSheet.ActiveRowIndex + 1).ToString();
        input.NumbersOnly = false;
        input.Icon = this.Icon;
        if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
        {
          string refstring;
          refstring = input.TextInputInputBox.Text;

          int numVal = Convert.ToInt32(refstring);
          sheet.Columns[numVal - 1].Visible = true;

        }
      }
    }

    void menuContext_ClearCellType_Click(object sender, EventArgs e)
    {
      
      btnClearCellType_Click(sender, e);
    }
    void menuContext_Headers_Click(object sender, EventArgs e)
    {
      HeaderEditForm frmHeader = new HeaderEditForm(fpSpread1, false);
      frmHeader.ShowDialog();
    }

    void menuContext_Checkbox_Click(object sender, EventArgs e)
    {
      
      ItemCheckBox_Click(sender, e);
    }

    void menuContext_Delete_Click(object sender, EventArgs e)
    {
      
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges.Length == 1)
      {
        FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
        input.TextInputInputBox.Top = 30;
        input.TextInputInputBox.Left = 10;
        input.TextInputInputBox.Width = 320;
        input.NumbersOnly = true;
        input.Icon = this.Icon;
        //Delete Row
        if (cellRanges[0].Column == -1)
        {
          input.Text = "Delete Row";
          input.TextInputPrompt.Text = "Please enter the number of rows to be deleted";
          input.TextInputInputBox.Text = cellRanges[0].RowCount.ToString();
          int rowCount;
          if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && int.TryParse(input.TextInputInputBox.Text, out rowCount))
          {
            sheet.Rows.Remove(cellRanges[0].Row, rowCount);
          }

        }
        //Delete Column
        else if (cellRanges[0].Row == -1)
        {
          input.Text = "Remove Columns";
          input.TextInputPrompt.Text = "Please enter the number of columns you want to delete";
          input.TextInputInputBox.Text = cellRanges[0].ColumnCount.ToString();
          int columnCount;
          if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && int.TryParse(input.TextInputInputBox.Text, out columnCount))
          {
            sheet.Columns.Remove(cellRanges[0].Column, columnCount);
          }
        }
      }
      else
      {
        MessageBox.Show("The action you have chosen does not support multiple selections.\r\n please select a single range and try again.");

      }
    }

    void menuContext_Lock_Click(object sender, EventArgs e)
    {
      
      btnLock_Click(sender, e);
    }

    void menuContext_Span_Click(object sender, EventArgs e)
    {
      
      btnMerge_Click(sender, e);
      rbMerge.Pressed = !rbMerge.Pressed;
    }

    void menuContext_Insert_Click(object sender, EventArgs e)
    {
      
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges.Length == 1)
      {
        FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
        input.TextInputInputBox.Top = 30;
        input.TextInputInputBox.Left = 10;
        input.TextInputInputBox.Width = 320;
        input.NumbersOnly = true;
        input.Icon = this.Icon;
        if (cellRanges[0].Column == -1)
        {
          input.Text = "Insert Row";
          input.TextInputPrompt.Text = "Please enter the number of rows to be inserted";
          input.TextInputInputBox.Text = cellRanges[0].RowCount.ToString();
          int rowCount;
          if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && int.TryParse(input.TextInputInputBox.Text, out rowCount))
          {
            sheet.Rows.Add(cellRanges[0].Row, rowCount);
          }
        }
        else if (cellRanges[0].Row == -1)
        {
          input.Text = "Insert Column";
          input.TextInputPrompt.Text = "Please enter the number of columns to be inserted";
          input.TextInputInputBox.Text = cellRanges[0].ColumnCount.ToString();
          int columnCount;
          if (input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK && int.TryParse(input.TextInputInputBox.Text, out columnCount))
          {
            sheet.Columns.Add(cellRanges[0].Column, columnCount);
          }
        }
      }
      else
      {
        MessageBox.Show("The action you have chosen does not support multiple selections.\r\n please select a single range and try again.");
      }
    }

    void menuContext_SparklineSwitch_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ISparklineGroup spGroup = null;
      FarPoint.Win.Spread.ISparkline spLine = null;
      if (fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex, out spGroup, out spLine))
      {
        ExcelSparklineGroup group = (spGroup as ExcelSparklineGroup);
        if (group != null && group.IsSwitchable())
        {
          group.SwitchRowColumn();
        }
      }
    }

    void menuContext_ListBox_Click(object sender, EventArgs e)
    {
      ItemListBox_Click(sender, e);
    }


    void menuContext_ColorPicker_Click(object sender, EventArgs e)
    {
      ItemColorPicker_Click(sender, e);
    }

    void menuContext_Barcode_Click(object sender, EventArgs e)
    {
      ItemBarcode_Click(sender, e);
    }

    void menuContext_MultiColumnComboBox_Click(object sender, EventArgs e)
    {
      ItemMultiColumnComboBox_Click(sender, e);
    }

    void menuContext_Button_Click(object sender, EventArgs e)
    {
      
      ItemButton_Click(sender, e);
    }

    void menuContext_SheetSettings_Click(object sender, EventArgs e)
    {
      
      btnGeneralChart1_Click(sender, e);
    }

    void menuContext_SheetSkinDesigner_Click(object sender, EventArgs e)
    {
      
      btnSheetSkins1_Click(sender, e);
    }

    void menuContext_SpreadSkinDesigner_Click(object sender, EventArgs e)
    {
      
      btnSpreadSkins1_Click(sender, e);
    }

    void menuContext_SheetPrintOptions_Click(object sender, EventArgs e)
    {
      
      PrintOptionsDlg dlgPrint = new PrintOptionsDlg(fpSpread1);
      dlgPrint.ShowDialog();
    }


    void menuContext_AutoFit_Click(object sender, EventArgs e)
    {
      
      //(sender, e);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges[0].Row == -1)
      {
        foreach (CellRange range in cellRanges)
        {
          for (int i = 0; i < range.ColumnCount; i++)
          {
            int columnIndex = range.Column + i;
            float width = sheet.GetPreferredColumnWidth(columnIndex);
            sheet.Columns[columnIndex].Width = width;
          }
        }
      }
      else if (cellRanges[0].Column == -1)
      {
        foreach (CellRange range in cellRanges)
        {
          for (int i = 0; i < range.RowCount; i++)
          {
            int rowIndex = range.Row + i;
            float height = sheet.GetPreferredRowHeight(rowIndex);
            sheet.Rows[rowIndex].Height = height;
          }
        }
      }
    }

    void menuContext_RichText_Click(object sender, EventArgs e)
    {
      
      ItemRichText_Click(sender, e);
    }

    void menuContext_RegEx_Click(object sender, EventArgs e)
    {
      
      ItemRegularExpression_Click(sender, e);
    }

    void menuContext_PasteShape_Click(object sender, EventArgs e)
    {
      
      rbPasteAsShape_Click(sender, e);
    }

    void menuContext_PasteChart_Click(object sender, EventArgs e)
    {
      
      rbPasteAsChart_Click(sender, e);

    }

    void menuContext_DeleteSheet_Click(object sender, EventArgs e)
    {
      if (fpSpread1.Sheets.Count == 1) return;

      this.fpSpread1.Sheets.Remove(fpSpread1.ActiveSheet);
    }


    void menuContext_EditNote_Click(object sender, EventArgs e)
    {
      

      fpSpread1.StartAnnotationMode();
    }

    void menuContext_DeleteNote_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ActiveSheet.ActiveCell.Note = null;
    }

    void SheetTabContextMenu_Copy_Click(object sender, EventArgs e)
    {
      SpreadView spreadView = fpSpread1.GetRootWorkbook();
      FarPoint.Win.Spread.Action action = spreadView.GetActionMap().Get(SpreadActions.ClipboardCopyActiveSheet);
      if (action is FarPoint.Win.Spread.UndoRedo.UndoAction)
      {
        action = (action as FarPoint.Win.Spread.UndoRedo.UndoAction).Clone();
      }

      if (action != null)
      {
        if (spreadView.AllowUndo && (action is FarPoint.Win.Spread.UndoRedo.UndoAction))
        {
          FarPoint.Win.Spread.UndoRedo.UndoAction undoAction = action as FarPoint.Win.Spread.UndoRedo.UndoAction;
          spreadView.UndoManager.PerformUndoAction(undoAction);
        }
        else
        {
          action.PerformAction(spreadView);
        }
      }
    }

    void SheetTabContextMenu_Cut_Click(object sender, EventArgs e)
    {
      SpreadView spreadView = fpSpread1.GetRootWorkbook();
      FarPoint.Win.Spread.Action action = spreadView.GetActionMap().Get(SpreadActions.ClipboardCutActiveSheet);
      if (action is FarPoint.Win.Spread.UndoRedo.UndoAction)
      {
        action = (action as FarPoint.Win.Spread.UndoRedo.UndoAction).Clone();
      }

      if (action != null)
      {
        if (spreadView.AllowUndo && (action is FarPoint.Win.Spread.UndoRedo.UndoAction))
        {
          FarPoint.Win.Spread.UndoRedo.UndoAction undoAction = action as FarPoint.Win.Spread.UndoRedo.UndoAction;
          spreadView.UndoManager.PerformUndoAction(undoAction);
        }
        else
        {
          action.PerformAction(spreadView);
        }
      }
    }

    void SheetTabContextMenu_Paste_Click(object sender, EventArgs e)
    {
      SpreadView spreadView = fpSpread1.GetRootWorkbook();
      FarPoint.Win.Spread.Action action = spreadView.GetActionMap().Get(SpreadActions.ClipboardPasteSheet);
      if (action is FarPoint.Win.Spread.UndoRedo.UndoAction)
      {
        action = (action as FarPoint.Win.Spread.UndoRedo.UndoAction).Clone();
      }
      if (action != null)
      {
        action.PerformAction(spreadView);
      }
    }
    #endregion

    #region HOME Tab
    #region Fonts Group
    /// <summary>
    /// List of built-in font sizes displaying in FontSize ComboBox
    /// </summary>
    float[] fontSizes = new float[] { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 };

    /// <summary>
    /// Initialize Ribbon Font Group.
    /// </summary>
    private void InitializeGroupFont()
    {
      for (int i = 0; i < fontSizes.Length; i++)
        rcbFontSize.Items.Add(new RibbonButton(fontSizes[i].ToString()));
      this.rbBottomBorder.Click += new EventHandler(Borders);
      this.rbTopBorder.Click += new EventHandler(Borders);
      this.rbLeftBorder.Click += new EventHandler(Borders);
      this.rbRightBorder.Click += new EventHandler(Borders);
      this.rbNoBorder.Click += new EventHandler(Borders);
      this.rbAllBorder.Click += new EventHandler(Borders);
      this.rbOutsideBorder.Click += new EventHandler(Borders);
      this.rbThickBoxBorder.Click += new EventHandler(Borders);
      this.rbBottomDoubleBorder.Click += new EventHandler(Borders);
      this.rbThickBottomBorder.Click += new EventHandler(Borders);
      this.rbTopAndBottomBorder.Click += new EventHandler(Borders);
      this.rbTopAndThickBottomBorder.Click += new EventHandler(Borders);
      this.rbTopAndDoubleBottomBorder.Click += new EventHandler(Borders);
      this.rbMoreBorder.Click += new EventHandler(Borders);
    }

    /// <summary>
    /// Bold ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnBold_Click(object sender, EventArgs e)
    {
      ChangeFont(string.Empty, 0, FontStyle.Bold, rbBold.Pressed, true);
    }

    /// <summary>
    /// Italic ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnItalic_Click(object sender, EventArgs e)
    {
      ChangeFont(string.Empty, 0, FontStyle.Italic, rbItalic.Pressed, true);
    }

    /// <summary>
    /// Underline ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnUnderLine_Click(object sender, EventArgs e)
    {
      ChangeFont(string.Empty, 0, FontStyle.Underline, rbUnderLine.Pressed, true);
    }

    /// <summary>
    /// Background color button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ColorPickerFillColor_Click(object sender, EventArgs e)
    {
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "BackColor", this.rccFillColor.Color));
    }

    /// <summary>
    /// Background color picker select index changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ColorPickerFillColor_SelectedColorChanged(object sender, EventArgs e)
    {
      ColorPickerFillColor_Click(sender, e);
    }

    /// <summary>
    /// Font color button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ColorPickerFontColor_Click(object sender, EventArgs e)
    {
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "ForeColor", this.rccFontColor.Color));
    }

    /// <summary>
    /// Font color picker select index changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ColorPickerFontColor_SelectedColorChanged(object sender, EventArgs e)
    {
      ColorPickerFontColor_Click(sender, e);
    }

    /// <summary>
    /// Increase font size ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnIncreaseFontSize_Click(object sender, EventArgs e)
    {
      float value = 0f;
      if (float.TryParse(rcbFontSize.Text, out value))
      {
        int idx = Array.BinarySearch<float>(fontSizes, value);
        if (idx >= 0)
          idx += 1;
        else
          idx = ~idx;
        if (idx < rcbFontSize.Items.Count - 1)
          rcbFontSize.SelectedIndex = idx;
      }
    }

    /// <summary>
    /// Decrease font size ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnDecreaseFontSize_Click(object sender, EventArgs e)
    {
      float value = 0f;
      if (float.TryParse(rcbFontSize.Text, out value))
      {
        int idx = Array.BinarySearch<float>(fontSizes, value);
        if (idx >= 0)
          idx -= 1;
        else
          idx = ~idx - 1;
        if (idx >= 0)
          rcbFontSize.SelectedIndex = idx;
      }
    }

    /// <summary>
    /// Family font ribbon combobox select index changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ComboBoxFont_SelectedIndexChanged(object sender, EventArgs e)
    {
      string fontFamilyName = this.rcbFont.Text;
      ChangeFont(fontFamilyName, 0, FontStyle.Regular, false, false);
    }

    /// <summary>
    /// Font size ribbon combobox select index changed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ComboBoxFontSize_SelectedIndexChanged(object sender, EventArgs e)
    {
      float newSize = 0f;
      if (float.TryParse(this.rcbFontSize.Text, out newSize))
      {
        ChangeFont(string.Empty, newSize, FontStyle.Regular, false, false);
      }
    }

    /// <summary>
    /// Change the font style of cell
    /// </summary>
    /// <param name="fontName">Family font name</param>
    /// <param name="size">Font size</param>
    /// <param name="style">Font style</param>
    /// <param name="toogled">Turn on/off font style</param>
    /// <param name="isToogle">Is Font style turn on/off</param>
    private void ChangeFont(string fontName, float size, FontStyle style, bool toogled, bool isToogle)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange cellRange in cellRanges)
      {
        for (int i = 0; i < cellRange.RowCount; i++)
        {
          for (int j = 0; j < cellRange.ColumnCount; j++)
          {
            Font cellFont = sheet.GetStyleInfo(i + cellRange.Row, j + cellRange.Column).Font ?? fpSpread1.Font;
            bool hasCellFont = sheet.Cells[i + cellRange.Row, j + cellRange.Column].Font != null;
            sheet.Cells[i + cellRange.Row, j + cellRange.Column].Font = new Font(string.IsNullOrEmpty(fontName) ? cellFont.Name : fontName
                                                                            , size > 0 ? size : cellFont.Size
                                                                            , isToogle ? toogled ? cellFont.Style | style : cellFont.Style & ~style : style
                                                                            , cellFont.Unit);
            if (hasCellFont)
              cellFont.Dispose();
          }
        }
      }
    }

    /// <summary>
    /// Border ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Borders(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      RibbonButton rbtn = sender as RibbonButton;
      foreach (CellRange range in cellRanges)
      {
        for (int i = 0; i < range.RowCount; i++)
        {
          for (int j = 0; j < range.ColumnCount; j++)
          {
            switch (rbtn.Name)
            {
              case "rbBottomBorder":
                FarPoint.Win.ComplexBorder cb0 = new FarPoint.Win.ComplexBorder(null, null, null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb0;
                break;
              case "rbTopBorder":
                FarPoint.Win.ComplexBorder cb1 = new FarPoint.Win.ComplexBorder(null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null, null);
                sheet.Cells[i + range.Row, j + range.Column].Border = cb1;
                break;
              case "rbLeftBorder":
                FarPoint.Win.ComplexBorder cb2 = new FarPoint.Win.ComplexBorder(new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null, null, null);
                sheet.Cells[i + range.Row, j + range.Column].Border = cb2;
                break;
              case "rbRightBorder":
                FarPoint.Win.ComplexBorder cb3 = new FarPoint.Win.ComplexBorder(null, null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null);
                sheet.Cells[i + range.Row, j + range.Column].Border = cb3;
                break;
              case "rbNoBorder":
                FarPoint.Win.ComplexBorder cb4 = new FarPoint.Win.ComplexBorder(new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.None));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb4;
                break;
              case "rbAllBorder":
                FarPoint.Win.ComplexBorder cb5 = new FarPoint.Win.ComplexBorder(new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb5;
                break;
              case "rbOutsideBorder":
                FarPoint.Win.ComplexBorder cb6 = new FarPoint.Win.ComplexBorder(new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb6;
                break;
              case "rbThickBoxBorder":
                FarPoint.Win.ComplexBorder cb7 = new FarPoint.Win.ComplexBorder(new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThickLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb7;
                break;
              case "rbBottomDoubleBorder":
                FarPoint.Win.ComplexBorder cb8 = new FarPoint.Win.ComplexBorder(null, null, null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.DoubleLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb8;
                break;
              case "rbThickBottomBorder":
                FarPoint.Win.ComplexBorder cb9 = new FarPoint.Win.ComplexBorder(null, null, null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThickLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb9;
                break;
              case "rbTopAndBottomBorder":
                FarPoint.Win.ComplexBorder cb10 = new FarPoint.Win.ComplexBorder(null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb10;
                break;
              case "rbTopAndThickBottomBorder":
                FarPoint.Win.ComplexBorder cb11 = new FarPoint.Win.ComplexBorder(null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThickLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb11;
                break;
              case "rbTopAndDoubleBottomBorder":
                FarPoint.Win.ComplexBorder cb12 = new FarPoint.Win.ComplexBorder(null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.ThinLine), null, new
                    FarPoint.Win.ComplexBorderSide(FarPoint.Win.ComplexBorderSideStyle.DoubleLine));
                sheet.Cells[i + range.Row, j + range.Column].Border = cb12;
                break;
            }
          }
        }
      }
      if (rbtn.Name == "rbMoreBorder")
        FarPoint.Win.Spread.Design.ExternalDialogs.BorderEditor(this.fpSpread1);
    }
    #endregion

    #region  Clipboard Group
    /// <summary>
    /// Initialize clipboard group.
    /// </summary>
    private void InitializeGroupClipBoard()
    {
      this.rbPasteAll.Click += new EventHandler(rbPasteAll_Click);
      this.rbPasteValues.Click += new EventHandler(rbPasteValues_Click);
      this.rbPasteFormatting.Click += new EventHandler(rbPasteFormatting_Click);
      this.rbPasteFormulas.Click += new EventHandler(rbPasteFormulas_Click);
      this.rbPasteAsLink.Click += new EventHandler(rbPasteAsLink_Click);
      this.rbPasteAsString.Click += new EventHandler(rbPasteAsString_Click);
      this.rbPasteAsShape.Click += new EventHandler(rbPasteAsShape_Click);
      this.rbPasteAsChart.Click += new EventHandler(rbPasteAsChart_Click);
    }

    /// <summary>
    /// Paste ribbon button dropdown.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void rbPaste_DropDown(object sender, EventArgs e)
    {
      rbPasteAsShape.Enabled = (Clipboard.GetDataObject().GetDataPresent("FarPoint.Win.Spread.ShapeClipboardObject"));
      rbPasteAsChart.Enabled = (Clipboard.GetDataObject().GetDataPresent("FarPoint.Win.Spread.Chart.SpreadChartClipboardInfo"));
      if ((Clipboard.GetDataObject().GetDataPresent(typeof(CellInfoRange))))
      {
        this.rbPasteFormatting.Enabled = true;
        this.rbPasteFormulas.Enabled = true;
        this.rbPasteAsLink.Enabled = true;
      }
    }

    /// <summary>
    /// Cut ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnCut_Click(object sender, EventArgs e)
    {
      if ((fpSpread1.ActiveSheet.SelectionCount > 1))
      {
        MessageBox.Show(rm.GetString("CutSelectionWarning"), rm.GetString("MultipleSelectionsTitle"));
      }
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardCut));
    }

    /// <summary>
    /// Copy ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnCopy_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ActiveSheet.ClipboardCopy();
    }

    /// <summary>
    /// Paste ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnPaste_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPaste));
    }

    /// <summary>
    /// Paste all ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteAll_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteAll));
    }

    /// <summary>
    /// Paste values ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteValues_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteValues));
    }

    /// <summary>
    /// Paste formatting ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteFormatting_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteFormatting));
    }

    /// <summary>
    /// Paste formulas ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteFormulas_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteFormulas));
    }

    /// <summary>
    /// Paste as link ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteAsLink_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteAsLink));
    }

    /// <summary>
    /// Paste as string ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteAsString_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteAsString));
    }

    /// <summary>
    /// Paste as shape ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteAsShape_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteShape));
    }

    /// <summary>
    /// Paste as chart ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void rbPasteAsChart_Click(object sender, EventArgs e)
    {
      this.fpSpread1.UndoManager.PerformUndoAction((FarPoint.Win.Spread.UndoRedo.UndoAction)this.fpSpread1.GetActionMap().Get(SpreadActions.ClipboardPasteAll));
    }
    #endregion

    #region  Alignment Group
    /// <summary>
    /// Top alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnTopAlign_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellVerticalAlignment.Top : CellVerticalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "VerticalAlignment", newValue));
      rbMiddleAlign.Pressed = false;
      rbBottomAlign.Pressed = false;
      rbVerticalJustify.Pressed = false;
      rbVerticalDistributed.Pressed = false;
    }

    /// <summary>
    /// Middle alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnMiddleAlign_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellVerticalAlignment.Center : CellVerticalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "VerticalAlignment", newValue));
      rbTopAlign.Pressed = false;
      rbBottomAlign.Pressed = false;
      rbVerticalJustify.Pressed = false;
      rbVerticalDistributed.Pressed = false;
    }

    /// <summary>
    /// Bottom alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnBottomAlign_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellVerticalAlignment.Bottom : CellVerticalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "VerticalAlignment", newValue));
      rbTopAlign.Pressed = false;
      rbMiddleAlign.Pressed = false;
      rbVerticalJustify.Pressed = false;
      rbVerticalDistributed.Pressed = false;
    }

    /// <summary>
    /// Vertical distributed ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnVerticalDistributed_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellVerticalAlignment.Distributed : CellVerticalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "VerticalAlignment", newValue));
      rbTopAlign.Pressed = false;
      rbMiddleAlign.Pressed = false;
      rbVerticalJustify.Pressed = false;
      rbBottomAlign.Pressed = false;
    }

    /// <summary>
    /// Vertical justify ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnVerticaljustify_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellVerticalAlignment.Justify : CellVerticalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "VerticalAlignment", newValue));
      rbTopAlign.Pressed = false;
      rbMiddleAlign.Pressed = false;
      rbVerticalDistributed.Pressed = false;
      rbBottomAlign.Pressed = false;
    }

    /// <summary>
    /// Left alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAlignTextLeft_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellHorizontalAlignment.Left : CellHorizontalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "HorizontalAlignment", newValue));
      rbAlignTextMiddle.Pressed = false;
      rbAlignTextRight.Pressed = false;
      rbHorizontalJustify.Pressed = false;
      rbHorizontalDistributed.Pressed = false;
    }

    /// <summary>
    /// Center alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnCenter_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellHorizontalAlignment.Center : CellHorizontalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "HorizontalAlignment", newValue));
      rbAlignTextLeft.Pressed = false;
      rbAlignTextRight.Pressed = false;
      rbHorizontalJustify.Pressed = false;
      rbHorizontalDistributed.Pressed = false;
    }

    /// <summary>
    /// Right alignment ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAlignTextRight_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellHorizontalAlignment.Right : CellHorizontalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "HorizontalAlignment", newValue));
      rbAlignTextMiddle.Pressed = false;
      rbAlignTextLeft.Pressed = false;
      rbHorizontalJustify.Pressed = false;
      rbHorizontalDistributed.Pressed = false;
    }

    /// <summary>
    /// Horizontal justify ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAlignTextHorizontalJustify_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellHorizontalAlignment.Justify : CellHorizontalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "HorizontalAlignment", newValue));
      rbAlignTextMiddle.Pressed = false;
      rbAlignTextLeft.Pressed = false;
      rbAlignTextRight.Pressed = false;
      rbHorizontalDistributed.Pressed = false;
    }

    /// <summary>
    /// Horizontal distributed ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnAlignTextHorizontalDistributed_Click(object sender, EventArgs e)
    {
      object newValue = ((RibbonToggleButton)sender).Pressed ? CellHorizontalAlignment.Distributed : CellHorizontalAlignment.General;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
        this.fpSpread1.UndoManager.PerformUndoAction(new PropertyChangeUndoAction(this.fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1], "HorizontalAlignment", newValue));
      rbAlignTextMiddle.Pressed = false;
      rbAlignTextLeft.Pressed = false;
      rbAlignTextRight.Pressed = false;
      rbHorizontalJustify.Pressed = false;
    }

    /// <summary>
    /// Decrease text indent ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnDecIndent_Click(object sender, EventArgs e)
    {
      int a = fpSpread1.ActiveSheet.ActiveCell.TextIndent;
      fpSpread1.ActiveSheet.ActiveCell.TextIndent = Math.Max(a - 10, 0);
    }

    /// <summary>
    /// Increase text indent ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnIncreaseIndent_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.ActiveCell.TextIndent += 10;
    }

    /// <summary>
    /// Wrap text ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnWrapText_Click(object sender, EventArgs e)
    {
      if (this.fpSpread1.ActiveSheet.ActiveCell.CellType is FarPoint.Win.Spread.CellType.TextCellType)
      {
        TextCellType t = this.fpSpread1.ActiveSheet.ActiveCell.CellType as FarPoint.Win.Spread.CellType.TextCellType;
        t.WordWrap = !t.WordWrap;
        rbWrapText.Pressed = t.WordWrap;
      }
    }

    /// <summary>
    /// Merge cell ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnMerge_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] ranges = GetSpreadSelections();
      if (rbMerge.Pressed)
      {
        foreach (CellRange range in ranges)
          fpSpread1.ActiveSheet.AddSpanCell(range.Row, range.Column, range.RowCount, range.ColumnCount);
      }
      else
      {
        foreach (CellRange range in ranges)

          for (int row = range.Row; row <= range.Row + range.RowCount - 1; row++)
          {
            for (int column = range.Column; column <= range.Column + range.ColumnCount - 1; column++)
            {
              fpSpread1.ActiveSheet.RemoveSpanCell(row, column);
            }
          }
      }
    }
    #endregion

    #region  Cell Type Group
    /// <summary>
    /// Initialize cell type group
    /// </summary>
    private void InitializeGroupCellType()
    {
      this.rbdwText.Click += new EventHandler(ItemText_Click);
      this.rbdwPicture.Click += new EventHandler(ItemImage_Click);
      this.rbdwGeneral.Click += new EventHandler(ItemGeneral_Click);
      this.rbdwNumber.Click += new EventHandler(ItemNumber_Click);
      this.rbdwPercent.Click += new EventHandler(ItemPercent_Click);
      this.rbdwCurrency.Click += new EventHandler(ItemCurrency_Click);
      this.rbdwRegularExpression.Click += new EventHandler(ItemRegularExpression_Click);
      this.rbdwBarCode.Click += new EventHandler(ItemBarcode_Click);
      this.rbdwButton.Click += new EventHandler(ItemButton_Click);
      this.rbdwCheckBox.Click += new EventHandler(ItemCheckBox_Click);
      this.rbdwComboBox.Click += new EventHandler(ItemComboBox_Click);
      this.rbdwMultiOption.Click += new EventHandler(ItemMultiOption_Click);
      this.rbdwMultiColumnComboBox.Click += new EventHandler(ItemMultiColumnComboBox_Click);
      this.rbdwListBox.Click += new EventHandler(ItemListBox_Click);
      this.rbdwRichText.Click += new EventHandler(ItemRichText_Click);
      this.rbdwDateTime.Click += new EventHandler(ItemDataTime_Click);
      this.rbdwColorPicker.Click += new EventHandler(ItemColorPicker_Click);
      this.rbdwProgress.Click += new EventHandler(ItemProgress_Click);
      this.rbdwSlider.Click += new EventHandler(ItemSlider_Click);
      this.rbdwMask.Click += new EventHandler(ItemMask_Click);
      this.rbdwHyperLink.Click += new EventHandler(ItemHyperLink_Click);
      this.rbdwGcDate.Click += new EventHandler(ItemGcDate_Click);
      this.rbdwGcTextBox.Click += new EventHandler(ItemGcTextBox_Click);
    }

    /// <summary>
    /// Text celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemText_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.TextCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Text celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemImage_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ImageCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// General celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemGeneral_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.GeneralCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Number celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemNumber_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.NumberCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Percent celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemPercent_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PercentCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Currency celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemCurrency_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.CurrencyCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Barcode celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemBarcode_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.BarcodeCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Button celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemButton_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ButtonCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// CheckBox celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemCheckBox_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.CheckBoxCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// ComboBox celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemComboBox_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ComboCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// MultiOption celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemMultiOption_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.MultiOptionCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Regular expression celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemRegularExpression_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.RegExCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Multi column combobox celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemMultiColumnComboBox_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.MultiColumnComboBoxCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Listbox celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemListBox_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ListBoxCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Richtext celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemRichText_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.RichTextCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// DateTime celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemDataTime_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.DateTimeCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Color picker celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemColorPicker_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ColorPickerCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Progress celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemProgress_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.ProgressCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Slider celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemSlider_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.SliderCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Mask celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemMask_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.MaskCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// Hyperlink celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemHyperLink_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ExternalDialogs.CellTypeEditor(this.fpSpread1, FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.LinkCellType);
      Celltype_SelectionChanged();
    }

    /// <summary>
    /// GcTextBox celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemGcTextBox_Click(object sender, EventArgs e)
    {
      if (_pluginCellTypeSettings == null)
      {
        LoadInputManPluginCellType();
      }
      if (this._pluginCellTypeSettings != null)
      {
        for (int index = 0; index <= this._pluginCellTypeSettings.Count - 1; index++)
        {
          if (this._pluginCellTypeSettings[index].GetType().Name.Contains("GcTextCell"))
          {
            ShowCellTypeDialog(FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PluginCellType, this._pluginCellTypeSettings[index] as IPluginCellTypeSetting);
            break; // TODO: might not be correct. Was : Exit For
          }
        }
      }
    }

    /// <summary>
    /// GcDate celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ItemGcDate_Click(object sender, EventArgs e)
    {
      if (_pluginCellTypeSettings == null)
      {
        LoadInputManPluginCellType();
      }
      if (this._pluginCellTypeSettings != null)
      {
        for (int index = 0; index <= this._pluginCellTypeSettings.Count - 1; index++)
        {
          if (this._pluginCellTypeSettings[index].GetType().Name.Contains("GcDateTimeCell"))
          {
            ShowCellTypeDialog(FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PluginCellType, this._pluginCellTypeSettings[index] as IPluginCellTypeSetting);
            break; // TODO: might not be correct. Was : Exit For
          }
        }
      }
    }

    /// <summary>
    /// Clear celltype ribbon button click.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void btnClearCellType_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ActiveSheet.ActiveCell.CellType = null;
      Celltype_SelectionChanged();
    }
    #endregion

    #region  (Editing)
    // (Initialization editorial team)
    private void InitializeGroupEditing()
    {
      this.rbSortAToZ.Click += new EventHandler(btnMenuSortAToZ_Click);
      this.rbSortZToA.Click += new EventHandler(btnMenuSortZToA_Click);
      this.rbGoto.Click += new EventHandler(btnMenuGoto_Click);
      this.rbFind.Click += new EventHandler(btnMenuFind_Click);
      this.rbCustomSort.Click += new EventHandler(btnCustomSort_Click);
      this.rbSelectAllSheet.Click += new EventHandler(btnSheets_Click);
      this.rbSelectAllCells.Click += new EventHandler(btnCells_Click);

      this.rbSelectAllData.Click += new EventHandler(btnData_Click);
    }
    // (Clear all)
    private void btnClearAll_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.ClearRange(range.Row, range.Column, range.RowCount, range.ColumnCount, false);
      fpSpread1_SelectionChanged(null, null);
    }
    // (a to z Sort)
    private void btnMenuSortAToZ_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange cr = sheet.GetSelection(0);
      if (cr != null)
        sheet.SortRange(cr.Row, cr.Column, cr.RowCount, cr.ColumnCount, true, new SortInfo[] { new SortInfo(sheet.ActiveColumnIndex, true) });

    }
    // (z to a sort)
    private void btnMenuSortZToA_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange cr = sheet.GetSelection(0);
      if (cr != null)
        sheet.SortRange(cr.Row, cr.Column, cr.RowCount, cr.ColumnCount, true, new SortInfo[] { new SortInfo(sheet.ActiveColumnIndex, false) });
    }
    // (Custom Sort)
    private void btnCustomSort_Click(object sender, EventArgs e)
    {
      SortDlg dlgSort = new SortDlg(fpSpread1);
      if (dlgSort != null) dlgSort.ShowDialog();
    }
    // (Lock)
    private void btnLock_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.Protect = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();

      CellRange range = cellRanges[0];
      if (range.Row == -1 && range.Column == -1)
      {
        fpSpread1.ActiveSheet.DefaultStyle.Locked = !fpSpread1.ActiveSheet.DefaultStyle.Locked;
        return;
      }
      else if (range.Row == -1)
      {
        fpSpread1.ActiveSheet.Columns[range.Column, range.Column + range.ColumnCount - 1].Locked = !fpSpread1.ActiveSheet.Columns[range.Column, range.Column + range.ColumnCount - 1].Locked;
        return;
      }
      else if (range.Column == -1)
      {
        fpSpread1.ActiveSheet.Rows[range.Row, range.Row + range.RowCount - 1].Locked = !fpSpread1.ActiveSheet.Rows[range.Row, range.Row + range.RowCount - 1].Locked;
        return;
      }
      else
      {
        FarPoint.Win.Spread.Cell cellobj;
        cellobj = fpSpread1.ActiveSheet.Cells[range.Row, range.Column, range.Row + range.RowCount - 1, range.Column + range.ColumnCount - 1];
        if (cellobj.Locked == false)
        {
          rbLock.Pressed = true;
          cellobj.Locked = true;
        }
        else
        {
          cellobj.Locked = false;
          rbLock.Pressed = false;
        }
      }
    }
    // (Find)
    private void btnMenuFind_Click(object sender, EventArgs e)
    {
      fpSpread1.SearchWithDialog("");
      if ((fpSpread1.SearchDialog != null))
        fpSpread1.SearchDialog.Icon = this.Icon;
    }
    // (Select Form)
    private void btnSheets_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      sheet.ClearSelection();
      sheet.AddSelection(-1, -1, -1, -1);
    }
    // (Select a cell)
    private void btnCells_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      sheet.ClearSelection();
      sheet.AddSelection(0, 0, sheet.RowCount, sheet.ColumnCount);
    }
    // (Select Data)
    private void btnData_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      sheet.ClearSelection();
      int DataRows = sheet.GetLastNonEmptyRow(NonEmptyItemFlag.Data);
      int DataColumns = sheet.GetLastNonEmptyColumn(NonEmptyItemFlag.Data);
      for (int r = 0; r <= DataRows; r++)
      {
        for (int c = 0; c <= DataColumns; c++)
        {
          if (sheet.Cells[r, c].Value != null)
            sheet.AddSelection(r, c, 1, 1);
        }
      }

    }

    // (Go)
    private void btnMenuGoto_Click(object sender, EventArgs e)
    {
        FarPoint.Win.Spread.DrawingSpace.TextInput input = new FarPoint.Win.Spread.DrawingSpace.TextInput();
        // Adjust input textbox
        input.TextInputInputBox.Left = input.TextInputInputBox.Left + 120;
        input.TextInputInputBox.Width = input.TextInputInputBox.Width - 120;
        input.Text = rm.GetString("GoTo");
        input.TextInputPrompt.Text = rm.GetString("GoToPrompt");
        input.TextInputInputBox.Text = FarPoint.Win.Spread.Design.common.ConvertIndexToLetters(fpSpread1.ActiveSheet.ActiveColumnIndex) + (fpSpread1.ActiveSheet.ActiveRowIndex + 1).ToString();
        input.NumbersOnly = false;
        input.HelpTopic = "SDGoToCell.html";
        input.Icon = this.Icon;
        // Verify input after OK button is pressed
        if ((input.ShowDialog(this) == System.Windows.Forms.DialogResult.OK))
        {
          string @ref = null;
          int rIndex = -1;
          int cIndex = -1;
          int rowOffset = -1;
          string strError = rm.GetString("GoTo_ErrorText");          
          string strTitle = rm.GetString("GoTo_ErrorTitle");          

          @ref = input.TextInputInputBox.Text;
          for (rowOffset = 0; rowOffset <= @ref.Length - 1; rowOffset++)
          {
            if (char.IsDigit(@ref.ToCharArray()[rowOffset]))
            {
              try
              {
                cIndex = FarPoint.Win.Spread.Design.common.ConvertLettersToIndex(@ref.Substring(0, rowOffset));
                rIndex = int.Parse(@ref.Substring(rowOffset)) - 1;
              }
              catch
              {
                MessageBox.Show(strError, strTitle);
                return;
              }
              break; 
            }
          }
          if ((rIndex > -1 && cIndex > -1))
          {
            int acvi = fpSpread1.GetActiveColumnViewportIndex();
            int arvi = fpSpread1.GetActiveRowViewportIndex();
            fpSpread1.SetViewportLeftColumn(Math.Max(acvi, 0), cIndex);
            fpSpread1.SetViewportTopRow(Math.Max(arvi, 0), rIndex);
            fpSpread1.ActiveSheet.SetActiveCell(rIndex, cIndex);
          }
          else
          {
            MessageBox.Show(strError, strTitle);
          }
        }       
    }
    #endregion

    #region  (Style)
    // (Initialization style group)
    private void InitializeGroupStyle()
    {
      this.rbCFGradientBlue.Click += new EventHandler(ItemBlue_Click);
      this.rbCFGradientRed.Click += new EventHandler(ItemRed_Click);
      this.rbCFGradientGreen.Click += new EventHandler(ItemGreen_Click);
      this.rbCFGradientOrange.Click += new EventHandler(ItemOrange_Click);
      this.rbCFGradientLightBlue.Click += new EventHandler(ItemLightBlue_Click);
      this.rbCFGradientPurple.Click += new EventHandler(ItemPurple_Click);
      this.rbCFSolidBlue.Click += new EventHandler(ItemSolidBlue_Click);
      this.rbCFSolidRed.Click += new EventHandler(ItemSolidRed_Click);
      this.rbCFSolidGreen.Click += new EventHandler(ItemSolidGreen_Click);
      this.rbCFSolidOrange.Click += new EventHandler(ItemSolidOrange_Click);
      this.rbCFSolidLightBlue.Click += new EventHandler(ItemSolidLightBlue_Click);
      this.rbCFGreenYellowRed.Click += new EventHandler(ItemGreenYellowRedScale_Click);
      this.ItemRedYellowGreenScale.Click += new EventHandler(ItemRedYellowGreenScale_Click);
      this.ItemGreenWhiteRedScale.Click += new EventHandler(ItemGreenWhiteRedScale_Click);
      this.ItemRedWhiteGreenScale.Click += new EventHandler(ItemRedWhiteGreenScale_Click);
      this.ItemBlueWhiteRedScale.Click += new EventHandler(ItemBlueWhiteRedScale_Click);
      this.ItemRedWhiteBlueScale.Click += new EventHandler(ItemRedWhiteBlueScale_Click);
      this.ItemWhiteRedScale.Click += new EventHandler(ItemWhiteRedScale_Click);
      this.ItemRedWhiteScale.Click += new EventHandler(ItemRedWhiteScale_Click);
      this.ItemGreenWihte.Click += new EventHandler(ItemGreenWihte_Click);
      this.ItemWhiteGreen.Click += new EventHandler(ItemWhiteGreen_Click);
      this.ItemGreenYellowScale.Click += new EventHandler(ItemGreenYellowScale_Click);
      this.rbCF3ArrowsColored.Click += new EventHandler(ItemrbCF3ArrowsColoredSmallImage_Click);
      this.rbCF3ArrowsGray.Click += new EventHandler(ItemrbCF3ArrowsGraySmallImage_Click);
      this.rbCF3Triangles.Click += new EventHandler(ItemrbCF3Triangles_Click);
      this.rbCF4ArrowsGray.Click += new EventHandler(ItemrbCF4ArrowsGray_Click);
      this.rbCF4ArrowsColored.Click += new EventHandler(ItemrbCF4ArrowsColored_Click);
      this.rbCF5ArrowsGray.Click += new EventHandler(ItemrbCF5ArrowsGray_Click);
      this.rbCF5Colored.Click += new EventHandler(ItemrbCF5Colored_Click);
      this.rbCF3TrafficLightsUnRimmmed.Click += new EventHandler(ItemrbCF3TrafficLightsUnRimmmed_Click);
      this.rbCF3TrafficLightsRimmmed.Click += new EventHandler(ItemrbCF3TrafficLightsRimmmed_Click);
      this.rbCF3Sign.Click += new EventHandler(ItemrbCF3Sign_Click);
      this.rbCF4TracfficLightsUnRimmed.Click += new EventHandler(ItemrbCF4TracfficLightsUnRimmed_Click);
      this.rbCFRedToBlack.Click += new EventHandler(ItemrbCFRedToBlack_Click);
      this.rbCF3SymbolsCircled.Click += new EventHandler(ItemrbCF3SymbolsCircled_Click);
      this.rbCF3SymbolsUnCircled.Click += new EventHandler(ItemrbCF3SymbolsUnCircled_Click);
      this.rbCF3Flags.Click += new EventHandler(ItemrbCF3Flags_Click);
      this.rbCFStars.Click += new EventHandler(ItemrbCFStars_Click);
      this.rbCF4Ratings.Click += new EventHandler(ItemrbCF4Ratings_Click);
      this.rbCFQuarsters.Click += new EventHandler(Item5Quarters_Click);
      this.rbCF5Ratings.Click += new EventHandler(ItemrbCF5Ratings_Click);
      this.rbCF5Boxs.Click += new EventHandler(ItemrbCF5Boxs_Click);
      this.rbLessThan.Click += new EventHandler(btnLessThan_Click);
      this.rbBetween.Click += new EventHandler(btnBetween_Click);
      this.rbGreaterThan.Click += new EventHandler(btnGreateThan_Click);
      this.rbCFDuplicateValues.Click += new EventHandler(btnDuplicateValues_Click);
      this.rbCFEqualTo.Click += new EventHandler(btnEqualTo_Click);
      this.rbCFTextThatContains.Click += new EventHandler(btnTextthatcontains_Click);
      this.rbCFADateOccurring.Click += new EventHandler(btnADataOccurring_Click);
      this.rbCFTop10Items.Click += new EventHandler(btnTop10Items_Click);
      this.rbCFTop10Per.Click += new EventHandler(btnTop10Percent_Click);
      this.rbCFBottom10Items.Click += new EventHandler(btnBottom10Items_Click);
      this.rbCFBottom10Per.Click += new EventHandler(btnBottom10percent_Click);
      this.rbCFNewRule.Click += new EventHandler(btnNewRules_Click);
      this.rbCFAboveAverage.Click += new EventHandler(btnAboveAverage_Click);
      this.rbCFBelowAverage.Click += new EventHandler(btnBelowAverage_Click);
      this.rbCFManagerRules.Click += new EventHandler(btnManageRules_Click);
      this.rbCFClearFromSelectedCell.Click += new EventHandler(btnFromSelectedCells_Click);
      this.rbCFClearEntireSheet.Click += new EventHandler(btnEntireSheet_Click);
      this.rbCFHighLightMoreRules.Click += new EventHandler(MoreRules);
      this.rbCFDataBarMoreRules.Click += new EventHandler(MoreRules);
      this.rbCFTopBottomMoreRules.Click += new EventHandler(MoreRules);
      this.rbCFColorScaleMoreRule.Click += new EventHandler(MoreRules);
      this.rbCFIconSetsMoreRule.Click += new EventHandler(MoreRules);
    }
    // (Blue fill)
    private void ItemSolidBlue_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Blue;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Red fill)
    private void ItemSolidRed_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Red;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Green fill)
    private void ItemSolidGreen_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Green;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      CellRange range = cellRanges[0];
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Orange filling)
    private void ItemSolidOrange_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Orange;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Light blue fill)
    private void ItemSolidLightBlue_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.LightBlue;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Purple filling)
    private void ItemSolidPurple_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Purple;
      d.ShowBorder = true;
      d.Minimum = new FarPoint.Win.Spread.ConditionalFormattingValue(0, FarPoint.Win.Spread.ConditionalFormattingValueType.Number);
      d.Maximum = new FarPoint.Win.Spread.ConditionalFormattingValue(15, FarPoint.Win.Spread.ConditionalFormattingValueType.Max);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Blue gradient)
    private void ItemBlue_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Blue;
      d.FillColor = Color.Blue;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Green Gradient)
    private void ItemGreen_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Green;
      d.FillColor = Color.Green;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Orange Gradient)
    private void ItemOrange_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Orange;
      d.FillColor = Color.Orange;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (Light blue gradient)
    private void ItemLightBlue_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.LightBlue;
      d.FillColor = Color.LightBlue;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Purple gradient)
    private void ItemPurple_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Purple;
      d.FillColor = Color.Purple;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red gradient)
    private void ItemRed_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DatabarConditionalFormattingRule d = new FarPoint.Win.Spread.DatabarConditionalFormattingRule();
      d.BorderColor = Color.Red;
      d.FillColor = Color.Red;
      d.Gradient = true;
      d.Priority = 3;
      d.ShowBorder = true;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Green, yellow and red)
    private void ItemGreenYellowRedScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Green, Color.Yellow, Color.Red);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red yellow and green)
    private void ItemRedYellowGreenScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Red, Color.Yellow, Color.Green);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Green, white and red)
    private void ItemGreenWhiteRedScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Green, Color.White, Color.Red);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red and green)
    private void ItemRedWhiteGreenScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Red, Color.White, Color.Green);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Blue and red)
    private void ItemBlueWhiteRedScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Blue, Color.White, Color.Red);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red, white and blue)
    private void ItemRedWhiteBlueScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.ThreeColorScaleConditionalFormattingRule(Color.Red, Color.White, Color.Blue);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (White and red)
    private void ItemWhiteRedScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.White, Color.Red);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red and white)
    private void ItemRedWhiteScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.Red, Color.White);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Green White)
    private void ItemGreenWihte_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.Green, Color.White);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (White and green)
    private void ItemWhiteGreen_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.White, Color.Green);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Green and yellow)
    private void ItemGreenYellowScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.Green, Color.Yellow);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Yellow-green)
    private void ItemYellowGreenScale_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule d = new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule(Color.Yellow, Color.Green);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Three colored arrows)
    private void ItemrbCF3ArrowsColoredSmallImage_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
          (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeColoredArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Three gray arrows)
    private void ItemrbCF3ArrowsGraySmallImage_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
          (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeGrayArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 Triangle)
    private void ItemrbCF3Triangles_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
          (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeTriangles);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (4 gray arrow)
    private void ItemrbCF4ArrowsGray_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FourGrayArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Four colored arrows)
    private void ItemrbCF4ArrowsColored_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FourColoredArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (5 gray arrow)
    private void ItemrbCF5ArrowsGray_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FiveGrayArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (5 colored arrows)
    private void ItemrbCF5Colored_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FiveColoredArrows);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 frameless traffic lights)
    private void ItemrbCF3TrafficLightsUnRimmmed_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeUnrimmedTrafficLights);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 rimmed traffic lights)
    private void ItemrbCF3TrafficLightsRimmmed_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeRimmedTrafficLights);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 logo)
    private void ItemrbCF3Sign_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
      (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeSigns); // fixed ;) -scl
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (4 frameless traffic lights)
    private void ItemrbCF4TracfficLightsUnRimmed_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FourTrafficLights);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (Red to black)
    private void ItemrbCFRedToBlack_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.RedToBlack);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 with a circle logo)
    private void ItemrbCF3SymbolsCircled_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeCircledSymbols);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 No circle logo)
    private void ItemrbCF3SymbolsUnCircled_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeUnCircledSymbols);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    // (3 flag)
    private void ItemrbCF3Flags_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeFlags);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (3 stars)
    private void ItemrbCFStars_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.ThreeStars);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (4 Ratings)
    private void ItemrbCF4Ratings_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FourRatings);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (5 Ratings)
    private void ItemrbCF5Ratings_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FiveRatings);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);

    }
    // (5 boxes)
    private void ItemrbCF5Boxs_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FiveBoxes);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    //5Quarters
    private void Item5Quarters_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.IconSetConditionalFormattingRule d = new FarPoint.Win.Spread.IconSetConditionalFormattingRule
         (FarPoint.Win.Spread.ConditionalFormattingIconSetStyle.FiveQuarters);
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      sheet.SetConditionalFormatting(range.Row, range.Column, range.RowCount, range.ColumnCount, d);
    }
    private CellRange[] GetActiveCellRanges()
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;      
      FarPoint.Win.Spread.Model.CellRange[] CellRange = null;
      if (fpSpread1.ActiveSheet.SelectionCount >= 1)
      {
        CellRange = fpSpread1.ActiveSheet.GetSelections();
      }
      else
      {
        if ((fpSpread1.ActiveSheet.ActiveRowIndex != -1 & fpSpread1.ActiveSheet.ActiveColumnIndex != -1))
        {
          CellRange = new CellRange[1] { new FarPoint.Win.Spread.Model.CellRange(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex, 1, 1) };
        }
      }
      CellRange = GetCorrectCellRanges(CellRange);
      return CellRange;
    }

    private void showConditionalFormatDialog(ComparisonOperator compairOperator)
    {
      CellRange[] cellrange = GetActiveCellRanges();
      if (cellrange == null)
      {
        MessageBox.Show("Before you set conditional format. you need select a cellrange first!", "GrapeCity Spread Designer", MessageBoxButtons.OK);
        return;
      }
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, fpSpread1, compairOperator.ToString(), cellrange);
    }
    // (Greater than)
    private void btnGreateThan_Click(object sender, EventArgs e)
    {
      showConditionalFormatDialog(ComparisonOperator.GreaterThan);      
    }
    // (Less than)
    private void btnLessThan_Click(object sender, EventArgs e)
    {
      showConditionalFormatDialog(ComparisonOperator.LessThan);           
    }
    // (Duplicate values)
    private void btnDuplicateValues_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFDuplicateValues", cellRanges);
    }
    // (Between)
    private void btnBetween_Click(object sender, EventArgs e)
    {
      showConditionalFormatDialog(ComparisonOperator.Between);          
    }
    // (Equal)
    private void btnEqualTo_Click(object sender, EventArgs e)
    {
      showConditionalFormatDialog(ComparisonOperator.EqualTo);          
    }
    // (Text contains)
    private void btnTextthatcontains_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFTextThatContains", cellRanges);
    }
    // (Date of occurrence)
    private void btnADataOccurring_Click(object sender, EventArgs e)
    {
      CellRange[] cellRanges = GetActiveCellRanges();

      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFADateOccurring", cellRanges);
    }
    // (Top 10)
    private void btnTop10Items_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFTop10Items", cellRanges);
    }
    // (Top 10%)
    private void btnTop10Percent_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFTop10Per", cellRanges);

    }
    // (Bottom 10)
    private void btnBottom10Items_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFBottom10Items", cellRanges);
    }
    // (Bottom 10%)
    private void btnBottom10percent_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFBottom10Per", cellRanges);
    }
    // (Above average)
    private void btnAboveAverage_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFAboveAverage", cellRanges);
    }
    // (Below average)
    private void btnBelowAverage_Click(object sender, EventArgs e)
    {      
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetActiveCellRanges();
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormatDialog(this, this.fpSpread1, "rbCFBelowAverage", cellRanges);
    }
    // (More rules)
    private void MoreRules(object sender, EventArgs e)
    {
      IConditionalFormattingRule rule = null;
      switch (sender.ToString())
      {
        case "CFNewRule":
          rule = new TwoColorScaleConditionalFormattingRule();
          break;
        case "CFHighLightMoreRules":
          rule = new UnaryComparisonConditionalFormattingRule(UnaryComparisonOperator.GreaterThan, 0, false);
          break;
        case "CFDataBarMoreRules":
          rule = new DatabarConditionalFormattingRule();
          break;
        case "CFTopBottomMoreRules":
          rule = new TopRankedValuesConditionalFormattingRule();
          break;
        case "CFColorScaleMoreRule":
          rule = new TwoColorScaleConditionalFormattingRule();
          break;
        case "CFIconSetsMoreRule":
          rule = new IconSetConditionalFormattingRule(ConditionalFormattingIconSetStyle.ThreeUnrimmedTrafficLights);
          break;
        default:
          break;
      }
      FarPoint.Win.Spread.ConditionalFormattingUtils.CreateConditionalFFormattingForm(this, fpSpread1, rule);
    }

    // (New Rule)
    private void btnNewRules_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ConditionalFormattingUtils.CreateConditionalFFormattingForm
          (this, this.fpSpread1, new FarPoint.Win.Spread.TwoColorScaleConditionalFormattingRule());
    }
    // (Management Rules)
    private void btnManageRules_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ConditionalFormattingUtils.ShowConditionalFormattingManagerForm(this, this.fpSpread1);

    }
    // (Clear select cell)
    private void btnFromSelectedCells_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange range = GetSpreadSelection();
      this.fpSpread1.ActiveSheet.ClearConditionalFormating(range.Row, range.Column, range.RowCount, range.ColumnCount);
    }
    // (Clear All Forms)
    private void btnEntireSheet_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ActiveSheet.ClearConditionalFormatings();
    }
    #endregion
    #endregion

    #region  (Insert tab)
    private void InitializeGroupText()
    {
      this.rbTable.Click += new EventHandler(rbTable_Click);
      this.rbSymbol.Click += new EventHandler(btnSymbol_Click);
      this.rbWordArt.Click += new EventHandler(btnWordArt_Click);
      this.Shape11.Click += new EventHandler(Shapes_Click);
      this.Shape12.Click += new EventHandler(Shapes_Click);
      this.Shape21.Click += new EventHandler(Shapes_Click);
      this.Shape22.Click += new EventHandler(Shapes_Click);
      this.Shape23.Click += new EventHandler(Shapes_Click);
      this.Shape24.Click += new EventHandler(Shapes_Click);
      this.Shape25.Click += new EventHandler(Shapes_Click);
      this.Shape26.Click += new EventHandler(Shapes_Click);
      this.Shape27.Click += new EventHandler(Shapes_Click);
      this.Shape31.Click += new EventHandler(Shapes_Click);
      this.Shape32.Click += new EventHandler(Shapes_Click);
      this.Shape33.Click += new EventHandler(Shapes_Click);
      this.Shape34.Click += new EventHandler(Shapes_Click);
      this.Shape35.Click += new EventHandler(Shapes_Click);
      this.Shape36.Click += new EventHandler(Shapes_Click);
      this.Shape37.Click += new EventHandler(Shapes_Click);
      this.Shape41.Click += new EventHandler(Shapes_Click);
      this.Shape42.Click += new EventHandler(Shapes_Click);
      this.Shape43.Click += new EventHandler(Shapes_Click);
      this.Shape44.Click += new EventHandler(Shapes_Click);
      this.Shape51.Click += new EventHandler(Shapes_Click);
      this.Shape52.Click += new EventHandler(Shapes_Click);
      this.Shape53.Click += new EventHandler(Shapes_Click);
      this.Shape54.Click += new EventHandler(Shapes_Click);
      this.Shape55.Click += new EventHandler(Shapes_Click);
      this.Shape56.Click += new EventHandler(Shapes_Click);
      this.Shape57.Click += new EventHandler(Shapes_Click);
      this.Shape61.Click += new EventHandler(Shapes_Click);
      this.Shape62.Click += new EventHandler(Shapes_Click);
      this.Shape63.Click += new EventHandler(Shapes_Click);
      this.Shape64.Click += new EventHandler(Shapes_Click);
      this.Shape65.Click += new EventHandler(Shapes_Click);

      this.rbDeleteShape.Click += new EventHandler(btnActiveShape_Click);
      this.rbDeleteActiveShape.Click += new EventHandler(btnActiveShape_Click);
      this.rbDeleteAllShape.Click += new EventHandler(btnAllShapes_Click);
    }
    #region  (Text group)
    // Table
    //rbTable_Click
    private void rbTable_Click(object sender, EventArgs e)
    {
      Form frm = TableUtils.ShowTableCreatorForm(this, fpSpread1);
      frm.FormClosed += frm_FormClosed;
    }

    void frm_FormClosed(object sender, FormClosedEventArgs e)
    {
      LoadTableRibbon();
      Form frm = sender as Form;
      frm.FormClosed -= frm_FormClosed;
    }

    // (Special symbols)
    private void btnSymbol_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar dt = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar(this.fpSpread1, new FarPoint.Win.Spread.DrawingSpace.PSContainer());
      dt.ShowSymbolDialog();
    }

    // (WordArt)
    private void btnWordArt_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.TextAndFont tf = new FarPoint.Win.Spread.DrawingSpace.TextAndFont(false);
      if (tf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        this.fpSpread1.ActiveSheet.AddShape(new FarPoint.Win.Spread.DrawingSpace.TextShape() { Font = tf.SelectedFont, Text = tf.TextString });
    }
    #endregion
    #region  (Illustrations group)
    // (Pictures)
    private void btnPicture_Click(object sender, EventArgs e)
    {
      OpenFileDialog dlgFile = new OpenFileDialog();
      FarPoint.Win.Spread.DrawingSpace.RectangleShape RectangleShape = new
          FarPoint.Win.Spread.DrawingSpace.RectangleShape();


      fpSpread1.ActiveSheet.AddShape(RectangleShape, fpSpread1.ActiveSheet.ActiveRowIndex,
          fpSpread1.ActiveSheet.ActiveColumnIndex);

      if (dlgFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        RectangleShape.Picture = Image.FromFile(dlgFile.FileName);
        RectangleShape.CanSize = Sizing.HeightAndWidth;
        //fpSpread1.ActiveSheet.DefaultStyle.BackColor = Color.FromArgb(10);
      }
    }
    // (Comments)
    int flag;
    private void btnAnnotionMode_Click(object sender, EventArgs e)
    {
      if (flag == 0)
      {
        fpSpread1.StartAnnotationMode();
        flag = 1;
      }
      else
      {
        fpSpread1.StopAnnotationMode();
        flag = 0;
      }
    }
    // (Shape)
    private void Shapes_Click(object sender, EventArgs e)
    {
      RibbonGalleryItem b = sender as RibbonGalleryItem;
      switch (b.Name)
      {
        case "Shape11":
          FarPoint.Win.Spread.DrawingSpace.LineShape LineShape1 = new FarPoint.Win.Spread.DrawingSpace.LineShape();
          fpSpread1.ActiveSheet.AddShape(LineShape1, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape12":
          FarPoint.Win.Spread.DrawingSpace.LineShape LineShape2 = new FarPoint.Win.Spread.DrawingSpace.LineShape();
          LineShape2.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
          LineShape2.StartCap = System.Drawing.Drawing2D.LineCap.Flat;
          fpSpread1.ActiveSheet.AddShape(LineShape2, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape21":
          FarPoint.Win.Spread.DrawingSpace.RectangleShape RectangleShape = new FarPoint.Win.Spread.DrawingSpace.RectangleShape();
          fpSpread1.ActiveSheet.AddShape(RectangleShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape22":
          FarPoint.Win.Spread.DrawingSpace.RoundedRectangleShape RoundedRectangleShape = new FarPoint.Win.Spread.DrawingSpace.RoundedRectangleShape();
          fpSpread1.ActiveSheet.AddShape(RoundedRectangleShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape23":
          FarPoint.Win.Spread.DrawingSpace.MultiSideShape MultiSideShape = new FarPoint.Win.Spread.DrawingSpace.MultiSideShape();
          fpSpread1.ActiveSheet.AddShape(MultiSideShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape24":
          FarPoint.Win.Spread.DrawingSpace.EllipseShape EllipseShape = new FarPoint.Win.Spread.DrawingSpace.EllipseShape();
          fpSpread1.ActiveSheet.AddShape(EllipseShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape25":
          FarPoint.Win.Spread.DrawingSpace.TriangleShape TriangleShape = new FarPoint.Win.Spread.DrawingSpace.TriangleShape();
          fpSpread1.ActiveSheet.AddShape(TriangleShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape26":
          FarPoint.Win.Spread.DrawingSpace.FivePointStarShape FivePointStarShape = new FarPoint.Win.Spread.DrawingSpace.FivePointStarShape();
          fpSpread1.ActiveSheet.AddShape(FivePointStarShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape27":
          FarPoint.Win.Spread.DrawingSpace.BurstShape BurstShape = new FarPoint.Win.Spread.DrawingSpace.BurstShape();
          fpSpread1.ActiveSheet.AddShape(BurstShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape31":
          FarPoint.Win.Spread.DrawingSpace.ArrowShape ArrowShape = new FarPoint.Win.Spread.DrawingSpace.ArrowShape();
          fpSpread1.ActiveSheet.AddShape(ArrowShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape32":
          // FarPoint.Win.Spread.DrawingSpace.RectangleShape RectangleShape = new FarPoint.Win.Spread.DrawingSpace.RectangleShape();
          //fpSpread1.ActiveSheet.AddShape(RectangleShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape33":
          FarPoint.Win.Spread.DrawingSpace.TwoWayArrowShape TwoWayArrowShape = new FarPoint.Win.Spread.DrawingSpace.TwoWayArrowShape();
          fpSpread1.ActiveSheet.AddShape(TwoWayArrowShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape34":
          // FarPoint.Win.Spread.DrawingSpace.RectangleShape RectangleShape = new FarPoint.Win.Spread.DrawingSpace.RectangleShape();
          //fpSpread1.ActiveSheet.AddShape(RectangleShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape35":
          FarPoint.Win.Spread.DrawingSpace.ThreeWayArrowShape ThreeWayArrowShape = new FarPoint.Win.Spread.DrawingSpace.ThreeWayArrowShape();
          fpSpread1.ActiveSheet.AddShape(ThreeWayArrowShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape36":
          FarPoint.Win.Spread.DrawingSpace.FourWayArrowShape FourWayArrowShape = new FarPoint.Win.Spread.DrawingSpace.FourWayArrowShape();
          fpSpread1.ActiveSheet.AddShape(FourWayArrowShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape37":
          FarPoint.Win.Spread.DrawingSpace.CalloutArrowShape CalloutArrowShape = new FarPoint.Win.Spread.DrawingSpace.CalloutArrowShape();
          fpSpread1.ActiveSheet.AddShape(CalloutArrowShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape41":
          FarPoint.Win.Spread.DrawingSpace.CaptionBalloonShape CaptionBalloonShape = new FarPoint.Win.Spread.DrawingSpace.CaptionBalloonShape();
          fpSpread1.ActiveSheet.AddShape(CaptionBalloonShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape42":
          FarPoint.Win.Spread.DrawingSpace.ExclamationBalloonShape ExclamationBalloonShape = new FarPoint.Win.Spread.DrawingSpace.ExclamationBalloonShape();
          fpSpread1.ActiveSheet.AddShape(ExclamationBalloonShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape43":
          FarPoint.Win.Spread.DrawingSpace.SquareCaptionBalloonShape SquareCaptionBalloonShape = new FarPoint.Win.Spread.DrawingSpace.SquareCaptionBalloonShape();
          fpSpread1.ActiveSheet.AddShape(SquareCaptionBalloonShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape44":
          FarPoint.Win.Spread.DrawingSpace.ThoughtBalloonShape ThoughtBalloonShape = new FarPoint.Win.Spread.DrawingSpace.ThoughtBalloonShape();
          fpSpread1.ActiveSheet.AddShape(ThoughtBalloonShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape51":
          FarPoint.Win.Spread.DrawingSpace.UniversalNoShape UniversalNoShape = new FarPoint.Win.Spread.DrawingSpace.UniversalNoShape();
          fpSpread1.ActiveSheet.AddShape(UniversalNoShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape52":
          FarPoint.Win.Spread.DrawingSpace.DonutShape DonutShape = new FarPoint.Win.Spread.DrawingSpace.DonutShape();
          fpSpread1.ActiveSheet.AddShape(DonutShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape53":
          FarPoint.Win.Spread.DrawingSpace.ArcShape ArcShape = new FarPoint.Win.Spread.DrawingSpace.ArcShape();
          fpSpread1.ActiveSheet.AddShape(ArcShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape54":
          FarPoint.Win.Spread.DrawingSpace.HeartShape HeartShape = new FarPoint.Win.Spread.DrawingSpace.HeartShape();
          fpSpread1.ActiveSheet.AddShape(HeartShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape55":
          FarPoint.Win.Spread.DrawingSpace.CrescentShape CrescentShape = new FarPoint.Win.Spread.DrawingSpace.CrescentShape();
          fpSpread1.ActiveSheet.AddShape(CrescentShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape56":
          FarPoint.Win.Spread.DrawingSpace.LightningBoltShape LightningBoltShape = new FarPoint.Win.Spread.DrawingSpace.LightningBoltShape();
          fpSpread1.ActiveSheet.AddShape(LightningBoltShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape57":
          FarPoint.Win.Spread.DrawingSpace.BannerShape BannerShape = new FarPoint.Win.Spread.DrawingSpace.BannerShape();
          fpSpread1.ActiveSheet.AddShape(BannerShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape61":
          FarPoint.Win.Spread.DrawingSpace.DiamondShape DiamondShape = new FarPoint.Win.Spread.DrawingSpace.DiamondShape();
          fpSpread1.ActiveSheet.AddShape(DiamondShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape62":
          FarPoint.Win.Spread.DrawingSpace.XShape XShape = new FarPoint.Win.Spread.DrawingSpace.XShape();
          fpSpread1.ActiveSheet.AddShape(XShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape64":
          FarPoint.Win.Spread.DrawingSpace.ChevronShape ChevronShape = new FarPoint.Win.Spread.DrawingSpace.ChevronShape();
          fpSpread1.ActiveSheet.AddShape(ChevronShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape63":
          FarPoint.Win.Spread.DrawingSpace.CheckmarkShape CheckmarkShape = new FarPoint.Win.Spread.DrawingSpace.CheckmarkShape();
          fpSpread1.ActiveSheet.AddShape(CheckmarkShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
        case "Shape65":
          FarPoint.Win.Spread.DrawingSpace.EditorCalloutShape EditorCalloutShape = new FarPoint.Win.Spread.DrawingSpace.EditorCalloutShape();
          fpSpread1.ActiveSheet.AddShape(EditorCalloutShape, fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
          break;
      }

    }
    #endregion
    #region  (Charts group)
    private void InitializeGroupChart()
    {
      this.RibbonButton10.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton11.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton12.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton13.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton14.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton15.Click += new EventHandler(btnAllCharts_Click);
      this.RibbonButton50.Click += new EventHandler(btnAllCharts_Click);

      this.ClusteredColumnChart.Click += new EventHandler(AllCharts);
      this.StackedColumnChart.Click += new EventHandler(AllCharts);
      this.Stacked100ColumnChart.Click += new EventHandler(AllCharts);
      this.HighLowColumnChart.Click += new EventHandler(AllCharts);
      this.ClusteredColumnIn3DChart.Click += new EventHandler(AllCharts);
      this.StackedColumnIn3DChart.Click += new EventHandler(AllCharts);
      this.Stacked100ColumnIn3DChart.Click += new EventHandler(AllCharts);
      this.ColumnIn3DChart.Click += new EventHandler(AllCharts);
      this.ClusteredCylinderChart.Click += new EventHandler(AllCharts);
      this.StackedCylinderChart.Click += new EventHandler(AllCharts);
      this.Stacked100CylinderChart.Click += new EventHandler(AllCharts);
      this.CylinderIn3DChart.Click += new EventHandler(AllCharts);
      this.ClusteredFullConeChart.Click += new EventHandler(AllCharts);
      this.StackedFullConeChart.Click += new EventHandler(AllCharts);
      this.Stacked100FullConeChart.Click += new EventHandler(AllCharts);
      this.FullConeIn3DChart.Click += new EventHandler(AllCharts);
      this.ClusteredFullPyramidChart.Click += new EventHandler(AllCharts);
      this.StackedFullPyramidChart.Click += new EventHandler(AllCharts);
      this.Stacked100FullPyramidChart.Click += new EventHandler(AllCharts);
      this.FullPyramidIn3DChart.Click += new EventHandler(AllCharts);
      // (Line chart)
      this.LineIn2DChart.Click += new EventHandler(AllCharts);
      this.StackedLineChart.Click += new EventHandler(AllCharts);
      this.Stacked100LineChart.Click += new EventHandler(AllCharts);
      this.LineWithMarkersChart.Click += new EventHandler(AllCharts);
      this.StackedLineWithMarkersChart.Click += new EventHandler(AllCharts);
      this.Stacked100LineWithMarkersChart.Click += new EventHandler(AllCharts);
      this.LineIn3DChart.Click += new EventHandler(AllCharts);
      // (Pie)
      this.PieIn2DChart.Click += new EventHandler(AllCharts);
      this.ExplodedPieIn2DChart.Click += new EventHandler(AllCharts);
      this.PieIn3DChart.Click += new EventHandler(AllCharts);
      this.ExplodedPieIn3DChart.Click += new EventHandler(AllCharts);
      // (Bar Chart)
      this.ClusteredBarChart.Click += new EventHandler(AllCharts);
      this.StackedBarChart.Click += new EventHandler(AllCharts);
      this.Stacked100BarChart.Click += new EventHandler(AllCharts);
      this.ClusteredBarIn3DChart.Click += new EventHandler(AllCharts);
      this.StackedBarIn3DChart.Click += new EventHandler(AllCharts);
      this.Stacked100BarIn3DChart.Click += new EventHandler(AllCharts);
      this.ClusteredHorizontalCylinderChart.Click += new EventHandler(AllCharts);
      this.StackedHorizontalCylinderChart.Click += new EventHandler(AllCharts);
      this.Stacked100HorizontalCylinderChart.Click += new EventHandler(AllCharts);
      this.ClusteredHorizontalFullConeChart.Click += new EventHandler(AllCharts);
      this.StackedHorizontalFullConeChart.Click += new EventHandler(AllCharts);
      this.Stacked100HorizontalFullConeChart.Click += new EventHandler(AllCharts);
      this.ClusteredHorizontalFullPyramidChart.Click += new EventHandler(AllCharts);
      this.Stacked100HorizontalFullPyramidChart.Click += new EventHandler(AllCharts);
      this.HighLowBarFullPyramidChart.Click += new EventHandler(AllCharts);
      // (Area Chart)
      this.AreaIn2DChart.Click += new EventHandler(AllCharts);
      this.StackedAreaChart.Click += new EventHandler(AllCharts);
      this.Stacked100AreaChart.Click += new EventHandler(AllCharts);
      this.AreaIn3DChart.Click += new EventHandler(AllCharts);
      this.StackedAreaIn3DChart.Click += new EventHandler(AllCharts);
      this.Stacked100AreaIn3DChart.Click += new EventHandler(AllCharts);
      // (Scatter)
      this.XYPointChart.Click += new EventHandler(AllCharts);
      this.XYLineChart.Click += new EventHandler(AllCharts);
      this.XYLineWithMarkerChart.Click += new EventHandler(AllCharts);
      // (Other Charts)
      this.HighLowCloseChart.Click += new EventHandler(AllCharts);
      this.OpenHighLowCloseChart.Click += new EventHandler(AllCharts);
      this.CandlestickChart.Click += new EventHandler(AllCharts);
      this.XYZSurfaceChart.Click += new EventHandler(AllCharts);
      this.DoughnutChart.Click += new EventHandler(AllCharts);
      this.ExplodedDoughnutChart.Click += new EventHandler(AllCharts);
      this.XYBubbleIn2DChart.Click += new EventHandler(AllCharts);
      this.XYBubbleIn3DChart.Click += new EventHandler(AllCharts);
      this.RadarLineChart.Click += new EventHandler(AllCharts);
      this.RadarLineWithMarkerChart.Click += new EventHandler(AllCharts);
      this.RadarAreaChart.Click += new EventHandler(AllCharts);
    }
    private void btnAllCharts_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Chart.Design.InsertChart ic = new FarPoint.Win.Chart.Design.InsertChart();
      if (ic.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        String chartType = ic.SelectedModel;
        selectChartType(chartType);
      }
    }
    private void selectChartType(String s)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      if (sheet.SelectionCount == 0)
      {
        sheet.AddSelection(0, 0, 1, 1);
      }
      CellRange[] cellRanges = sheet.GetSelections();
      switch (s)
      {
        case "ClusteredColumnChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
                FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;
        case "StackedColumnChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;
        case "Stacked100ColumnChart":
          SpreadChart PercentStackedCol = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          foreach (YPlotArea plotArea in PercentStackedCol.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
            }
          }

          break;
        case "HighLowColumnChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.HighLowBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;

        case "ClusteredColumnIn3DChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          break;
        case "StackedColumnIn3DChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          break;
        case "Stacked100ColumnIn3DChart":
          SpreadChart pswc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          foreach (YPlotArea plotArea in pswc.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
            }
          }
          break;
        case "ColumnIn3DChart":
          fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.BarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;


        case "ClusteredCylinderChart":
          SpreadChart cc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in cc.Model.PlotAreas)
          {
            foreach (ClusteredBarSeries series in plotArea.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "StackedCylinderChart":
          SpreadChart sbs = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in sbs.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {

              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "Stacked100CylinderChart":
          SpreadChart psc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in psc.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "CylinderIn3DChart":
          SpreadChart c3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.BarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in c3.Model.PlotAreas)
          {

            foreach (BarSeries bs in plotArea.Series)
            {
              bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
            }
          }
          break;



        case "ClusteredFullConeChart":
          SpreadChart cf = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in cf.Model.PlotAreas)
          {
            foreach (ClusteredBarSeries series in plotArea.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }

            }
          }

          break;
        case "StackedFullConeChart":
          SpreadChart sf = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in sf.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }
            }
          }
          break;
        case "Stacked100FullConeChart":
          SpreadChart psf = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in psf.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }
            }
          }
          break;
        case "FullConeIn3DChart":
          SpreadChart fc3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.BarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in fc3.Model.PlotAreas)
          {

            foreach (BarSeries bs in plotArea.Series)
            {
              bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
            }

          }
          break;


        case "ClusteredFullPyramidChart":
          SpreadChart cfp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in cfp.Model.PlotAreas)
          {
            foreach (ClusteredBarSeries series in plotArea.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;
        case "StackedFullPyramidChart":
          SpreadChart sfull = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in sfull.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;
        case "Stacked100FullPyramidChart":
          SpreadChart psfp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in psfp.Model.PlotAreas)
          {
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;
        case "FullPyramidIn3DChart":
          SpreadChart fp3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.BarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea plotArea in fp3.Model.PlotAreas)
          {
            foreach (BarSeries series in plotArea.Series)
            {
              series.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
            }
          }
          break;
        // (Line chart)
        case "LineIn2DChart":
          SpreadChart l = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.LineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea plotArea in l.Model.PlotAreas)
          {

            foreach (LineSeries series in plotArea.Series)
            {
              NoMarker nm = new NoMarker();
              series.PointMarker = nm;
            }
          }
          break;
        case "StackedLineChart":
          SpreadChart sl = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea plotArea in sl.Model.PlotAreas)
          {

            foreach (StackedLineSeries series in plotArea.Series)
            {
              foreach (LineSeries lseries in plotArea.Series)
              {
                NoMarker nm = new NoMarker();
                lseries.PointMarker = nm;
              }

            }
          }
          break;
        case "Stacked100LineChart":
          SpreadChart psl = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          foreach (YPlotArea plotArea in psl.Model.PlotAreas)
          {

            foreach (StackedLineSeries series in plotArea.Series)
            {
              series.Percent = true;
              foreach (LineSeries lseries in plotArea.Series)
              {
                NoMarker nm = new NoMarker();
                lseries.PointMarker = nm;
              }
            }
          }
          break;
        case "LineWithMarkersChart":
          SpreadChart lwm = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.LineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "StackedLineWithMarkersChart":
          SpreadChart slwm = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "Stacked100LineWithMarkersChart":
          SpreadChart pslwm = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea plotArea in pslwm.Model.PlotAreas)
          {
            foreach (StackedLineSeries series in plotArea.Series)
            {
              series.Percent = true;
            }
          }
          break;
        case "LineIn3DChart":
          SpreadChart l3d = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.LineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;
        // (Pie)
        case "PieIn2DChart":
          SpreadChart PieIn2D = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "ExplodedPieIn2DChart":
          SpreadChart ep = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (PiePlotArea item in ep.Model.PlotAreas)
          {

            foreach (PieSeries series in item.Series)
            {
              series.PieDetachments.AddRange(new float[] { 0.5F, 0.5F });
            }
          }
          break;
        case "PieIn3DChart":
          SpreadChart PieIn3D = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;
        case "ExplodedPieIn3DChart":
          SpreadChart ep3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (PiePlotArea item in ep3.Model.PlotAreas)
          {

            foreach (PieSeries series in item.Series)
            {
              series.PieDetachments.AddRange(new float[] { 0.5F, 0.5F });
            }
          }
          break;
        // (Bar Chart)
        case "ClusteredBarChart":
          SpreadChart sc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea item in sc.Model.PlotAreas)
          {
            item.Vertical = false;
          }
          break;
        case "StackedBarChart":
          SpreadChart sbar = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea item in sbar.Model.PlotAreas)
          {
            item.Vertical = false;
          }
          break;
        case "Stacked100BarChart":
          SpreadChart psb = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea plotArea in psb.Model.PlotAreas)
          {
            plotArea.Vertical = false;
            foreach (StackedBarSeries series in plotArea.Series)
            {
              series.Percent = true;
            }
          }
          break;
        case "ClusteredBarIn3DChart":
          SpreadChart cbi3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in cbi3.Model.PlotAreas)
          {
            item.Vertical = false;
          }
          break;
        case "StackedBarIn3DChart":
          SpreadChart sbi3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in sbi3.Model.PlotAreas)
          {
            item.Vertical = false;
          }
          break;

        case "Stacked100BarIn3DChart":
          SpreadChart psb3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in psb3.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {
              series.Percent = true;
            }
          }
          break;

        case "ClusteredHorizontalCylinderChart":
          SpreadChart chc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in chc.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (ClusteredBarSeries series in item.Series)
            {

              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "StackedHorizontalCylinderChart":
          SpreadChart shc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in shc.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {

              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "Stacked100HorizontalCylinderChart":
          SpreadChart pshc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in pshc.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.Cylinder;
              }

            }
          }
          break;
        case "ClusteredHorizontalFullConeChart":
          SpreadChart chf = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in chf.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (ClusteredBarSeries series in item.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }
            }
          }
          break;
        case "StackedHorizontalFullConeChart":
          SpreadChart shfc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in shfc.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }
            }
          }
          break;
        case "Stacked100HorizontalFullConeChart":
          SpreadChart pshfc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in pshfc.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullCone;
              }
            }
          }
          break;
        case "ClusteredHorizontalFullPyramidChart":
          SpreadChart chfp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.ClusteredBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in chfp.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (ClusteredBarSeries series in item.Series)
            {

              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;

        case "Stacked100HorizontalFullPyramidChart":
          SpreadChart shfp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in shfp.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {

              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;
        case "HighLowBarFullPyramidChart":
          SpreadChart hbfp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedBarSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in hbfp.Model.PlotAreas)
          {
            item.Vertical = false;
            foreach (StackedBarSeries series in item.Series)
            {
              series.Percent = true;
              foreach (BarSeries bs in series.Series)
              {
                bs.BarShape = FarPoint.Win.Chart.BarShape.FullPyramid;
              }
            }
          }
          break;
        // (Area Chart)
        case "AreaIn2DChart":
          SpreadChart Area = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.AreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "StackedAreaChart":
          SpreadChart sa = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedAreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "Stacked100AreaChart":
          SpreadChart psa = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedAreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (YPlotArea item in psa.Model.PlotAreas)
          {
            foreach (StackedAreaSeries series in item.Series)
            {
              series.Percent = true;
            }
          }
          break;
        case "AreaIn3DChart":
          SpreadChart ai3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.AreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;
        case "StackedAreaIn3DChart":
          SpreadChart sa3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedAreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;
        case "Stacked100AreaIn3DChart":
          SpreadChart Stacked100Area = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.StackedAreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          foreach (YPlotArea item in Stacked100Area.Model.PlotAreas)
          {
            foreach (StackedAreaSeries series in item.Series)
            {
              series.Percent = true;
            }
          }
          break;
        // (Scatter)
        case "XYPointChart":
          SpreadChart xyp = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYPointSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "XYLineChart":
          SpreadChart xyl = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (XYPlotArea item in xyl.Model.PlotAreas)
          {
            foreach (XYLineSeries series in item.Series)
            {
              NoMarker nm = new NoMarker();
              series.PointMarker = nm;
            }
          }
          break;
        case "XYLineWithMarkerChart":
          SpreadChart xylwm = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        // (Other types)
        case "HighLowCloseChart":
          SpreadChart hc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.HighLowCloseSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "OpenHighLowCloseChart":
          SpreadChart ohc = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.OpenHighLowCloseSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "CandlestickChart":
          SpreadChart cs = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.CandlestickSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);

          break;
        case "XYZSurfaceChart":
          SpreadChart surf = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYZSurfaceSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);

          break;
        case "DoughnutChart":
          SpreadChart d = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (PiePlotArea item in d.Model.PlotAreas)
          {
            item.HoleSize = 0.7f;
          }
          break;
        case "ExplodedDoughnutChart":
          SpreadChart ed = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.PieSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (PiePlotArea item in ed.Model.PlotAreas)
          {
            item.HoleSize = 0.7f;
            foreach (PieSeries series in item.Series)
            {
              series.PieDetachments.AddRange(new float[] { 0.5F, 0.5F });
            }
          }
          break;
        case "XYBubbleIn2DChart":
          SpreadChart bi2 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYBubbleSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;
        case "XYBubbleIn3DChart":
          SpreadChart bi3 = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.XYBubbleSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View3D, true);
          break;
        case "RadarLineChart":
          SpreadChart rl = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.RadarLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          foreach (RadarPlotArea item in rl.Model.PlotAreas)
          {
            foreach (RadarLineSeries series in item.Series)
            {
              NoMarker nm = new NoMarker();
              series.PointMarker = nm;
            }
          }
          break;
        case "RadarLineWithMarkerChart":
          SpreadChart rwm = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.RadarLineSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;
        case "RadarAreaChart":
          SpreadChart ra = fpSpread1.ActiveSheet.AddChart(cellRanges, typeof(FarPoint.Win.Chart.RadarAreaSeries), 400, 400, 0, 0,
              FarPoint.Win.Chart.ChartViewType.View2D, true);
          break;
      }
    }
    private void AllCharts(object sender, EventArgs e)
    {
      RibbonGalleryItem b = sender as RibbonGalleryItem;
      selectChartType(b.Name);
    }
    #endregion
    #region  (Delete)
    private void btnActiveShape_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("DeleteActive"));
    }
    private void btnAllShapes_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("DeleteAll"));
    }
    #endregion
    #region  (Waveform group)
    private void btnLineChart_Click(object sender, EventArgs e)
    {
      Form dlg = ExcelSparklineGroup.ShowSelectDataForm(this, this.fpSpread1, SparklineType.Line);
      dlg.FormClosing += sparkLineDlg_FormClosing;
    }
    private void btnColumnChart_Click(object sender, EventArgs e)
    {
      Form dlg = ExcelSparklineGroup.ShowSelectDataForm(this, this.fpSpread1, SparklineType.Column);
      dlg.FormClosing += sparkLineDlg_FormClosing;
    }
    private void btnWinLoss_Click(object sender, EventArgs e)
    {
      Form dlg = ExcelSparklineGroup.ShowSelectDataForm(this, this.fpSpread1, SparklineType.Winloss);
      dlg.FormClosing += sparkLineDlg_FormClosing;
    }

    void sparkLineDlg_FormClosing(object sender, FormClosingEventArgs e)
    {
      Form frm = (Form)sender;
      if (frm.DialogResult == System.Windows.Forms.DialogResult.OK)
      {
        rtSparkline.Visible = true;
        c1Ribbon1.SelectedTab = rtSparkline;
        sparkline_SelectionChanged();
      }
    }

    #endregion
    #region  (Camera Graphics Group)
    private void btnCameraShape_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
      {
        FarPoint.Win.Spread.DrawingSpace.SpreadCameraShape test = new
            FarPoint.Win.Spread.DrawingSpace.SpreadCameraShape(sheet, range);
        test.Location = new System.Drawing.Point(0, 0);
        fpSpread1.ActiveSheet.AddShape(test);
      }
    }
    #endregion
    #endregion

    #region  (Page Layout tab)
    private void InitializePageLayoutTab()
    {
      this.rbOrientationNormal.Click += new EventHandler(btnAuto_Click);
      this.rbOrientationPortraint.Click += new EventHandler(btnPortrait_Click);
      this.rbOrientationLandscape.Click += new EventHandler(btnlandscape_Click);
      this.rbMarginNormal.Click += new EventHandler(ItemNormal_Click);
      this.rbMarginNarrow.Click += new EventHandler(ItemNarrow_Click);
      this.rbMarginWide.Click += new EventHandler(ItemWide_Click);
      this.rbPrintAreaSet.Click += new EventHandler(btnSet_Click);
      this.rbPrintAreaClear.Click += new EventHandler(btnClear_Click);
      this.btnCustomMargins.Click += new EventHandler(btnCustomMargins_Click);

    }
    // (Automatic orientation)
    private void btnAuto_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      printset.Orientation = FarPoint.Win.Spread.PrintOrientation.Auto;
      fpSpread1.ActiveSheet.PrintInfo = printset;

    }
    // (Portrait orientation)
    private void btnPortrait_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      printset.Orientation = FarPoint.Win.Spread.PrintOrientation.Portrait;
      fpSpread1.ActiveSheet.PrintInfo = printset;
    }
    // (Landscape orientation)
    private void btnlandscape_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      printset.Orientation = FarPoint.Win.Spread.PrintOrientation.Landscape;
      fpSpread1.ActiveSheet.PrintInfo = printset;
    }
    // (Background)
    private void btnBackgroud_Click(object sender, EventArgs e)
    {
      OpenFileDialog dlgFile = new OpenFileDialog();
      if (dlgFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        fpSpread1.BackgroundImage = Image.FromFile(dlgFile.FileName);
        fpSpread1.ActiveSheet.DefaultStyle.BackColor = Color.FromArgb(0, 255, 245, 255);
      }
    }
    private void btnPrintTitles_Click(object sender, EventArgs e)
    {
      fpSpread1.ShowPageSetup(this.fpSpread1.ActiveSheet, true, true);
    }
    // (Normal margins)
    private void ItemNormal_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      FarPoint.Win.Spread.PrintMargin printmar = new FarPoint.Win.Spread.PrintMargin();
      printmar.Left = 70;
      printmar.Right = 70;
      printmar.Top = 75;
      printmar.Bottom = 75;

      printset.Margin = printmar;
      fpSpread1.ActiveSheet.PrintInfo = printset;


    }
    // (Narrow distance)
    private void ItemNarrow_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      FarPoint.Win.Spread.PrintMargin printmar = new FarPoint.Win.Spread.PrintMargin();

      printmar.Left = 25;
      printmar.Right = 25;
      printmar.Top = 75;
      printmar.Bottom = 75;

      // printset.Centering = FarPoint.Win.Spread.Centering.Both;
      // printset.FirstPageNumber = 1;
      printset.Margin = printmar;
      fpSpread1.ActiveSheet.PrintInfo = printset;
    }
    // (Wide margins)
    private void ItemWide_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      FarPoint.Win.Spread.PrintMargin printmar = new FarPoint.Win.Spread.PrintMargin();

      printmar.Left = 100;
      printmar.Right = 100;
      printmar.Top = 100;
      printmar.Bottom = 100;

      // printset.Centering = FarPoint.Win.Spread.Centering.Both;
      // printset.FirstPageNumber = 1;
      printset.Margin = printmar;
      fpSpread1.ActiveSheet.PrintInfo = printset;
    }
    // (Custom Margins)
    private void btnCustomMargins_Click(object sender, EventArgs e)
    {
      PrintOptionsDlg dlgPrint = new PrintOptionsDlg(fpSpread1, 5);
      dlgPrint.ShowDialog();
    }
    // (Set Print Area)
    private void btnSet_Click(object sender, EventArgs e)
    {
      //  (Creating a print settings object)
      FarPoint.Win.Spread.PrintInfo printset;
      if (fpSpread1.ActiveSheet.PrintInfo == null)
      {
        printset = new FarPoint.Win.Spread.PrintInfo();
      }
      else
      {
        printset = fpSpread1.ActiveSheet.PrintInfo;
      }
      SheetView sheet = this.fpSpread1.ActiveSheet;

      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges.Length == 0)
      {
        MessageBox.Show("Unchecked area");
        return;
      }
      //  (Allow the print data)
      printset.ColStart = cellRanges[0].Column;
      printset.ColEnd = cellRanges[cellRanges.Length - 1].Column;
      printset.RowStart = cellRanges[0].Row;
      printset.RowEnd = cellRanges[cellRanges.Length - 1].Row;
      printset.PrintType = FarPoint.Win.Spread.PrintType.CellRange;

      fpSpread1.ActiveSheet.PrintInfo = printset;
      rbPrintAreaClear.Enabled = true;
    }
    // (Print Area Clear)
    private void btnClear_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.PrintInfo printset = fpSpread1.ActiveSheet.PrintInfo;

      printset.ColStart = -1;
      printset.ColEnd = -1;
      printset.RowStart = -1;
      printset.RowEnd = -1;
      printset.PrintType = FarPoint.Win.Spread.PrintType.All;
      rbPrintAreaClear.Enabled = false;
    }
    // (Smart Print)
    private void btnSmartPrint_Click(object sender, EventArgs e)
    {
      PrintOptionsDlg dlgPrint = new PrintOptionsDlg(fpSpread1, 1);
      dlgPrint.ShowDialog();
    }
    #endregion

    #region  (Data tab)
    // (Name Manager)
    private void btnNameManager_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ShowListCustomNameForm(this);
    }
    // (Sort)
    private void btnSortData_Click(object sender, EventArgs e)
    {
      SortDlg dlgSort = new SortDlg(fpSpread1);
      if (dlgSort != null) dlgSort.ShowDialog();
    }

    private void btnSortAtoZ_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange cr = sheet.GetSelection(0);
      if (cr != null)
        sheet.SortRange(cr.Row, cr.Column, cr.RowCount, cr.ColumnCount, true, new SortInfo[] { new SortInfo(sheet.ActiveColumnIndex, true) });
    }

    private void btnSortZtoA_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange cr = sheet.GetSelection(0);
      if (cr != null)
        sheet.SortRange(cr.Row, cr.Column, cr.RowCount, cr.ColumnCount, true, new SortInfo[] { new SortInfo(sheet.ActiveColumnIndex, false) });
    }

    private void rbGroup_Click(object sender, EventArgs e)
    {
      try
      {
        SheetView sheet = this.fpSpread1.ActiveSheet;
        CellRange cr = sheet.GetSelection(0);
        if (cr.Column == -1 && cr.Row == -1)
        {
        }
        else if (cr.Column == -1)
        {
          sheet.AddRangeGroup(cr.Row, cr.RowCount, true);
        }
        else if (cr.Row == -1)
        {
          sheet.AddRangeGroup(cr.Column, cr.ColumnCount, false);
        }
        else
        {
          MessageBox.Show("Please select an entire row or column grouping");
        }
        this.rbUnGroup.Enabled = true;
        this.rbCollapseGroup.Enabled = true;
        this.rbExpandGroup.Enabled = true;
      }
      catch (System.Exception ex)
      {
      }

    }

    private void rbUnGroup_Click(object sender, EventArgs e)
    {
      if (this.rbUnGroup.Enabled)
      {
        try
        {
          SheetView sheet = this.fpSpread1.ActiveSheet;
          CellRange cr = sheet.GetSelection(0);
          if (cr.Column == -1 && cr.Row == -1)
          {
          }
          else if (cr.Column == -1)
          {
            sheet.RemoveRangeGroup(cr.Row, cr.RowCount, true);
            if ((sheet.GetRangeGroupLevels(true) == 0) && (sheet.GetRangeGroupLevels(false) == 0))
            {
              this.rbUnGroup.Enabled = false;
              this.rbCollapseGroup.Enabled = false;
              this.rbExpandGroup.Enabled = false;
            }
          }
          else if (cr.Row == -1)
          {
            sheet.RemoveRangeGroup(cr.Column, cr.ColumnCount, false);
            if ((sheet.GetRangeGroupLevels(true) == 0) && (sheet.GetRangeGroupLevels(false) == 0))
            {
              this.rbUnGroup.Enabled = false;
              this.rbCollapseGroup.Enabled = false;
              this.rbExpandGroup.Enabled = false;
            }
          }
          else
          {
            MessageBox.Show("Please select an entire row or column grouping range cancel grouping");
          }
        }
        catch (System.Exception ex)
        {
        }

      }
    }

    private void rbExpandGroup_Click(object sender, EventArgs e)
    {
      if (this.rbExpandGroup.Enabled)
      {
        try
        {
          SheetView sheet = this.fpSpread1.ActiveSheet;
          CellRange cr = sheet.GetSelection(0);
          if (cr.Column == -1 && cr.Row == -1)
          {
          }
          else if (cr.Column == -1)
          {
            for (int i = cr.Row; i < cr.Row + cr.RowCount; i++)
            {
              int level = sheet.GetRangeGroupLevels(true);
              for (int j = 0; j < level; j++)
              {
                RangeGroupInfo[] groupInfo = sheet.GetRangeGroupInfo(j, true);
                if (groupInfo != null)
                {
                  foreach (RangeGroupInfo rgi in groupInfo)
                    if (i >= rgi.Start && i <= rgi.End)
                      sheet.ExpandRangeGroup(rgi, true, true);
                }
              }
            }
          }
          else if (cr.Row == -1)
          {
            for (int i = cr.Column; i < cr.Column + cr.ColumnCount; i++)
            {
              int level = sheet.GetRangeGroupLevels(false);
              for (int j = 0; j < level; j++)
              {
                RangeGroupInfo[] groupInfo = sheet.GetRangeGroupInfo(j, false);
                if (groupInfo != null)
                {
                  foreach (RangeGroupInfo rgi in groupInfo)
                    if (i >= rgi.Start && i <= rgi.End)
                      sheet.ExpandRangeGroup(rgi, false, true);
                }
              }
            }
          }
          else
          {
            MessageBox.Show("Please select an entire row or column grouping ranges are expanded grouping operation");
          }
        }
        catch (System.Exception ex)
        {
        }
      }
    }

    private void rbCollapseGroup_Click(object sender, EventArgs e)
    {
      if (this.rbCollapseGroup.Enabled)
      {
        try
        {
          SheetView sheet = this.fpSpread1.ActiveSheet;
          CellRange cr = sheet.GetSelection(0);
          if (cr.Column == -1 && cr.Row == -1) // sheet selection
          {
          }
          else if (cr.Column == -1) // row selection
          {
            for (int i = cr.Row; i < cr.Row + cr.RowCount; i++)
            {
              int level = sheet.GetRangeGroupLevels(true);
              for (int j = 0; j < level; j++)
              {
                RangeGroupInfo[] groupInfo = sheet.GetRangeGroupInfo(j, true);
                if (groupInfo != null)
                {
                  foreach (RangeGroupInfo rgi in groupInfo)
                    if (i >= rgi.Start && i <= rgi.End)
                      sheet.ExpandRangeGroup(rgi, true, false);
                }
              }
            }
          }
          else if (cr.Row == -1)
          {
            for (int i = cr.Column; i < cr.Column + cr.ColumnCount; i++)
            {
              int level = sheet.GetRangeGroupLevels(false);
              for (int j = 0; j < level; j++)
              {
                RangeGroupInfo[] groupInfo = sheet.GetRangeGroupInfo(j, false);
                if (groupInfo != null)
                {
                  foreach (RangeGroupInfo rgi in groupInfo)
                    if (i >= rgi.Start && i <= rgi.End)
                      sheet.ExpandRangeGroup(rgi, false, false);
                }
              }
            }
          }
          else
          {
            MessageBox.Show("Please select an entire row or column grouping range folding grouping");
          }
        }
        catch (System.Exception ex)
        {
        }
      }
    }

    #endregion

    #region  (View tab)
    private void InitializeViewTab()
    {
      this.rbFreezeRowsAndColumns.Click += new EventHandler(FreezePanes_Click);
      this.rbFreezeTopRow.Click += new EventHandler(FreezeTopRow_Click);
      this.rbFreezeFirstColumn.Click += new EventHandler(FreezeFirstColumn_Click);
      this.rbFreezeTrailingRowsAndColumns.Click += new EventHandler(FreezePanesFromTrailing_Click);
    }
    private void btnZoom_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.ZoomDlg dlgZoom = new FarPoint.Win.Spread.Design.ZoomDlg();
      if (dlgZoom.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        fpSpread1.ActiveSheet.ZoomFactor = dlgZoom.ZoomFactor;
    }
    private void CheckBoxRowHeader_CheckedChanged(object sender, EventArgs e)
    {

      fpSpread1.ActiveSheet.RowHeader.Visible = !fpSpread1.ActiveSheet.RowHeader.Visible;
    }

    private void CheckBoxColumnHeader_CheckedChanged(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.ColumnHeader.Visible = !fpSpread1.ActiveSheet.ColumnHeader.Visible;
    }

    private void CheckBoxVerticalGridLine_CheckedChanged(object sender, EventArgs e)
    {
      if (rbVerticalGridLine.Checked == false)

        this.fpSpread1.ActiveSheet.VerticalGridLine = new GridLine(GridLineType.None);
      else

        this.fpSpread1.ActiveSheet.VerticalGridLine = new GridLine(GridLineType.Flat);

    }

    private void CheckBoxHorizontalGridLine_CheckedChanged(object sender, EventArgs e)
    {
      if (rbHorizontalGridLine.Checked == false)
        this.fpSpread1.ActiveSheet.HorizontalGridLine = new GridLine(GridLineType.None);
      else
        this.fpSpread1.ActiveSheet.HorizontalGridLine = new GridLine(GridLineType.Flat);

    }
    // (Bar public)
    private void CheckBoxFormulaBar_CheckedChanged(object sender, EventArgs e)
    {
      if (panel2.Visible == false)
      {
        FormulaPanel.Visible = true;
      }
      else
      {
        FormulaPanel.Visible = false;
      }
    }

    private void btn100_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.ZoomFactor = 1f;
    }
    // (Zoom to selected area)
    private void btnZoomToSelection_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.ZoomFactor = 4.0f;
      int activePaneRow = fpSpread1.GetActiveRowViewportIndex();
      int activePaneColumn = fpSpread1.GetActiveColumnViewportIndex();
      fpSpread1.SetViewportLeftColumn(activePaneColumn, fpSpread1.ActiveSheet.ActiveColumnIndex);
      fpSpread1.SetViewportTopRow(activePaneRow, fpSpread1.ActiveSheet.ActiveRowIndex);
    }
    private void FreezePanes_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      fpSpread1.ActiveSheet.FrozenColumnCount = cellRanges[0].Column > 0 ? cellRanges[0].Column : 0;
      fpSpread1.ActiveSheet.FrozenRowCount = cellRanges[0].Row > 0 ? cellRanges[0].Row : 0;

    }
    private void FreezeTopRow_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.FrozenRowCount = 1;
    }
    private void FreezeFirstColumn_Click(object sender, EventArgs e)
    {
      fpSpread1.ActiveSheet.FrozenColumnCount = 1;
    }
    private void FreezePanesFromTrailing_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      sheet.FrozenTrailingColumnCount = sheet.ColumnCount - cellRanges[cellRanges.Length - 1].Column;
      sheet.FrozenTrailingRowCount = sheet.RowCount - cellRanges[cellRanges.Length - 1].Row;
    }
    #endregion

    #region  (Settings tab)
    // (Spread Settings)
    private void btnGeneral1_Click(object sender, EventArgs e)
    {
      //FarPoint.Win.Spread.Design.SpreadOptionsDlg dlgSpreadOptions = new FarPoint.Win.Spread.Design.SpreadOptionsDlg(fpSpread);
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 2);
      dlgSpreadOptions.ShowDialog();
    }
    private void btnSplitBox1_Click(object sender, EventArgs e)
    {
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 3);
      dlgSpreadOptions.ShowDialog();
    }

    private void btnEdit1_Click(object sender, EventArgs e)
    {
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 0);
      dlgSpreadOptions.ShowDialog();
    }

    private void btnView1_Click(object sender, EventArgs e)
    {
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 4);
      dlgSpreadOptions.ShowDialog();
    }
    private void btnScrollBar1_Click(object sender, EventArgs e)
    {
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 1);
      dlgSpreadOptions.ShowDialog();
    }

    private void btnTitles1_Click(object sender, EventArgs e)
    {
      SpreadOptionsDlg dlgSpreadOptions = new SpreadOptionsDlg(fpSpread1, 5);
      dlgSpreadOptions.ShowDialog();
    }
    // (Sheet Set)
    private void btnGeneralChart1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 0);
      sheetdlg.ShowDialog();
    }
    private void btnGridLines1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 3);
      sheetdlg.ShowDialog();
    }

    private void btnColors1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 1);
      sheetdlg.ShowDialog();
    }

    private void btnCalculation1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 4);
      sheetdlg.ShowDialog();
    }

    private void btnHeaders1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 2);
      sheetdlg.ShowDialog();
    }

    private void btnFonts1_Click(object sender, EventArgs e)
    {
      SheetSettingsDlgV3 sheetdlg = new SheetSettingsDlgV3
          (new FarPoint.Win.Spread.Design.DesignerMain(fpSpread1), 5);
      sheetdlg.ShowDialog();
    }
    // (Appearance settings)
    private void btnSpreadSkins1_Click(object sender, EventArgs e)
    {
      SpreadSkinsForm frmSpreadSkins = new SpreadSkinsForm(fpSpread1);
      frmSpreadSkins.ShowDialog();
    }
    private void btnSheetSkins1_Click(object sender, EventArgs e)
    {
      SheetSkinsForm frmSheetSkins = new SheetSkinsForm(fpSpread1.ActiveSheet);
      frmSheetSkins.ShowDialog();
    }
    private void btnFocusIndicator1_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      FarPoint.Win.Spread.Design.FocusEditor frmDocusEditor = new FarPoint.Win.Spread.Design.FocusEditor();

      if (frmDocusEditor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        fpSpread1.FocusRenderer = frmDocusEditor.FocusRenderer;
      }
    }
    private void btnStyle1_Click(object sender, EventArgs e)
    {
      fpSpread1.ShowCollectionEditor(fpSpread1, CollectionEditorOptions.NamedStyles);

    }
    private void btnTabScript1_Click(object sender, EventArgs e)
    {
      TabStripDlg tabDlg = new TabStripDlg(fpSpread1);
      tabDlg.ShowDialog();

    }
    private void btnAlternatingRow1_Click(object sender, EventArgs e)
    {
      fpSpread1.ShowCollectionEditor(fpSpread1.ActiveSheet, CollectionEditorOptions.AlternatingRow);
    }
    // (Other settings)
    private void btnGroupInfo1_Click(object sender, EventArgs e)
    {
      fpSpread1.ShowCollectionEditor(fpSpread1.ActiveSheet, CollectionEditorOptions.GroupInfo);
    }

    private void btnCellsColumns1_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.CellsEditorForm frmCellsEditor = new CellsEditorForm(fpSpread1.ActiveSheet);
      frmCellsEditor.ShowDialog();
    }

    private void btnInputMap1_Click(object sender, EventArgs e)
    {
      this.fpSpread1.ShowInputMapEditor(this);
    }


    #endregion

    #region  (Drawing tools tab)
    private void InitializeDrawingToolsTab()
    {
      this.RibbonColorChooser1.Click += new EventHandler(ColorPickerBackColor_Click);
      this.RibbonColorChooser1.SelectedColorChanged += new EventHandler(ColorPickerBackColor_SelectedColorChanged);
      this.RibbonColorChooser3.SelectedColorChanged += new EventHandler(ColorPickerOutLineColor_SelectedColorChanged);
      this.RibbonColorChooser2.SelectedColorChanged += new EventHandler(ColorPickerForeColor_SelectedColorChanged);
      this.rbShapeThickNone.Click += new EventHandler(ShapeStyle);
      this.RibbonButton159.Click += new EventHandler(ShapeStyle);
      this.RibbonButton160.Click += new EventHandler(ShapeStyle);
      this.RibbonButton161.Click += new EventHandler(ShapeStyle);
      this.RibbonButton162.Click += new EventHandler(ShapeStyle);
      this.RibbonButton163.Click += new EventHandler(ShapeStyle);
      this.RibbonButton164.Click += new EventHandler(ShapeStyle);
      this.rbShapeThickCustom.Click += new EventHandler(ShapeStyle);
      this.rbShapeOutlineSolid.Click += new EventHandler(ShapeStyle);
      this.rbShapeOutlineDash.Click += new EventHandler(ShapeStyle);
      this.rbShapeOutlineDot.Click += new EventHandler(ShapeStyle);
      this.rbShapeOutlineDashDot.Click += new EventHandler(ShapeStyle);
      this.rbShapeOutlineDashDotDot.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowNone.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowRight.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowBottomRight.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowBottom.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowBottomLeft.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowLeft.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowTopLeft.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowTop.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowTopRight.Click += new EventHandler(ShapeStyle);
      this.rbShapeShadowCustom.Click += new EventHandler(ShapeStyle);
      this.btnRotate1.Click += new EventHandler(ShapeStyle);
      this.btnRotate5.Click += new EventHandler(ShapeStyle);
      this.btnRotate45.Click += new EventHandler(ShapeStyle);
      this.btnRotate90.Click += new EventHandler(ShapeStyle);
      this.btnRotate180.Click += new EventHandler(ShapeStyle);
      this.btnRotate270.Click += new EventHandler(ShapeStyle);
      this.RibbonButton189.Click += new EventHandler(ShapeStyle);
      this.RibbonButton188.Click += new EventHandler(ShapeStyle);
      this.btnScale10.Click += new EventHandler(ShapeStyle);
      this.btnScale25.Click += new EventHandler(ShapeStyle);
      this.btnScale50.Click += new EventHandler(ShapeStyle);
      this.btnScale75.Click += new EventHandler(ShapeStyle);
      this.btnScale150.Click += new EventHandler(ShapeStyle);
      this.btnScale200.Click += new EventHandler(ShapeStyle);
      this.btnScale300.Click += new EventHandler(ShapeStyle);
      this.RibbonButton144.Click += new EventHandler(ShapeStyle);
      this.RibbonButton147.Click += new EventHandler(ShapeStyle);
      this.RibbonButton150.Click += new EventHandler(ShapeStyle);
      this.RibbonButton151.Click += new EventHandler(ShapeStyle);
      this.RibbonButton146.Click += new EventHandler(ShapeStyle);
      this.RibbonButton149.Click += new EventHandler(ShapeStyle);
      this.RibbonButton152.Click += new EventHandler(ShapeStyle);
      this.RibbonButton145.Click += new EventHandler(ShapeStyle);
      this.RibbonButton180.Click += new EventHandler(ShapeStyle);
      this.RibbonButton187.Click += new EventHandler(ShapeStyle);
      this.RibbonButton197.Click += new EventHandler(ShapeStyle);
      this.rbClearPicture.Click += new EventHandler(btnClearPicture_Click);
    }

    #region  (Graphic layout)

    private void btnSetPicture_Click(object sender, EventArgs e)
    {
      OpenFileDialog dlgFile = new OpenFileDialog();
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as
          FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (dlgFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        if (pso != null)
          pso.Picture = Image.FromFile(dlgFile.FileName);
        else
        {
          fpSpread1.BackgroundImage = Image.FromFile(dlgFile.FileName);
          //fpSpread1.ActiveSheet.DefaultStyle.BackColor = Color.FromArgb(10);
        }
      }
      rbClearPicture.Enabled = true;
    }
    private void btnClearPicture_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as
          FarPoint.Win.Spread.DrawingSpace.PSShape;

      if (pso != null)
        pso.Picture = null;
      else
        fpSpread1.BackgroundImage = null;
      rbClearPicture.Enabled = false;
    }
    // (Send to back)
    private void btnSendToBack1_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("SendToBack"));
    }
    // (Bring to front)
    private void btnBringToFront1_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("BringToFront"));
    }

    #endregion
    #region  (Graphic Styles)
    private void ColorPickerBackColor_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.BackColor = this.RibbonColorChooser1.Color;

    }
    private void ColorPickerBackColor_SelectedColorChanged(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.BackColor = this.RibbonColorChooser1.Color;
    }
    private void ColorPickerOutLineColor_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.ShapeOutlineColor = this.RibbonColorChooser3.Color;
    }
    private void ColorPickerOutLineColor_SelectedColorChanged(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.ShapeOutlineColor = this.RibbonColorChooser3.Color;
    }
    private void ColorPickerForeColor_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.ForeColor = this.RibbonColorChooser2.Color;
    }
    private void ColorPickerForeColor_SelectedColorChanged(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      if (pso != null) pso.ForeColor = this.RibbonColorChooser2.Color;
    }
    private void ShapeStyle(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.PSShape pso = fpSpread1.ActiveWindowlessObject as FarPoint.Win.Spread.DrawingSpace.PSShape;
      RibbonButton btn = sender as RibbonButton;
      if (pso != null)
      {
        switch (btn.Name)
        {
          case "rbShapeThickNone":
            pso.ShapeOutlineThickness = 0;
            break;
          case "btnThickness1":
            pso.ShapeOutlineThickness = 1;
            break;
          case "btnThickness2":
            pso.ShapeOutlineThickness = 2;
            break;
          case "btnThickness3":
            pso.ShapeOutlineThickness = 3;
            break;
          case "btnThickness4":
            pso.ShapeOutlineThickness = 4;
            break;
          case "btnThickness5":
            pso.ShapeOutlineThickness = 5;
            break;
          case "btnThickness10":
            pso.ShapeOutlineThickness = 10;
            break;
          case "rbShapeThickCustom":
            {
              TextInput input = new TextInput();
              input.TextInputInputBox.Left = input.TextInputInputBox.Left + 120;
              input.TextInputInputBox.Width = input.TextInputInputBox.Width - 120;
              input.Text = "Outline Thickness";
              input.TextInputPrompt.Text = "Enter the thickness for the object outline.";
              float oldShapeOutlineThickness = pso.ShapeOutlineThickness;
              float shapeOutlineThickness;
              input.TextInputInputBox.Text = oldShapeOutlineThickness.ToString("#0");
              input.NumbersOnly = true;
              if (input.ShowDialog(this) == DialogResult.OK)
              {
                try
                {
                  // Make sure input is numeric and float
                  shapeOutlineThickness = Convert.ToSingle(input.TextInputInputBox.Text);
                }
                catch (OverflowException)
                {
                  shapeOutlineThickness = 0.0F;
                }
                catch (InvalidCastException)
                {
                  shapeOutlineThickness = 0.0F;
                }
                catch (FormatException)
                {
                  return;
                }
                pso.ShapeOutlineThickness = shapeOutlineThickness;
              }
            }
            break;
          case "rbShapeOutlineSolid":
            pso.ShapeOutlineStyle = DashStyle.Solid;
            break;
          case "rbShapeOutlineDash":
            pso.ShapeOutlineStyle = DashStyle.Dash;
            break;
          case "rbShapeOutlineDot":
            pso.ShapeOutlineStyle = DashStyle.Dot;
            break;
          case "rbShapeOutlineDashDot":
            pso.ShapeOutlineStyle = DashStyle.DashDot;
            break;
          case "rbShapeOutlineDashDotDot":
            pso.ShapeOutlineStyle = DashStyle.DashDotDot;
            break;
          case "rbShapeShadowNone":
            pso.ShadowDirection = ShadowDirection.None;
            break;
          case "rbShapeShadowRight":
            pso.ShadowDirection = ShadowDirection.Right;
            break;
          case "rbShapeShadowBottomRight":
            pso.ShadowDirection = ShadowDirection.BottomRight;
            break;
          case "rbShapeShadowBottom":
            pso.ShadowDirection = ShadowDirection.Bottom;
            break;
          case "rbShapeShadowBottomLeft":
            pso.ShadowDirection = ShadowDirection.BottomLeft;
            break;
          case "rbShapeShadowLeft":
            pso.ShadowDirection = ShadowDirection.Left;
            break;
          case "rbShapeShadowTopLeft":
            pso.ShadowDirection = ShadowDirection.TopLeft;
            break;
          case "rbShapeShadowTop":
            pso.ShadowDirection = ShadowDirection.Top;
            break;
          case "rbShapeShadowTopRight":
            pso.ShadowDirection = ShadowDirection.TopRight;
            break;
          case "rbShapeShadowCustom":
            {
              FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
              draw.OwningControl = fpSpread1;
              draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
              draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("DropShadow", "Custom..."));
            }
            break;
          case "btnRotate1":
            pso.RotationAngle += 1;
            break;
          case "btnRotate5":
            pso.RotationAngle += 5;
            break;
          case "btnRotate45":
            pso.RotationAngle += 45;
            break;
          case "btnRotate90":
            pso.RotationAngle += 90;
            break;
          case "btnRotate180":
            pso.RotationAngle += 180;
            break;
          case "btnRotate270":
            pso.RotationAngle += 270;
            break;
          case "bntRotateReset":
            pso.RotationAngle = 0;
            break;
          case "btnRotateCustom":
            {
              TextInput input = new TextInput();
              input.TextInputInputBox.Left = input.TextInputInputBox.Left + 120;
              input.TextInputInputBox.Width = input.TextInputInputBox.Width - 120;
              input.Text = "Rotation Amount";
              input.TextInputPrompt.Text = "Enter the number of degrees to rotate the object.";
              float oldRotateValue = pso.RotationAngle;
              float rotateValue;
              input.TextInputInputBox.Text = oldRotateValue.ToString("#0");
              input.NumbersOnly = true;
              if (input.ShowDialog(this) == DialogResult.OK)
              {
                try
                {
                  // Make sure input is numeric and float
                  rotateValue = Convert.ToSingle(input.TextInputInputBox.Text);
                }
                catch (OverflowException)
                {
                  rotateValue = 0.0F;
                }
                catch (InvalidCastException)
                {
                  rotateValue = 0.0F;
                }
                catch (FormatException)
                {
                  return;
                }
                pso.Rotate(rotateValue - oldRotateValue);
              }
            }

            break;
          case "btnFlipV":
            pso.FlipShapeVertical();
            break;
          case "btnFlipH":
            pso.FlipShapeHorizontal();
            break;
          case "btnScale10":
            pso.Scale(0.1f, 0.1f);
            break;
          case "btnScale25":
            pso.Scale(0.25f, 0.25f);
            break;
          case "btnScale50":
            pso.Scale(0.5f, 0.5f);
            break;
          case "btnScale75":
            pso.Scale(0.75f, 0.75f);
            break;
          case "btnScale150":
            pso.Scale(1.5f, 1.5f);
            break;
          case "btnScale200":
            pso.Scale(2f, 2f);
            break;
          case "btnScale300":
            pso.Scale(3f, 3f);
            break;
          case "btnScaleCustom":
            {
              float scaleValue;

              // Create input dialog to retrieve scale amount
              TextInput input = new TextInput();
              // Adjust input textbox
              input.TextInputInputBox.Left = input.TextInputInputBox.Left + 120;
              input.TextInputInputBox.Width = input.TextInputInputBox.Width - 120;
              // Set label and prompt text

              input.Text = "Size Scaling Percentage";
              input.TextInputPrompt.Text = "Enter the percentage to scale the object (i.e. 200 for 200% or double size).";

              input.TextInputInputBox.Text = "100";

              input.NumbersOnly = true;
              input.AllowNegativeNumbers = false;
              // Verify input after OK button is pressed
              if (input.ShowDialog(this) == DialogResult.OK)
              {
                try
                {
                  // Make sure input is numeric and float
                  scaleValue = Convert.ToSingle(input.TextInputInputBox.Text);
                }
                catch (OverflowException)
                {
                  scaleValue = 0.0F;
                }
                catch (InvalidCastException)
                {
                  scaleValue = 0.0F;
                }
                catch (FormatException)
                {
                  return;
                }
                pso.Scale(scaleValue / 100, scaleValue / 100);
              }
            }
            break;
          case "btnArrowLeftTop":
            Point pt = new Point(pso.Location.X - 1, pso.Location.Y - 1);
            pso.Location = pt;
            break;
          case "btnArrowTop":
            Point pt1 = new Point(pso.Location.X, pso.Location.Y - 1);
            pso.Location = pt1;
            break;
          case "btnArrowRightTop":
            Point pt2 = new Point(pso.Location.X + 1, pso.Location.Y - 1);
            pso.Location = pt2;
            break;
          case "btnArrowLeft":
            Point pt3 = new Point(pso.Location.X - 1, pso.Location.Y);
            pso.Location = pt3;
            break;
          case "btnArrowRight":
            Point pt7 = new Point(pso.Location.X + 1, pso.Location.Y);
            pso.Location = pt7;
            break;
          case "btnArrowLeftBottom":
            Point pt4 = new Point(pso.Location.X - 1, pso.Location.Y + 1);
            pso.Location = pt4;
            break;
          case "btnArrowBottom":
            Point pt5 = new Point(pso.Location.X, pso.Location.Y + 1);
            pso.Location = pt5;
            break;
          case "btnArrowRighBottom":
            Point pt6 = new Point(pso.Location.X + 1, pso.Location.Y + 1);
            pso.Location = pt6;
            break;

        }
      }
    }

    #endregion
    #endregion

    #region  (Chart Tools tab)

    private void InitializeChartToolsTab()
    {
      this.rbChartMoveHV.Click += new EventHandler(allowMove);
      this.rbChartMoveH.Click += new EventHandler(allowMove);
      this.rbChartMoveV.Click += new EventHandler(allowMove);
      this.rbChartMoveNone.Click += new EventHandler(allowMove);
      this.rbChartSizeBoth.Click += new EventHandler(allowMove);
      this.rbChartSizeWidth.Click += new EventHandler(allowMove);
      this.rbChartSizeHeight.Click += new EventHandler(allowMove);
      this.rbChartSizeNone.Click += new EventHandler(allowMove);
      this.rbChartMoveWithCell.CheckedChanged += rbChartMoveWithCell_CheckedChanged;
      this.rbChartSizeWithCell.CheckedChanged += rbChartSizeWithCell_CheckedChanged;
    }

    void rbChartSizeWithCell_CheckedChanged(object sender, EventArgs e)
    {
      if (fpSpread1.ActiveSheet == null) return;
      FarPoint.Win.Spread.Chart.SpreadChart chart = null;
      if (fpSpread1.ActiveWindowlessObject is FarPoint.Win.Spread.Chart.SpreadChart)
        chart = (FarPoint.Win.Spread.Chart.SpreadChart)fpSpread1.ActiveWindowlessObject;
      if (chart == null) return;
      chart.DynamicSize = rbChartSizeWithCell.Checked;
    }

    void rbChartMoveWithCell_CheckedChanged(object sender, EventArgs e)
    {
      if (fpSpread1.ActiveSheet == null) return;
      FarPoint.Win.Spread.Chart.SpreadChart chart = null;
      if (fpSpread1.ActiveWindowlessObject is FarPoint.Win.Spread.Chart.SpreadChart)
        chart = (FarPoint.Win.Spread.Chart.SpreadChart)fpSpread1.ActiveWindowlessObject;
      if (chart == null) return;
      chart.DynamicMove = rbChartMoveWithCell.Checked;
    }

    private void btnChangeChartType_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Chart.Design.InsertChart ic = new FarPoint.Win.Chart.Design.InsertChart();
      ic.ShowDialog();
      String chartType = ic.SelectedModel;

      FarPoint.Win.Spread.Chart.SpreadChart sourceChart = fpSpread1.ActiveWindowlessObject
          as FarPoint.Win.Spread.Chart.SpreadChart;
      FarPoint.Win.Chart.ChartModel targetModel = ic.Model;
      if (sourceChart != null)
        sourceChart.ChangeChartType(targetModel);
    }
    // (Bring to front)
    private void btnBringToFront_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("BringToFront"));
    }
    // (Send to back)
    private void btnSendToBack_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.DrawingSpace.DrawingToolbar draw = new FarPoint.Win.Spread.DrawingSpace.DrawingToolbar();
      draw.OwningControl = fpSpread1;
      draw.ShapeContainer = fpSpread1.ActiveSheet.DrawingContainer;
      draw.SetShapeStyle(new FarPoint.Win.Spread.DrawingSpace.ShapeEventArgs("SendToBack"));
    }
    private void btnSwitchRowColumn_Click(object sender, EventArgs e)
    {
      SpreadChart sourceChart = fpSpread1.ActiveWindowlessObject
           as SpreadChart;
      if (sourceChart != null) sourceChart.SwitchRowColumn();
    }
    // (Move Chart)
    private void btnMoveChart_Click(object sender, EventArgs e)
    {
      SpreadChart sourceChart = fpSpread1.ActiveWindowlessObject
          as SpreadChart;
      if (sourceChart != null)
      {
        MoveChart mc = new MoveChart(fpSpread1, sourceChart);

        mc.ShowDialog();
      }
    }
    // (Select Data)
    private void btnselectData_Click(object sender, EventArgs e)
    {
      SpreadChart sourceChart = fpSpread1.ActiveWindowlessObject
           as SpreadChart;
      if (sourceChart != null)
      {
        FarPoint.Win.Spread.Chart.ChartSelectDataSource csd = new ChartSelectDataSource
            (fpSpread1, sourceChart);
        csd.Show();
      }
    }
    // (Allow move)
    private void allowMove(object sender, EventArgs e)
    {
      SpreadChart sourceChart = fpSpread1.ActiveWindowlessObject
           as SpreadChart;
      //sourceChart.
      RibbonButton rb = sender as RibbonButton;
      if (sourceChart != null)
      {
        switch (rb.Name)
        {
          case "rbChartMoveHV":
            sourceChart.CanMove = Moving.HorizontalAndVertical;
            break;
          case "rbChartMoveH":
            sourceChart.CanMove = Moving.Horizontal;
            break;
          case "rbChartMoveV":
            sourceChart.CanMove = Moving.Vertical;
            break;
          case "rbChartMoveNone":
            sourceChart.CanMove = Moving.None;
            break;
          case "rbChartSizeBoth":
            sourceChart.CanSize = Sizing.HeightAndWidth;
            break;
          case "rbChartSizeWidth":
            sourceChart.CanSize = Sizing.Width;
            break;
          case "rbChartSizeHeight":
            sourceChart.CanSize = Sizing.Height;
            break;
          case "rbChartSizeNone":
            sourceChart.CanSize = Sizing.None;
            break;
        }
      }
    }
    #endregion

    #region  (Sparklines tab)
    private void InitializeSparklinesTab()
    {

      this.rbSparklineEditDataHidden.Click += new EventHandler(allowMove);
      this.rbSparklineEditDataSwitch.Click += new EventHandler(allowMove);
      this.rbSparklineTypeLine.Click += new EventHandler(ConvertLine_Click);
      this.rbSparklineTypeColumn.Click += new EventHandler(ConvertColumn_Click);
      this.rbSparklineTypeWinLoss.Click += new EventHandler(ConvertWinLoss_Click);
      this.rbSparklineClearSingle.Click += new EventHandler(ClearSelectedSparkline_Click);
      this.rbSparklineClearGroup.Click += new EventHandler(clearSparklineGroups_Click);
      this.rbSparklineLowPoint.CheckedChanged += new EventHandler(Display);
      this.rbSparklineHighPoint.CheckedChanged += new EventHandler(Display);
      this.rbSparklineMarkerColorNegativePoint.CheckedChanged += new EventHandler(Display);
      this.rbSparklineFirstPoint.CheckedChanged += new EventHandler(Display);
      this.rbSparklineLastPoint.CheckedChanged += new EventHandler(Display);
      this.rbSparklineMarkerColorMarker.CheckedChanged += new EventHandler(Display);
      this.rbSparklineColor.SelectedColorChanged += new EventHandler(ColorPickerSparkline_SelectedColorChanged);
      this.rbSparklineMarkerColorFirstPoint.SelectedColorChanged += new EventHandler(FirstMakerColor_SelectedColorChanged);
      this.rbSparklineMarkerColorLastPoint.SelectedColorChanged += new EventHandler(LastMakerColor_SelectedColorChanged);
      this.rbSparklineMarkerColorHighPoint.SelectedColorChanged += new EventHandler(HighMakerColor_SelectedColorChanged);
      this.rbSparklineMarkerColorLowPoint.SelectedColorChanged += new EventHandler(LowMakerColor_SelectedColorChanged);
      this.rbSparklineNegativePoints.SelectedColorChanged += new EventHandler(NegativeColor_SelectedColorChanged);
      this.rbSparklineMarkers.SelectedColorChanged += new EventHandler(MakersColor_SelectedColorChanged);
      this.rbSparklineAxisSub1.Click += new EventHandler(rbSparklineGeneralAxis_Click);
      this.rbSparklineAxisSub2.Click += new EventHandler(rbSparklineDateAxis_Click);
      this.rbSparklineAxisSub4.Click += new EventHandler(RightToLeft_Click);
      this.rbSparklineAxisSub3.Click += new EventHandler(ShowAxis_Click);
      this.rbSparklineAxisSub8.Click += new EventHandler(AutoMaxAxisType_Click);
      this.rbSparklineAxisSub5.Click += new EventHandler(AutoMinAxisType_Click);
      this.rbSparklineAxisSub6.Click += new EventHandler(MinAxisType_Click);
      this.rbSparklineAxisSub9.Click += new EventHandler(MaxAxisType_Click);
      this.rbSparklineEditDataGroup.Click += new EventHandler(EditGroupLocationData_Click);
      this.rbSparklineEditDataSingle.Click += new EventHandler(EditSingleSparklineData_Click);
      this.rbSparklineEditDataHidden.Click += new EventHandler(HiddenEmptyCells_Click);
      this.rbSparklineAxisSub7.Click += new EventHandler(btnCustomValue_Click);
      this.rbSparklineAxisSub10.Click += new EventHandler(btnCustomValue1_Click);
    }

    private void EditSingleSparklineData_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ExcelSparklineGroup.ShowSelectDataForm(this, this.fpSpread1,
          fpSpread1.ActiveSheet.ActiveCell.Sparkline.Data as FarPoint.Win.Spread.ExcelSparkline);

    }

    private void EditGroupLocationData_Click(object sender, EventArgs e)
    {
      // FarPoint.Win.Spread.ExcelSparklineGroup.ShowSelectDataForm  
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
           fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);

      FarPoint.Win.Spread.ExcelSparklineGroup.ShowSelectDataForm(this, fpSpread1, outGroup as FarPoint.Win.Spread.ExcelSparklineGroup, false);

    }

    private void HiddenEmptyCells_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      FarPoint.Win.Spread.Design.SparklineHiddenEmptyCellSetting ss = new FarPoint.Win.Spread.Design.SparklineHiddenEmptyCellSetting(outGroup as FarPoint.Win.Spread.ExcelSparklineGroup);
      ss.ShowDialog();

    }
    // (convert to line sparkline)
    private void ConvertLine_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
           fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
      outGroup.Renderer = new FarPoint.Win.Spread.LineSparklineRenderer();
      sparkline_SelectionChanged();
    }
    // (convert to column sparkline)
    private void ConvertColumn_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
           fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
      outGroup.Renderer = new FarPoint.Win.Spread.ColumnSparklineRenderer();
      sparkline_SelectionChanged();
    }
    // (convert to win-loss sparkline)
    private void ConvertWinLoss_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
           fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
      outGroup.Renderer = new FarPoint.Win.Spread.WinLossSparklineRenderer();
      sparkline_SelectionChanged();
    }
    // (ungroup)
    private void unGroup_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      rbSparklineUngroup.Enabled = false;
      rbSparklineGroup.Enabled = true;
      sheet.UngroupSparkline(cellRanges);
    }
    // (Group)
    private void Group_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      //  (Get the current cell Sparkline object)
      FarPoint.Win.Spread.ISparklineGroup outGroup;
      FarPoint.Win.Spread.ISparkline outSL;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
           fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
      rbSparklineUngroup.Enabled = true;
      rbSparklineGroup.Enabled = false;

      switch (outGroup.Renderer.ToString())
      {
        case "FarPoint.Win.Spread.LineSparklineRenderer":
          sheet.GroupSparkline(cellRanges, SparklineType.Line);
          break;
        case "FarPoint.Win.Spread.ColumnSparklineRenderer":
          sheet.GroupSparkline(cellRanges, SparklineType.Column);
          break;
        case "FarPoint.Win.Spread.WinLossSparklineRenderer":
          sheet.GroupSparkline(cellRanges, SparklineType.Winloss);
          break;
      }

    }
    // (Clear selected sparkline)
    private void ClearSelectedSparkline_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
      {
        sheet.ClearSparklines(range);
      }
      sparkline_SelectionChanged();
    }
    // (Clear sparkline group)
    private void clearSparklineGroups_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = GetSpreadSelections();
      foreach (CellRange range in cellRanges)
      {
        sheet.ClearSparklineGroupsInRange(range);
      }
      sparkline_SelectionChanged();
    }
    // (sparkline Color)
    private void ColorPickerSparkline_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).SeriesColor = rbSparklineColor.Color;
    }
    private void ColorPickerSparkline_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).SeriesColor = rbSparklineColor.Color;
    }
    private void HighMakerColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).HighMarkerColor = rbSparklineMarkerColorHighPoint.Color;

    }
    private void FirstMakerColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).FirstMarkerColor = rbSparklineMarkerColorFirstPoint.Color;
    }
    private void LastMakerColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).LastMarkerColor = rbSparklineMarkerColorLastPoint.Color;
    }
    private void LowMakerColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).LowMarkerColor = rbSparklineMarkerColorLowPoint.Color;
    }

    private void NegativeColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).NegativeColor = rbSparklineNegativePoints.Color;

    }
    private void MakersColor_SelectedColorChanged(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MarkersColor = rbSparklineMarkers.Color;
    }

    private void Display(object sender, EventArgs e)
    {
      RibbonCheckBox rcb = sender as RibbonCheckBox;
      SheetView sheet = this.fpSpread1.ActiveSheet;
      switch (rcb.Name)
      {
        case "rbSparklineLowPoint":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowLow = true;
          break;
        case "rbSparklineHighPoint":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowHigh = true;
          break;
        case "rbSparklineMarkerColorNegativePoint":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowNegative = true;
          break;
        case "rbSparklineFirstPoint":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowFirst = true;
          break;
        case "rbSparklineLastPoint":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowLast = true;
          break;
        case "rbSparklineMarkerColorMarker":
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ShowMarkers = true;
          break;
      }
    }

    void rbSparklineDateAxis_Click(object sender, EventArgs e)
    {
      ISparklineGroup spGroup;
      ISparkline spLine;
      fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex, out spGroup, out spLine);
      Form sparklineGroupSelectDataForm = FarPoint.Win.Spread.ExcelSparklineGroup.ShowSelectDataForm(this, fpSpread1, spGroup as FarPoint.Win.Spread.ExcelSparklineGroup, true);
      sparklineGroupSelectDataForm.FormClosed += sparklineGroupSelectDataForm_FormClosed;
    }

    void sparklineGroupSelectDataForm_FormClosed(object sender, FormClosedEventArgs e)
    {
 	    if (((Form)sender).DialogResult == System.Windows.Forms.DialogResult.OK)
      {
        sparkline_SelectionChanged();
      }
    }

    void rbSparklineGeneralAxis_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).DateAxis = false;
      sparkline_SelectionChanged();
    }

    private void ShowAxis_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).DisplayXAxis = true;
      sparkline_SelectionChanged();
    }
    private void RightToLeft_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).RightToLeft = true;
      sparkline_SelectionChanged();
    }
    private void AutoMaxAxisType_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MaxAxisType = SparklineAxisMinMax.Individual;
      sparkline_SelectionChanged();
    }
    private void AutoMinAxisType_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MinAxisType = SparklineAxisMinMax.Individual;
      sparkline_SelectionChanged();
    }
    private void MinAxisType_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MinAxisType = SparklineAxisMinMax.Group;
      sparkline_SelectionChanged();
    }
    private void MaxAxisType_Click(object sender, EventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MaxAxisType = SparklineAxisMinMax.Group;
      sparkline_SelectionChanged();
    }

    private void btnCustomValue_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      SheetView sheet = this.fpSpread1.ActiveSheet;
      if (sheet.ActiveCell.Sparkline != null)
      {
        FarPoint.Win.Spread.Design.SparklineAxisValueOption savo = new FarPoint.Win.Spread.Design.SparklineAxisValueOption
            (false, this.fpSpread1.ActiveSheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting);
        if (savo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ManualMin = savo.Value;
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MinAxisType = SparklineAxisMinMax.Custom;
          sparkline_SelectionChanged();
        }
      }

    }
    private void btnCustomValue1_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
      SheetView sheet = this.fpSpread1.ActiveSheet;
      if (sheet.ActiveCell.Sparkline != null)
      {
        FarPoint.Win.Spread.Design.SparklineAxisValueOption savo = new FarPoint.Win.Spread.Design.SparklineAxisValueOption
            (true, this.fpSpread1.ActiveSheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting);
        if (savo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).ManualMax = savo.Value;
          (sheet.ActiveCell.Sparkline.Setting as ExcelSparklineSetting).MaxAxisType = SparklineAxisMinMax.Custom;
          sparkline_SelectionChanged();
        }
      }
    }
    #endregion

    #region Tables tab
//
    private void InitTableContextMenu()
    {
      this.menuContext_Sep_Table.Index = 29;
      this.menuContext_Sep_Table.Text = rm.GetString("menuContext_Sep1.Text");


      this.menuContext_Table.Index = 30;
      this.menuContext_Table.Text = rm.GetString("menuContext_Table.Text");
      this.menuContext_Table.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
    this.menuContext_TableTotalsRow,
    this.menuContext_TableConvertToRange
  });

      this.menuContext_TableTotalsRow.Index = 0;
      this.menuContext_TableTotalsRow.Text = rm.GetString("menuContext_TableTotalsRow.Text");
      this.menuContext_TableTotalsRow.Image = NativeMethods.GetImageFromManifestResource("TableTotalRow.png", true);

      this.menuContext_TableConvertToRange.Index = 1;
      this.menuContext_TableConvertToRange.Text = rm.GetString("menuContext_TableConvertToRange.Text");

      this.menuContext_Sep_Table1.Index = 31;
      this.menuContext_Sep_Table1.Text = rm.GetString("menuContext_Sep1.Text");

      //Table insert context menu element
      this.menuContext_TableInsert.Index = 32;
      this.menuContext_TableInsert.Text = rm.GetString("menuContext_TableInsert.Text");
      this.menuContext_TableInsert.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
    this.menuContext_TableInsertColumnToLeft,
    this.menuContext_TableInsertColumnToRight,
    this.menuContext_TableInsertRowToAbove,
    this.menuContext_TableInsertRowToBelow
  });

      this.menuContext_TableInsertColumnToLeft.Index = 0;
      this.menuContext_TableInsertColumnToLeft.Text = rm.GetString("menuContext_TableInsertColumnToLeft.Text");
      this.menuContext_TableInsertColumnToLeft.Image = NativeMethods.GetImageFromManifestResource("TableInsertColumnLeft.png", true);
      this.menuContext_TableInsertColumnToLeft.ImageDisable = NativeMethods.GetImageFromManifestResource("TableInsertColumnLeftDisable.png", true);

      this.menuContext_TableInsertColumnToRight.Index = 1;
      this.menuContext_TableInsertColumnToRight.Text = rm.GetString("menuContext_TableInsertColumnToRight.Text");
      this.menuContext_TableInsertColumnToRight.Image = NativeMethods.GetImageFromManifestResource("TableInsertColumnRight.png", true);
      this.menuContext_TableInsertColumnToRight.ImageDisable = NativeMethods.GetImageFromManifestResource("TableInsertColumnRightDisable.png", true);

      this.menuContext_TableInsertRowToAbove.Index = 2;
      this.menuContext_TableInsertRowToAbove.Text = rm.GetString("menuContext_TableInsertRowToAbove.Text");
      this.menuContext_TableInsertRowToAbove.Image = NativeMethods.GetImageFromManifestResource("TableInsertRowAbove.png", true);
      this.menuContext_TableInsertRowToAbove.ImageDisable = NativeMethods.GetImageFromManifestResource("TableInsertRowAboveDisable.png", true);

      this.menuContext_TableInsertRowToBelow.Index = 3;
      this.menuContext_TableInsertRowToBelow.Text = rm.GetString("menuContext_TableInsertRowToBelow.Text");
      this.menuContext_TableInsertRowToBelow.Image = NativeMethods.GetImageFromManifestResource("TableInsertRowBelow.png", true);
      this.menuContext_TableInsertRowToBelow.ImageDisable = NativeMethods.GetImageFromManifestResource("TableInsertRowBelowDisable.png", true);


      //Table Delete context menu element
      this.menuContext_TableDelete.Index = 33;
      this.menuContext_TableDelete.Text = rm.GetString("menuContext_TableDelete.Text");
      this.menuContext_TableDelete.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
    this.menuContext_TableDeleteColumn,
    this.menuContext_TableDeleteRow
  });

      this.menuContext_TableDeleteColumn.Index = 0;
      this.menuContext_TableDeleteColumn.Text = rm.GetString("menuContext_TableDeleteColumn.Text");
      this.menuContext_TableDeleteColumn.Image = NativeMethods.GetImageFromManifestResource("TableDeleteColumn.png", true);
      this.menuContext_TableDeleteColumn.ImageDisable = NativeMethods.GetImageFromManifestResource("TableDeleteColumnDisable.png", true);

      this.menuContext_TableDeleteRow.Index = 1;
      //84956 Ken 2014-09-10
      this.menuContext_TableDeleteRow.Text = rm.GetString("menuContext_TableDeleteRow.Text");
      this.menuContext_TableDeleteRow.Image = NativeMethods.GetImageFromManifestResource("TableDeleteRow.png", true);
      this.menuContext_TableDeleteRow.ImageDisable = NativeMethods.GetImageFromManifestResource("TableDeleteRowDisable.png", true);

      //Table selection context menu element
     // this.menuContext_TableSelect.Index = 34;
      this.menuContext_TableSelect.Text = rm.GetString("menuContext_TableSelect.Text");
      this.menuContext_TableSelect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
    this.menuContext_TableSelectColumnData,
    this.menuContext_TableSelectEntryColumn,
    this.menuContext_TableSelectRow
  });

      this.menuContext_TableSelectColumnData.Index = 0;
      this.menuContext_TableSelectColumnData.Text = rm.GetString("menuContext_TableSelectColumnData.Text");

      this.menuContext_TableSelectEntryColumn.Index = 1;
      this.menuContext_TableSelectEntryColumn.Text = rm.GetString("menuContext_TableSelectEntryColumn.Text");

      this.menuContext_TableSelectRow.Index = 2;
      this.menuContext_TableSelectRow.Text = rm.GetString("menuContext_TableSelectRow.Text");
      menuContext_TableSelectColumnData.Click += SelectTableColumnData;
      menuContext_TableSelectEntryColumn.Click += SelectTableEntryColumn;
      menuContext_TableSelectRow.Click += SelectTableRow;
      menuContext_TableDeleteRow.Click += DeleteTableRow;
      menuContext_TableDeleteColumn.Click += DeleteTableColumn;
      menuContext_TableInsertColumnToLeft.Click += InsertTableColumnToLeft;
      menuContext_TableInsertColumnToRight.Click += InsertTableColumnToRight;
      menuContext_TableInsertRowToAbove.Click += InsertTableRowToAbove;
      menuContext_TableInsertRowToBelow.Click += InsertTableRowToBelow;
      menuContext_TableTotalsRow.Click += InsertTableTotalsRow;
      menuContext_TableConvertToRange.Click += InsertTableConvertToRange;
    }
    private void SelectTableColumnData(System.Object sender, System.EventArgs e)
    {
      SheetView sheetView = fpSpread1.ActiveSheet;
      TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
      if (tableView == null)
      {
        return;
      }
      CellRange cellRange = sheetView.GetSelection(0);
      sheetView.ClearSelection();
      int numberRowTotalAdjust = 0;
      int numberRowHeaderAdjust = 0;
      if ((tableView.HeaderRowVisible))
      {
        numberRowHeaderAdjust = 1;
        numberRowTotalAdjust = 1;
      }
      if ((tableView.TotalRowVisible))
      {
        numberRowTotalAdjust += 1;
      }
      sheetView.AddSelection(tableView.DataRange.Row + numberRowHeaderAdjust, cellRange.Column, tableView.DataRange.RowCount - numberRowTotalAdjust, cellRange.ColumnCount);
      //SelectionChangedUpdate(tableView.DataRange.Row + numberRowHeaderAdjust, tableView.DataRange.RowCount - numberRowTotalAdjust, cellRange.Column, cellRange.ColumnCount);
    }
    private void SelectTableEntryColumn(System.Object sender, System.EventArgs e)
    {
      SheetView sheetView = fpSpread1.ActiveSheet;
      TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
      if (tableView == null)
      {
        return;
      }
      CellRange cellRange = sheetView.GetSelection(0);
      sheetView.ClearSelection();
      sheetView.AddSelection(tableView.DataRange.Row, cellRange.Column, tableView.DataRange.RowCount, cellRange.ColumnCount);
     // SelectionChangedUpdate(tableView.DataRange.Row, tableView.DataRange.RowCount, sheetView.ActiveColumnIndex, 1);
    }
    private void SelectTableRow(System.Object sender, System.EventArgs e)
    {
      SheetView sheetView = fpSpread1.ActiveSheet;
      TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
      if (tableView == null)
      {
        return;
      }
      CellRange cellRange = sheetView.GetSelection(0);
      sheetView.ClearSelection();
      sheetView.AddSelection(cellRange.Row, tableView.DataRange.Column, cellRange.RowCount, tableView.DataRange.ColumnCount);
     // SelectionChangedUpdate(sheetView.ActiveRowIndex, 1, tableView.DataRange.Column, tableView.DataRange.ColumnCount);
    }
    private void DeleteTableRow(System.Object sender, System.EventArgs e)
    {
      try
      {
        SheetView sheetView = fpSpread1.ActiveSheet;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
        if (tableView == null)
        {
          return;
        }
        CellRange cellRange = sheetView.GetSelection(0);
        //remove table if selected cellrange is constain table
        if ((cellRange.Row <= tableView.DataRange.Row && cellRange.Row + cellRange.RowCount >= tableView.DataRange.Row + tableView.DataRange.RowCount))
        {
          cellRange = tableView.DataRange;
          sheetView.RemoveTable(tableView.Name);
          sheetView.ClearRange(cellRange.Row, cellRange.Column, cellRange.RowCount, cellRange.ColumnCount, false);
        }
        else
        {
          //CASE header/total row is visible
          if ((tableView.HeaderRowVisible && tableView.TotalRowVisible))
          {
            if ((cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount))
            {
              tableView.TotalRowVisible = false;
              tableView.ResetTotalRow();
              //#91456 Leon 20141023
              if ((cellRange.Row == tableView.DataRange.Row + 1))
              {
                sheetView.ClearRange(cellRange.Row, tableView.DataRange.Column, 1, tableView.DataRange.ColumnCount, false);
                if ((cellRange.RowCount - 2 > 0))
                {
                  tableView.RemoveDataRows(1, cellRange.RowCount - 2);
                }
              }
              else
              {
                if ((cellRange.RowCount - 1 > 0))
                {
                  tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row - 1, cellRange.RowCount - 1);
                }
              }
            }
            else
            {
              if ((cellRange.Row == tableView.DataRange.Row + 1 && cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount - 1))
              {
                sheetView.ClearRange(cellRange.Row, tableView.DataRange.Column, 1, tableView.DataRange.ColumnCount, false);
                if ((cellRange.RowCount - 1 > 0))
                {
                  tableView.RemoveDataRows(1, cellRange.RowCount - 1);
                }
              }
              else
              {
                tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row - 1, cellRange.RowCount);
              }
            }
          }
          else
          {
            //CASE header visible and total row is not visible
            if ((tableView.HeaderRowVisible && !tableView.TotalRowVisible))
            {
              //TODO
              if ((cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount))
              {
                if (cellRange.Row == tableView.DataRange.Row + 1)
                {
                  sheetView.ClearRange(cellRange.Row, tableView.DataRange.Column, 1, tableView.DataRange.ColumnCount, false);
                  if ((cellRange.RowCount - 1 > 0))
                  {
                    tableView.RemoveDataRows(1, cellRange.RowCount - 1);
                  }
                }
                else
                {
                  tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row - 1, cellRange.RowCount);
                }
              }
              else
              {
                tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row - 1, cellRange.RowCount);
              }
            }
            else
            {
              //CASE header is not visible and total row is visible
              if ((!tableView.HeaderRowVisible && tableView.TotalRowVisible))
              {
                //TODO
                if (cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount - 1)
                {
                  if ((cellRange.Row == tableView.DataRange.Row))
                  {
                    sheetView.ClearRange(cellRange.Row, tableView.DataRange.Column, 1, tableView.DataRange.ColumnCount, false);
                    if ((cellRange.RowCount - 1 > 0))
                    {
                      tableView.RemoveDataRows(1, cellRange.RowCount - 1);
                    }
                  }
                  else
                  {
                    tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row, cellRange.RowCount);
                  }
                }
                else
                {
                  if (cellRange.Row + cellRange.RowCount == tableView.DataRange.Row + tableView.DataRange.RowCount)
                  {
                    tableView.TotalRowVisible = false;
                    tableView.ResetTotalRow();
                    //#91456 Leon 20141023
                    if ((cellRange.RowCount - 1 > 0))
                    {
                      tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row, cellRange.RowCount - 1);
                    }
                  }
                  else
                  {
                    tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row, cellRange.RowCount);
                  }

                }
                //CASE header/total row is not visible
              }
              else
              {
                //TODO
                tableView.RemoveDataRows(cellRange.Row - tableView.DataRange.Row, cellRange.RowCount);
              }
            }
          }
        }
       // UpdateTableToolBar();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void DeleteTableColumn(System.Object sender, System.EventArgs e)
    {
      try
      {
        SheetView sheetView = fpSpread1.ActiveSheet;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
        if (tableView == null)
        {
          return;
        }
        CellRange cellRange = sheetView.GetSelection(0);
        if ((cellRange.Column <= tableView.DataRange.Column && cellRange.Column + cellRange.ColumnCount >= tableView.DataRange.Column + tableView.DataRange.ColumnCount))
        {
          cellRange = tableView.DataRange;
          sheetView.RemoveTable(tableView.Name);
          sheetView.ClearRange(cellRange.Row, cellRange.Column, cellRange.RowCount, cellRange.ColumnCount, false);
        }
        else
        {
          tableView.RemoveColumns(cellRange.Column - tableView.DataRange.Column, cellRange.ColumnCount);
        }
       // UpdateTableToolBar();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableColumnToLeft(System.Object sender, System.EventArgs e)
    {
      try
      {
        SheetView sheetView = fpSpread1.ActiveSheet;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
        if (tableView == null)
        {
          return;
        }
        CellRange cellRange = sheetView.GetSelection(0);
        tableView.AddColumns(cellRange.Column - tableView.DataRange.Column, cellRange.ColumnCount);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableColumnToRight(System.Object sender, System.EventArgs e)
    {
      try
      {
        SheetView sheetView = fpSpread1.ActiveSheet;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
        if (tableView == null)
        {
          return;
        }
        tableView.AddColumns(sheetView.ActiveColumnIndex + 1 - tableView.DataRange.Column, 1);
        //UpdateTableToolBar();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableRowToAbove(System.Object sender, System.EventArgs e)
    {
      try
      {

        SheetView sheetView = fpSpread1.ActiveSheet;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRowIndex, sheetView.ActiveColumnIndex);
        if (tableView == null)
        {
          return;
        }
        int adjustIndex = 0;
        if (tableView.HeaderRowVisible)
        {
          adjustIndex = 1;
        }
        CellRange cellRange = sheetView.GetSelection(0);
        tableView.AddDataRows(cellRange.Row - tableView.DataRange.Row - adjustIndex, cellRange.RowCount);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableRowToBelow(System.Object sender, System.EventArgs e)
    {
      try
      {
       
        SheetView sheetView = fpSpread1.ActiveSheet;
        if ((fpSpread1.ActiveSheet == null))
          return;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRow.Index, sheetView.ActiveColumn.Index);
        if (tableView == null)
        {
          return;
        }
        int adjustIndex = 0;
        if (tableView.HeaderRowVisible)
        {
          adjustIndex = 1;
        }
        tableView.AddDataRows(sheetView.ActiveRow.Index + 1 - tableView.DataRange.Row - adjustIndex, 1);
      //  UpdateTableToolBar();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableTotalsRow(System.Object sender, System.EventArgs e)
    {
      try
      {

        SheetView sheetView = fpSpread1.ActiveSheet;
        if ((fpSpread1.ActiveSheet == null))
          return;
        TableView tableView = sheetView.GetTable(sheetView.ActiveRow.Index, sheetView.ActiveColumn.Index);
        if (tableView == null)
        {
          return;
        }
        tableView.TotalRowVisible = !tableView.TotalRowVisible;
        //UpdateTableToolBar();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
    }
    private void InsertTableConvertToRange(System.Object sender, System.EventArgs e)
    {

      if ((fpSpread1.ActiveSheet == null))
        return;
      rbTableConvertToRange_Click(null,null);
    }
//
    private void InitializeTableTab()
    {
      this.rtbTableName.ChangeCommitted += rtbTableName_TextChanged;
      this.rbTableResize.Click += rbTableResize_Click;
      this.rbTableConvertToRange.Click += rbTableConvertToRange_Click;
      rcTableHeaderRow.CheckedChanged += rcTableHeaderRow_CheckedChanged;
      rcTableTotalRow.CheckedChanged += rcTableTotalRow_CheckedChanged;
      rcTableBandedRow.CheckedChanged += rcTableBandedRow_CheckedChanged;
      rcTableFirstColumn.CheckedChanged += rcTableFirstColumn_CheckedChanged;
      rcTableLastColumn.CheckedChanged += rcTableLastColumn_CheckedChanged;
      rcTableBandedColumn.CheckedChanged += rcTableBandedColumn_CheckedChanged;
      rcTableFilterButton.CheckedChanged += rcTableFilterButton_CheckedChanged;
      rglTableStyles.HighlightedItemChanged += rglTableStyles_HighlightedItemChanged;
      rglTableStyles.SelectedIndexChanged += rglTableStyles_SelectedIndexChanged;
      rbTableStyleClear.Click += rbTableStyleClear_Click;
    }

  

    void rbTableStyleClear_Click(object sender, EventArgs e)
    {
      if (cachedTableView != null)
        cachedTableView.StyleName = TableStyle.None;
    }

    void rglTableStyles_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (rglTableStyles.SelectedItem != null && cachedTableView != null)
        cachedTableView.StyleName = rglTableStyles.SelectedItem.Text;

    }

    void rglTableStyles_HighlightedItemChanged(object sender, GalleryEventArgs e)
    {
      if (cachedTableView != null)
      {
        if (e.GalleryItem != null)
          cachedTableView.StyleName = e.GalleryItem.Text;
        else if (rglTableStyles.SelectedItem != null)
          cachedTableView.StyleName = rglTableStyles.SelectedItem.Text;
      }
    }

    void rcTableFilterButton_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
        cachedTableView.FilterButtonVisible = rcTableFilterButton.Checked;
    }

    void rcTableBandedColumn_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.BandedColumns = rcTableBandedColumn.Checked;
        UpdateTableStylesRibbonGallery();
      }
    }

    void rcTableLastColumn_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.LastColumn = rcTableLastColumn.Checked;
        UpdateTableStylesRibbonGallery();
      }
    }

    void rcTableFirstColumn_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.FirstColumn = rcTableFilterButton.Checked;
        UpdateTableStylesRibbonGallery();
      }
    }

    void rcTableBandedRow_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.BandedRows = rcTableBandedRow.Checked;
        UpdateTableStylesRibbonGallery();
      }
    }

    void rcTableTotalRow_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.TotalRowVisible = rcTableTotalRow.Checked;
        UpdateTableStylesRibbonGallery();
      }
    }

    void rcTableHeaderRow_CheckedChanged(object sender, EventArgs e)
    {
      if (cachedTableView != null)
      {
        cachedTableView.HeaderRowVisible = rcTableHeaderRow.Checked;
        if (cachedTableView.HeaderRowVisible)
        {
          rcTableFilterButton.Enabled = true;
          rcTableFilterButton.Checked = true;
          cachedTableView.FilterButtonVisible = true;
        }
          
        else
        {
          rcTableFilterButton.Checked = false;
          rcTableFilterButton.Enabled = false;
        }
        UpdateTableStylesRibbonGallery();
      }
    }

    void rbTableConvertToRange_Click(object sender, EventArgs e)
    {
      TableView table = fpSpread1.ActiveSheet.GetTable(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
      if (table != null)
      {
        SpreadView spreadview = fpSpread1.GetRootWorkbook();
        if (spreadview != null && spreadview.AllowUndo)
          spreadview.UndoManager.PerformUndoAction(new ConvertTableToRangeUndoAction(fpSpread1, fpSpread1.ActiveSheet, table.Name));
        else
          fpSpread1.ActiveSheet.ConvertTableToRange(table.Name);
      }
    }

    void rbTableResize_Click(object sender, EventArgs e)
    {
      try
      {
        Form form = TableUtils.ShowTableResizeForm(this, fpSpread1);       
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }

    }

    void rtbTableName_TextChanged(object sender, EventArgs e)
    {
      try
      {
      if (cachedTableView != null)
        cachedTableView.Name = rtbTableName.Text;
    }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
        if (rtbTableName.TextBox !=null)
        {
          rtbTableName.TextBox.Focus();          
        }        
      }      
    }

    #endregion

    #region  (Qat Events [Qat=quick-access toolbar])

    //  (Initialization Qat menu)
    private void InitializeQat()
    {
      this.tbnUndo.Text = rm.GetString("tbnUndo");
      this.tbnUndo.Click += new EventHandler(tbnUndo_Click);

      this.tbnRedo.Text = rm.GetString("tbnRedo");
      this.tbnRedo.Click += new EventHandler(tbnRedo_Click);

      this.tbnNew.Text = FarPoint.Win.Ribbon.ResourceStringHelper.GetString("StartMenuForm.New");
      this.tbnNew.Click += new EventHandler(tbnNew_Click);

      this.tbnOpen.Text = rm.GetString("tbnOpen");
      this.tbnOpen.Click += new EventHandler(tbnOpen_Click);

      this.tbnSave.Text = rm.GetString("tbnSave");
      this.tbnSave.Click += new EventHandler(tbnSave_Click);

      this.tbnPrint.Text = rm.GetString("tbnPrint");
      this.tbnPrint.Click += new EventHandler(tbnPrint_Click);
    }

    void tbnPrint_Click(object sender, EventArgs e)
    {
      fpSpread1.ShowPageSetup(fpSpread1.ActiveSheetIndex, true, true);
    }

    void tbnSave_Click(object sender, EventArgs e)
    {
      if (fpSpread1.EditMode == true)
      {
        fpSpread1.StopCellEditing();
      }

      FileSave(false);
    }

    void tbnOpen_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show(rm.GetString("ResetPrompt"), rm.GetString("DesignerErrorTitle"), MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
      {
        FileOpen(null);
      }
    }

    void tbnNew_Click(object sender, EventArgs e)
    {
      if (MessageBox.Show(rm.GetString("ResetPrompt"), rm.GetString("DesignerErrorTitle"), MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
      {
        fpSpread1.Reset();
        fpSpread1.TabStripPolicy = TabStripPolicy.Always;
      }
    }

    void tbnRedo_Click(object sender, EventArgs e)
    {
      if (fpSpread1.UndoManager.CanRedo)
      {
        fpSpread1.UndoManager.Redo();
      }
    }

    void tbnUndo_Click(object sender, EventArgs e)
    {
      if (fpSpread1.UndoManager.CanUndo)
      {
        fpSpread1.UndoManager.Undo();
      }
    }

    private void SynUndoReDoState()
    {
      if (fpSpread1.UndoManager.UndoList != null && fpSpread1.UndoManager.UndoList.Count > 0)
      {
        FarPoint.Win.Spread.UndoRedo.UndoAction undoAction = fpSpread1.UndoManager.UndoList[fpSpread1.UndoManager.UndoList.Count - 1] as FarPoint.Win.Spread.UndoRedo.UndoAction;
        if (undoAction != null)
        {
          tbnUndo.Enabled = true;

          string undostring = string.Format(rm.GetString("DesignerMain.ButtonUndo.ToolTip.Undo"), undoAction.ToString());
          tbnUndo.Text = undostring;
          tbnUndo.ToolTip = undostring;
        }
        else
        {
          tbnUndo.Enabled = false;
          tbnUndo.Text = rm.GetString("DesignerMain.ButtonUndo.ToolTip.CanNotUndo");
          tbnUndo.ToolTip = rm.GetString("DesignerMain.ButtonUndo.ToolTip.CanNotUndo");
        }
      }
      else
      {
        tbnUndo.Enabled = false;
        tbnUndo.Text = rm.GetString("DesignerMain.ButtonUndo.ToolTip.CanNotUndo");
        tbnUndo.ToolTip = rm.GetString("DesignerMain.ButtonUndo.ToolTip.CanNotUndo");
      }

      if (fpSpread1.UndoManager.RedoList != null && fpSpread1.UndoManager.RedoList.Count > 0)
      {
        FarPoint.Win.Spread.UndoRedo.UndoAction redoAction = fpSpread1.UndoManager.RedoList[fpSpread1.UndoManager.RedoList.Count - 1] as FarPoint.Win.Spread.UndoRedo.UndoAction;
        if (redoAction != null)
        {
          tbnRedo.Enabled = true;

          string redostring = string.Format(rm.GetString("DesignerMain.ButtonRedo.ToolTip.Redo"), redoAction.ToString());
          tbnRedo.Text = redostring;
          tbnRedo.ToolTip = redostring;
        }
        else
        {
          tbnRedo.Enabled = false;
          tbnRedo.Text = rm.GetString("DesignerMain.ButtonRedo.ToolTip.CanNotRedo");
          tbnRedo.ToolTip = rm.GetString("DesignerMain.ButtonRedo.ToolTip.CanNotRedo");
        }
      }
      else
      {
        tbnRedo.Enabled = false;
        tbnRedo.Text = rm.GetString("DesignerMain.ButtonRedo.ToolTip.CanNotRedo");
        tbnRedo.ToolTip = rm.GetString("DesignerMain.ButtonRedo.ToolTip.CanNotRedo");
      }
    }

    private string glbFileName = null;
    private void FileOpen(string strFileName)
    {
      bool loadFailed = false;
      bool resetOverrides = false;
      ExcelOpenFileDialog excelOpenFileDlg = new ExcelOpenFileDialog();
      System.Windows.Forms.OpenFileDialog ofd = excelOpenFileDlg.Dialog as System.Windows.Forms.OpenFileDialog;

      int activeRow = -1;
      int activeCol = -1;

      ofd.Filter = rm.GetString("ofd.Filter_1");
      ofd.FilterIndex = 0;
      ofd.Multiselect = false;
      ofd.CheckFileExists = true;
      ofd.CheckPathExists = true;

      DialogResult ret;
      if (strFileName != null)
      {
        ofd.FileName = strFileName;
        ret = System.Windows.Forms.DialogResult.OK;
      }
      else
      {
        this.Activate();
        ret = excelOpenFileDlg.ShowDialog(this);
      }

      if (ret == System.Windows.Forms.DialogResult.OK)
      {
        fpSpread1.SuspendLayout();
        this.Cursor = Cursors.WaitCursor;

        try
        {

          if (ofd.FileName != null && ofd.FileName.Length > 0)
          {

            if (fpSpread1.IsExcelFile(ofd.FileName) == true)
            {


              loadFailed = !fpSpread1.OpenExcel(ofd.FileName, excelOpenFileDlg.OpenFlags);

              if (!loadFailed)
              {
                activeCol = fpSpread1.ActiveSheet.ActiveColumnIndex;
                activeRow = fpSpread1.ActiveSheet.ActiveRowIndex;
              }

              glbFileName = null;
            }
            else if (fpSpread1.IsSpreadSSFile(ofd.FileName) == true)
            {

              if (fpSpread1.Open(ofd.FileName) == false)
              {
                loadFailed = true;

                glbFileName = null;
              }
              else
              {

                if (fpSpread1.IsSpreadSSFile(ofd.FileName) == true)
                {
                  resetOverrides = true;

                  glbFileName = null;
                }
                else
                {

                  glbFileName = ofd.FileName;
                }
              }
            }
            else if (fpSpread1.IsSpreadXmlFile(ofd.FileName) == true)
            {

              if (fpSpread1.Open(ofd.FileName) == false)
              {
                loadFailed = true;

                glbFileName = ofd.FileName;
              }
              else
              {
                if (fpSpread1.ActiveSheet != null)
                {
                  activeCol = fpSpread1.ActiveSheet.ActiveColumnIndex;
                  activeRow = fpSpread1.ActiveSheet.ActiveRowIndex;
                }

                glbFileName = null;
              }
            }
            else
            {

              glbFileName = null;


              if (ofd.FileName.ToUpper().EndsWith(".TXT") || ofd.FileName.ToUpper().EndsWith(".CSV"))
              {

                TextOptionsDlg txtOptions = new TextOptionsDlg();

                fpSpread1.Invalidate();


                if (ofd.FileName.ToUpper().EndsWith(".CSV"))
                {
                  txtOptions.ColumnDelim.Text = rm.GetString("txtOptions.ColumnDelim.Text");
                  txtOptions.RowDelim.Text = rm.GetString("txtOptions.RowDelim.Text");
                  txtOptions.CellDelim.Text = "";
                  txtOptions.TextOptionsNoteArea.Text = rm.GetString("TextOptionsNoteAreaA");
                }
                else
                {

                  txtOptions.ColumnDelim.Text = rm.GetString("txtOptions.ColumnDelim.Text_1");
                  txtOptions.RowDelim.Text = rm.GetString("txtOptions.RowDelim.Text_1");
                  txtOptions.CellDelim.Text = "";
                  txtOptions.TextOptionsNoteArea.Text = rm.GetString("TextOptionsNoteAreaB");
                }
                fpSpread1.ResumeLayout();
                if (txtOptions.ShowDialog(this) == DialogResult.OK)
                {

                  string cellDelimiter = null;
                  string columnDelimiter = null;
                  string rowDelimiter = null;
                  FarPoint.Win.Spread.Model.IncludeHeaders includeHeaders;
                  if (txtOptions.IncludeColumnHdr.Checked = true && txtOptions.IncludeRowHdr.Checked == true)
                  {
                    includeHeaders = FarPoint.Win.Spread.Model.IncludeHeaders.BothCustomOnly;
                  }
                  else if (txtOptions.IncludeColumnHdr.Checked = true && txtOptions.IncludeRowHdr.Checked == false)
                  {
                    includeHeaders = FarPoint.Win.Spread.Model.IncludeHeaders.ColumnHeadersCustomOnly;
                  }
                  else if (txtOptions.IncludeColumnHdr.Checked = false && txtOptions.IncludeRowHdr.Checked == true)
                  {
                    includeHeaders = FarPoint.Win.Spread.Model.IncludeHeaders.RowHeadersCustomOnly;
                  }
                  else
                  {
                    includeHeaders = FarPoint.Win.Spread.Model.IncludeHeaders.None;
                  }

                  if (txtOptions.RowDelim.Text.Length != 0)
                  {
                    if (txtOptions.RowDelim.Text == "<tab>")
                    {
                      rowDelimiter = Char.ConvertFromUtf32(9);    //Keys.Tab
                    }
                    else if (txtOptions.RowDelim.Text == "<cr>")
                    {
                      rowDelimiter = Char.ConvertFromUtf32(13);   //Keys.Enter
                    }
                    else if (txtOptions.RowDelim.Text == "<lf>")
                    {
                      rowDelimiter = Char.ConvertFromUtf32(10);   //Keys.LineFeed
                    }
                    else if (txtOptions.RowDelim.Text == "<cr><lf>")
                    {
                      rowDelimiter = Char.ConvertFromUtf32(13) + Char.ConvertFromUtf32(10);   //Keys.Enter + Keys.LineFeed
                    }
                    else
                    {
                      rowDelimiter = txtOptions.RowDelim.Text;
                    }
                  }

                  if (txtOptions.ColumnDelim.Text.Length != 0)
                  {
                    if (txtOptions.ColumnDelim.Text == "<tab>")
                    {
                      columnDelimiter = Char.ConvertFromUtf32(9);    //Keys.Tab
                    }
                    else if (txtOptions.ColumnDelim.Text == "<cr>")
                    {
                      columnDelimiter = Char.ConvertFromUtf32(13);   //Keys.Enter
                    }
                    else if (txtOptions.ColumnDelim.Text == "<lf>")
                    {
                      columnDelimiter = Char.ConvertFromUtf32(10);   //Keys.LineFeed
                    }
                    else if (txtOptions.ColumnDelim.Text == "<cr><lf>")
                    {
                      columnDelimiter = Char.ConvertFromUtf32(13) + Char.ConvertFromUtf32(10);   //Keys.Enter + Keys.LineFeed
                    }
                    else
                    {
                      columnDelimiter = txtOptions.ColumnDelim.Text;
                    }
                  }

                  if (txtOptions.CellDelim.Text.Length != 0)
                  {
                    if (txtOptions.CellDelim.Text == "<tab>")
                    {
                      cellDelimiter = Char.ConvertFromUtf32(9);    //Keys.Tab
                    }
                    else if (txtOptions.CellDelim.Text == "<cr>")
                    {
                      cellDelimiter = Char.ConvertFromUtf32(13);   //Keys.Enter
                    }
                    else if (txtOptions.CellDelim.Text == "<lf>")
                    {
                      cellDelimiter = Char.ConvertFromUtf32(10);   //Keys.LineFeed
                    }
                    else if (txtOptions.CellDelim.Text == "<cr><lf>")
                    {
                      cellDelimiter = Char.ConvertFromUtf32(13) + Char.ConvertFromUtf32(10);   //Keys.Enter + Keys.LineFeed
                    }
                    else
                    {
                      cellDelimiter = txtOptions.CellDelim.Text;
                    }
                  }


                  fpSpread1.ActiveSheet.LoadTextFile(ofd.FileName, !(txtOptions.TextOptionsFormatted.Checked), includeHeaders, rowDelimiter, columnDelimiter, cellDelimiter);

                }
                fpSpread1.SuspendLayout();
              }
              else
              {

                try
                {
                  if (fpSpread1.Open(ofd.FileName) == false)
                  {
                    loadFailed = true;

                    glbFileName = null;
                  }
                  else
                  {
                    if (fpSpread1.IsSpreadSSFile(ofd.FileName) == true)
                    {
                      resetOverrides = true;
                    }
                  }
                }
                catch (Exception)
                {
                  loadFailed = true;
                }
              }
            }
          }
        }
        catch (Exception)
        {

          loadFailed = true;
        }
      }

      if (loadFailed == true)
      {
        this.Cursor = Cursors.Default;
        fpSpread1.ResumeLayout(true);
      }
      else
      {
        if (activeCol > 0)
        {
          fpSpread1.ActiveSheet.ActiveColumnIndex = activeCol;
        }
        if (activeRow > 0)
        {
          fpSpread1.ActiveSheet.ActiveRowIndex = activeRow;
        }
        this.Cursor = Cursors.Default;
        fpSpread1.ResumeLayout(true);
      }
      excelOpenFileDlg.Dispose();
    }

    FarPoint.Excel.ExcelSaveFlags saveFlags;
    private bool FileSave(bool useDialog)
    {
      ExcelSaveFileDialog excelSaveFileDialog = new ExcelSaveFileDialog();
      SaveFileDialog sf = (excelSaveFileDialog.Dialog as SaveFileDialog);
      sf.Filter = rm.GetString("sf.Filter_1");
      sf.FilterIndex = 0;
      sf.CheckFileExists = false;
      sf.CheckPathExists = true;
      sf.DefaultExt = "xml";

      if (glbFileName != null)
      {
        sf.FileName = glbFileName;
        if (glbFileName.EndsWith(".xls"))
        {
          sf.FilterIndex = 2;
        }

        if (glbFileName.EndsWith(".xlsx"))
        {
          sf.FilterIndex = 3;
        }
      }
      else
      {
        useDialog = true;
      }

      if (useDialog == true)
      {
        if (glbFileName != null)
        {
          sf.FileName = sf.FileName.Insert(sf.FileName.LastIndexOf("\\") + 1, "副本 ");
        }
        this.Activate();
        DialogResult dr = excelSaveFileDialog.ShowDialog(this);
        if (dr != System.Windows.Forms.DialogResult.OK)
        {
          excelSaveFileDialog.Dispose();
          return false;
        }
      }

      try
      {
        if (sf.FileName != null && sf.FileName.Length > 0)
        {
          if (sf.FilterIndex == 1 && sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xml") == false)
          {
            sf.FileName = sf.FileName + ".xml";
          }

          if (sf.FilterIndex == 2 && sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xls") == false)
          {
            sf.FileName = sf.FileName + ".xls";
          }

          if (sf.FilterIndex == 3 && sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xlsx") == false)
          {
            sf.FileName = sf.FileName + ".xlsx";
          }

          if (sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xls") == true)
          {
            this.Cursor = Cursors.WaitCursor;
            if (useDialog)
            {
              saveFlags = excelSaveFileDialog.SaveFlags;
            }
            else
            {
              if (saveFlags != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet)
              {
                string flagsStringList = String.Empty;
                int flagsCount = 0;
                foreach (FarPoint.Excel.ExcelSaveFlags f in Enum.GetValues(typeof(FarPoint.Excel.ExcelSaveFlags)))
                {
                  if (f != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet && f != FarPoint.Excel.ExcelSaveFlags.UseOOXMLFormat && ((f & saveFlags) != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet))
                  {
                    flagsStringList += f.ToString() + ", ";
                    flagsCount = flagsCount + 1;
                  }
                }
                flagsStringList = flagsStringList.TrimEnd().TrimEnd(new char[] { ',' });
                string msg = (flagsCount > 1 ? rm.GetString("ConfirmSaveFlags2") : rm.GetString("ConfirmSaveFlags3"));
                if (MessageBox.Show(rm.GetString("ConfirmSaveFlags") + flagsStringList + " " + msg + System.Environment.NewLine + rm.GetString("ConfirmSaveFlags1"), rm.GetString("ConfirmSaveFlagsCaption"), MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                {
                  saveFlags = FarPoint.Excel.ExcelSaveFlags.NoFlagsSet;
                }
              }
              if (fpSpread1.SaveExcel(sf.FileName, saveFlags) == false)
              {
                saveFlags = FarPoint.Excel.ExcelSaveFlags.NoFlagsSet;
                MessageBox.Show(rm.GetString("SaveFileError"), rm.GetString("DesignerErrorTitle"));
              }
              else
              {
                glbFileName = sf.FileName;
              }

              this.Cursor = Cursors.Default;
            }
          }
          else if (sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xlsx") == true)
          {
            this.Cursor = Cursors.WaitCursor;
            if (useDialog)
            {
              saveFlags = excelSaveFileDialog.SaveFlags | FarPoint.Excel.ExcelSaveFlags.UseOOXMLFormat;
            }
            else
            {
              if (saveFlags != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet)
              {
                string flagsStringList = String.Empty;
                int flagsCount = 0;
                foreach (FarPoint.Excel.ExcelSaveFlags f in Enum.GetValues(typeof(FarPoint.Excel.ExcelSaveFlags)))
                {
                  if (f != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet && f != FarPoint.Excel.ExcelSaveFlags.UseOOXMLFormat && ((f & saveFlags) != FarPoint.Excel.ExcelSaveFlags.NoFlagsSet))
                  {
                    flagsStringList += f.ToString() + ", ";
                    flagsCount = flagsCount + 1;
                  }
                }
                flagsStringList = flagsStringList.TrimEnd().TrimEnd(new char[] { ',' });
                string msg = (flagsCount > 1 ? rm.GetString("ConfirmSaveFlags2") : rm.GetString("ConfirmSaveFlags3"));
                flagsStringList = flagsStringList.TrimEnd().TrimEnd(new char[] { ',' });
                if (MessageBox.Show(rm.GetString("ConfirmSaveFlags") + flagsStringList + " " + msg + System.Environment.NewLine + rm.GetString("ConfirmSaveFlags1"), rm.GetString("ConfirmSaveFlagsCaption"), MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                {
                  saveFlags = FarPoint.Excel.ExcelSaveFlags.UseOOXMLFormat;
                }
              }
            }
            if (fpSpread1.SaveExcel(sf.FileName, saveFlags) == false)
            {
              saveFlags = FarPoint.Excel.ExcelSaveFlags.NoFlagsSet;
              MessageBox.Show(rm.GetString("SaveFileError"), rm.GetString("DesignerErrorTitle"));
            }
            else
            {
              glbFileName = sf.FileName;
            }
            this.Cursor = Cursors.Default;
          }
          else
          {
            if (sf.FileName.ToLower(System.Globalization.CultureInfo.InvariantCulture).EndsWith(".xml") == false)
            {
              if (MessageBox.Show(rm.GetString("SaveXMLNoExtension"), rm.GetString("DesignerErrorTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
              {
                excelSaveFileDialog.Dispose();
                return true;
              }
            }
            this.Cursor = Cursors.WaitCursor;
            if (fpSpread1.Save(sf.FileName, false) == false)
            {
              MessageBox.Show(rm.GetString("SaveFileError"), rm.GetString("DesignerErrorTitle"));
            }
            else
            {
              glbFileName = sf.FileName;
            }
            this.Cursor = Cursors.Default;
          }

        }

      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
        excelSaveFileDialog.Dispose();
        return false;
      }
      finally
      {
        excelSaveFileDialog.Dispose();
      }

      return true;
    }
    #endregion

    #region   (Spread Events)
    // (Select change)
    private void fpSpread1_SelectionChanged(object sender, FarPoint.Win.Spread.SelectionChangedEventArgs e)
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      font_SelectionChanged();
      sparkline_SelectionChanged();
      Celltype_SelectionChanged();
      alignment_SelectionChanged();
      LoadTableRibbon();
      cellLock_selectionChange();
    }
    private void cellLock_selectionChange()
    {
      rbLock.Pressed = this.fpSpread1.ActiveSheet.ActiveCell.Locked;
      rbMerge.Pressed = this.fpSpread1.ActiveSheet.ActiveCell.RowSpan > 1 || this.fpSpread1.ActiveSheet.ActiveCell.ColumnSpan > 1;
    }

    private void font_SelectionChanged()
    {
      Font font = this.fpSpread1.ActiveSheet.ActiveCell.Font;
      if (font == null)
      {
        font = fpSpread1.Font;
      }
      bool none = font == null;
      rbBold.Pressed = none ? false : font.Bold;
      rbItalic.Pressed = none ? false : font.Italic;
      rbUnderLine.Pressed = none ? false : font.Underline;
      //FontStrikeoutButton.Pressed = none ? false : font.Strikeout;
      this.rccFillColor.Color = this.fpSpread1.ActiveSheet.ActiveCell.BackColor;
      this.rccFontColor.Color = this.fpSpread1.ActiveSheet.ActiveCell.ForeColor;
      rcbFont.Text = none ? "" : font.FontFamily.Name;
      rcbFontSize.Text = none ? "" : font.Size.ToString();
    }

    private void alignment_SelectionChanged()
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      rbMiddleAlign.Pressed = false;
      rbBottomAlign.Pressed = false;
      rbTopAlign.Pressed = false;
      rbVerticalJustify.Pressed = false;
      rbVerticalDistributed.Pressed = false;
      rbAlignTextLeft.Pressed = false;
      rbAlignTextMiddle.Pressed = false;
      rbAlignTextRight.Pressed = false;
      rbHorizontalJustify.Pressed = false;

      rbHorizontalDistributed.Pressed = false;
      switch (sheet.ActiveCell.VerticalAlignment)
      {
        case CellVerticalAlignment.Center:
          rbMiddleAlign.Pressed = true;
          break;
        case CellVerticalAlignment.Bottom:
          rbBottomAlign.Pressed = true;
          break;
        case CellVerticalAlignment.Top:
          rbTopAlign.Pressed = true;
          break;

        case CellVerticalAlignment.Justify:
          rbVerticalJustify.Pressed = true;
          break;
        case CellVerticalAlignment.Distributed:
          rbVerticalDistributed.Pressed = true;
          break;
      }
      switch (sheet.ActiveCell.HorizontalAlignment)
      {
        case CellHorizontalAlignment.Left:
          rbAlignTextLeft.Pressed = true;
          break;
        case CellHorizontalAlignment.Center:
          rbAlignTextMiddle.Pressed = true;
          break;
        case CellHorizontalAlignment.Right:
          rbAlignTextRight.Pressed = true;
          break;

        case CellHorizontalAlignment.Justify:
          rbHorizontalJustify.Pressed = true;
          break;
        case CellHorizontalAlignment.Distributed:
          rbHorizontalDistributed.Pressed = true;
          break;
      }
    }

    private void sparkline_SelectionChanged()
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      bool enableGroup = false;
      bool enableUngroup = false;

      if (sheet.ActiveCell.Sparkline != null)
      {
        rtSparkline.Visible = true;
        FarPoint.Win.Spread.ISparklineGroup outGroup;
        FarPoint.Win.Spread.ISparkline outSL;
        fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(fpSpread1.ActiveSheet.ActiveRowIndex,
             fpSpread1.ActiveSheet.ActiveColumnIndex, out outGroup, out outSL);
        ExcelSparklineSetting excelSparklineSetting = outGroup.Setting as ExcelSparklineSetting;
        switch (outGroup.Renderer.ToString())
        {
          case "FarPoint.Win.Spread.LineSparklineRenderer":
            rbSparklineMarkerColorMarker.Enabled = true;
            rbSparklineMarkerColorMarker.Checked = excelSparklineSetting.ShowMarkers;
            rbSparklineMarkers.Enabled = true;
            rbSparklineMarkers.Color = excelSparklineSetting.MarkersColor;
            rbSparklineWeight.Enabled = true;
            rbSparklineTypeLine.Pressed = true;
            rbSparklineTypeColumn.Pressed = false;
            rbSparklineTypeWinLoss.Pressed = false;
            break;
          case "FarPoint.Win.Spread.ColumnSparklineRenderer":
            rbSparklineTypeColumn.Pressed = true;
            rbSparklineTypeWinLoss.Pressed = false;
            rbSparklineTypeLine.Pressed = false;
            rbSparklineMarkerColorMarker.Enabled = false;
            rbSparklineMarkers.Enabled = false;
            rbSparklineWeight.Enabled = false;
            break;
          case "FarPoint.Win.Spread.WinLossSparklineRenderer":
            rbSparklineTypeWinLoss.Pressed = true;
            rbSparklineTypeLine.Pressed = false;
            rbSparklineTypeColumn.Pressed = false;
            rbSparklineMarkerColorMarker.Enabled = false;
            rbSparklineMarkers.Enabled = false;
            rbSparklineWeight.Enabled = false;
            break;
        }
        if (excelSparklineSetting != null)
        {
          rbSparklineHighPoint.Checked = excelSparklineSetting.ShowHigh;
          rbSparklineLowPoint.Checked = excelSparklineSetting.ShowLow;
          rbSparklineMarkerColorNegativePoint.Checked = excelSparklineSetting.ShowNegative;
          rbSparklineFirstPoint.Checked = excelSparklineSetting.ShowFirst;
          rbSparklineLastPoint.Checked = excelSparklineSetting.ShowLast;

          rbSparklineColor.Color = excelSparklineSetting.SeriesColor;
          rbSparklineNegativePoints.Color = excelSparklineSetting.NegativeColor;
          rbSparklineMarkerColorHighPoint.Color = excelSparklineSetting.HighMarkerColor;
          rbSparklineMarkerColorLowPoint.Color = excelSparklineSetting.LowMarkerColor;
          rbSparklineMarkerColorFirstPoint.Color = excelSparklineSetting.FirstMarkerColor;
          rbSparklineMarkerColorLastPoint.Color = excelSparklineSetting.LastMarkerColor;
          if (excelSparklineSetting.DateAxis)
          {
            rbSparklineAxisSub1.Pressed = false;
            rbSparklineAxisSub2.Pressed = true;
          }
          else
          {
            rbSparklineAxisSub1.Pressed = true;
            rbSparklineAxisSub2.Pressed = false;
          }
          rbSparklineAxisSub3.Pressed = excelSparklineSetting.DisplayXAxis;
          rbSparklineAxisSub4.Pressed = excelSparklineSetting.RightToLeft;
          rbSparklineAxisSub5.Pressed = excelSparklineSetting.MinAxisType == SparklineAxisMinMax.Individual;
          rbSparklineAxisSub6.Pressed = excelSparklineSetting.MinAxisType == SparklineAxisMinMax.Group;
          rbSparklineAxisSub7.Pressed = excelSparklineSetting.MinAxisType == SparklineAxisMinMax.Custom;
          rbSparklineAxisSub8.Pressed = excelSparklineSetting.MaxAxisType == SparklineAxisMinMax.Individual;
          rbSparklineAxisSub9.Pressed = excelSparklineSetting.MaxAxisType == SparklineAxisMinMax.Group;
          rbSparklineAxisSub10.Pressed = excelSparklineSetting.MaxAxisType == SparklineAxisMinMax.Custom;
        }
        CellRange[] cellRanges = GetSpreadSelections();
        rbMerge.Pressed = false;
        foreach (CellRange range in cellRanges)
        {
          for (int i = 0; i < range.RowCount; i++)
          {
            for (int j = 0; j < range.ColumnCount; j++)
            {
              if (sheet.Cells[range.Row + i, range.Column + j].Sparkline != null)
              {
                FarPoint.Win.Spread.ISparklineGroup sparklineGroup;
                FarPoint.Win.Spread.ISparkline sparkline;
                fpSpread1.ActiveSheet.SparklineContainer.GetSparkline(range.Row + i, range.Column + j, out sparklineGroup, out sparkline);
                if (sparklineGroup != outGroup)
                {
                  enableGroup = true;
                }
                if (sparklineGroup.Count > 1)
                {
                  enableUngroup = true;
                }
              }
            }
          }
        }
        rbSparklineGroup.Enabled = enableGroup;
        rbSparklineUngroup.Enabled = enableUngroup;
      }
      else rtSparkline.Visible = false;
    }

    private void Celltype_SelectionChanged()
    {
      ICellType celltype = fpSpread1.ActiveSheet.ActiveCell.CellType;
      if (celltype != null)
      {
        rbWrapText.Enabled = celltype is TextCellType;
        if(rbWrapText.Enabled)
        {
          rbWrapText.Pressed = ((TextCellType)celltype).WordWrap;
        }
        else
        {
          rbWrapText.Pressed = false;
        }
        if (celltype is TextCellType) {rcbCellType.SelectedIndex =  rcbCellType.Items.IndexOf(rbdwText); }
        else if (celltype is ImageCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwPicture); }
        else if (celltype is GeneralCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwGeneral); }
        else if (celltype is NumberCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwNumber); }
        else if (celltype is PercentCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwPercent); }
        else if (celltype is CurrencyCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwCurrency); }
        else if (celltype is BarCodeCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwBarCode); }
        else if (celltype is ButtonCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwButton); }
        else if (celltype is CheckBoxCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwCheckBox); }
        else if (celltype is ComboBoxCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwComboBox); }
        else if (celltype is MultiOptionCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwMultiOption); }
        else if (celltype is RegularExpressionCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwRegularExpression); }
        else if (celltype is MultiColumnComboBoxCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwMultiColumnComboBox); }
        else if (celltype is ListBoxCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwListBox); }
        else if (celltype is RichTextCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwRichText); }
        else if (celltype is DateTimeCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwDateTime); }
        else if (celltype is ColorPickerCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwColorPicker); }
        else if (celltype is ProgressCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwProgress); }
        else if (celltype is SliderCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwSlider); }
        else if (celltype is MaskCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwMask); }
        else if (celltype is HyperLinkCellType) { rcbCellType.SelectedIndex = rcbCellType.Items.IndexOf(rbdwHyperLink); }

        rcbCellType.Text = rcbCellType.SelectedItem.Text;
        rcbCellType.LargeImage = rcbCellType.SelectedItem.LargeImage;
        rbClearCellType.Enabled = true;
      }
      else
      {
        rbWrapText.Enabled = false;
        rbClearCellType.Enabled = false;
        rcbCellType.Text = "None";
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadDesigner));
        rcbCellType.LargeImage = ((System.Drawing.Image)(resources.GetObject("rcbCellType.LargeImage")));
      }
    }

    private void OnActionComplete(Object sender, FarPoint.Win.Spread.UndoRedo.UndoRedoEventArgs e)
    {
      SynUndoReDoState();
      fpSpread1.Invalidate();
    }

    private void OnUndoComplete(Object sender, FarPoint.Win.Spread.UndoRedo.UndoRedoEventArgs e)
    {
      SynUndoReDoState();
      fpSpread1.Invalidate();
    }

    private void OnRedoComplete(Object sender, FarPoint.Win.Spread.UndoRedo.UndoRedoEventArgs e)
    {
      SynUndoReDoState();
      fpSpread1.Invalidate();
    }

    private void btnFormulaEdit_Click(object sender, EventArgs e)
    {
      FarPoint.Win.Spread.FormulaEditorUI formulaEditor = new FarPoint.Win.Spread.FormulaEditorUI(fpSpread1.ActiveSheet);

      IEnumerator en = default(IEnumerator);
      en = fpSpread1.ActiveSheet.GetCustomFunctionEnumerator();
      while ((en != null))
      {
        if ((en.MoveNext() == true))
        {
          formulaEditor.AddCustomFunction(fpSpread1.ActiveSheet.GetCustomFunction(en.Current.ToString()));
        }
        else
        {
          break;
        }
      }

      string formulaText = null;

      formulaText = this.formulaTextBox1.Text;

      if ((formulaText.StartsWith("=") == true))
      {
        formulaText = formulaText.Remove(0, 1);
      }

      bool hasFormula = false;
      if (this.fpSpread1.ActiveSheet.ActiveCell != null && this.fpSpread1.ActiveSheet.ActiveCell.Formula != string.Empty)
      {
        hasFormula = true;
      }
      if (!hasFormula)
      {
        formulaEditor.SetFormula(null);
      }
      else
      {
        formulaEditor.SetFormula(formulaText);
      }

      formulaEditor.Icon = this.Icon;
      try
      {
        if ((formulaEditor.ShowDialog(this) == System.Windows.Forms.DialogResult.OK))
        {
          string formula = formulaEditor.GetFormula();
          if ((formula != null))
          {
            if ((formula.Length != 0))
            {
              if ((formula.StartsWith("=")))
              {
                formulaTextBox1.Text = formula;
              }
              else
              {
                formulaTextBox1.Text = "=" + formula;
              }
            }
          }
          formulaTextBox1.SelectionStart = formulaTextBox1.TextLength;
        }
      }
      catch (FarPoint.Win.Spread.Model.ParseException ex)
      {
        string formula = formulaEditor.GetFormula();
        if ((formula != null))
        {
          if ((formula.Length != 0))
          {
            if ((formula.StartsWith("=")))
            {
              formulaTextBox1.Text = formula;
            }
            else
            {
              formulaTextBox1.Text = "=" + formula;
            }
          }
        }
        formulaTextBox1.SelectionStart = formulaTextBox1.TextLength;
      }

    }

    void fpSpread1_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button != System.Windows.Forms.MouseButtons.Right & e.Button != System.Windows.Forms.MouseButtons.Left) return;

      if (fpSpread1.ActiveSheet == null)
      {
        fpSpread1.ContextMenu = null;
        return;
      }

      Point pnt = new Point(e.X, e.Y);

      if (e.Button == System.Windows.Forms.MouseButtons.Right)
      {
        if (fpSpread1.ActiveSheet != null)
        {
          //cellNoteObj = fpSpread1.ActiveSheet.NotesContainer.ChildContains(pnt, false);
          if (cellNoteObj != null)
          {
            fpSpread1.ContextMenu = noteContextMenu;
          }


          HitTestInformation hitTestInfo;
          hitTestInfo = fpSpread1.HitTest(e.X, e.Y);
          if (hitTestInfo != null)
          {
            if (hitTestInfo.HeaderInfo != null && hitTestInfo.HeaderInfo.Column == -1 && hitTestInfo.HeaderInfo.Row == -1)
            {
              fpSpread1.ContextMenu = null;
            }

            if (hitTestInfo.TabStripInfo != null && hitTestInfo.TabStripInfo.Sheet >= 0 && hitTestInfo.TabStripInfo.Sheet < fpSpread1.Sheets.Count)
            {
              fpSpread1.ActiveWindowlessObject = null;
              fpSpread1.ContextMenu = sheetTabContextMenu;
              sheetTabContextMenu_Sheet = hitTestInfo.TabStripInfo.Sheet;
              fpSpread1.ActiveSheetIndex = sheetTabContextMenu_Sheet;
              return;
            }

            if (hitTestInfo.ViewportInfo == null)
            {
              fpSpread1.ContextMenu = null;
            }
            else if (hitTestInfo.ViewportInfo.InShape && fpSpread1.ActiveWindowlessObject is FarPoint.Win.Spread.DrawingSpace.SpreadCameraShape)
            {
              fpSpread1.ContextMenu = shapeContextMenu;
              return;
            }
            else if (hitTestInfo.ViewportInfo.Column == -1 & hitTestInfo.ViewportInfo.Row == -1)
            {
              // do nothing
            }


          }
        }
      }

      if (fpSpread1.ActiveSheet.RowCount == 0 | fpSpread1.ActiveSheet.ColumnCount == 0)
      {
        fpSpread1.ContextMenu = null;
        return;
      }

      SheetView sheet = this.getChildSheetFormPoint(fpSpread1.GetRootWorkbook(), pnt);
      if (sheet != null && sheet.DrawingContainer != null)
      {
        pnt = sheet.DrawingContainer.AdjustPointForViewport(pnt);
        psObjMenu = sheet.DrawingContainer.ChildContains(pnt, false);
        if (psObjMenu == null)
        {
          SheetView parentSheet = sheet.Parent;
          while (parentSheet != null)
          {
            if (parentSheet.DrawingContainer != null)
            {
              pnt = parentSheet.DrawingContainer.AdjustPointForViewport(e.Location);
              psObjMenu = parentSheet.DrawingContainer.ChildContains(pnt, false);

              if (psObjMenu == null)
              {
                parentSheet = parentSheet.Parent;
              }
            }
          }
        }

        if (fpSpread1.ActiveWindowlessObject != null & (fpSpread1.ActiveWindowlessObject is FarPoint.Win.Spread.DrawingSpace.Internal.StickyNoteCommentBase))
        {
          fpSpread1.ContextMenu = null;
          return;
        }

        if (psObjMenu == null)
        {
          fpSpread1.ContextMenu = contextMenu1;
        }
        else if (!(psObjMenu is FarPoint.Win.Spread.DrawingSpace.Internal.StickyNoteCommentBase))
        {
          if (psObjMenu is FarPoint.Win.Spread.Chart.SpreadChart)
          {
            fpSpread1.ContextMenu = null;
          }
          else
          {
            fpSpread1.ContextMenu = shapeContextMenu;
            if (psObjMenu.Locked & fpSpread1.ActiveSheet.Protect)
            {
              shapeContextCut.Enabled = false;
              shapeContextCopy.Enabled = false;
            }
            else
            {
              shapeContextCut.Enabled = true;
              shapeContextCopy.Enabled = true;
            }
          }

          fpSpread1.ContextMenu = shapeContextMenu;
          if (psObjMenu.Locked & fpSpread1.ActiveSheet.Protect)
          {
            shapeContextCut.Enabled = false;
            shapeContextCopy.Enabled = false;
          }
          else
          {
            shapeContextCut.Enabled = true;
            shapeContextCopy.Enabled = true;
          }

          if (psObjMenu.Locked & fpSpread1.ActiveSheet.Protect == false)
          {
            fpSpread1.ActiveWindowlessObject = psObjMenu;
          }
        }
        else
        {
          fpSpread1.ContextMenu = null;
        }
      }
      else
      {
        fpSpread1.ContextMenu = contextMenu1;
      }

      if (fpSpread1.ActiveSheet != null)
      {
        //cellNoteObj = fpSpread1.ActiveSheet.NotesContainer.ChildContains(pnt, false);
        //if (cellNoteObj != null)
        //{
        //    fpSpread1.ContextMenu = noteContextMenu;
        //}
      }

    }

    void fpSpread1_MouseUp(object sender, MouseEventArgs e)
    {

    }

    void fpSpread1_ActiveSheetChanged(object sender, EventArgs e)
    {
      psObjMenu = fpSpread1.ActiveWindowlessObject as PSObject;
      if (e is EnterCellEventArgs)
      {
        fpSpread1.ContextMenu = contextMenu1;
      }
      else
      {
        if (psObjMenu is FarPoint.Win.Spread.DrawingSpace.Internal.StickyNoteCommentBase)
        {
          fpSpread1.ContextMenu = null;
        }
        else if (psObjMenu != null)
        {
          if (!(psObjMenu is FarPoint.Win.Spread.Chart.SpreadChart))
          {
            fpSpread1.ContextMenu = shapeContextMenu;
          }
          else
          {
            fpSpread1.ContextMenu = null;
          }
        }
      }
    }

    void fpSpread1_CellClick(object sender, CellClickEventArgs e)
    {

    }

    private void fpSpread1_EnterCell(object sender, EnterCellEventArgs e)
    {
      LoadTableRibbon();
    }

    void fpSpread1_SheetTabClick(object sender, SheetTabClickEventArgs e)
    {
      if (e.SheetTabIndex == -1) return;

      if (MouseButtons == System.Windows.Forms.MouseButtons.Right)
      {
        if (e.SheetTabIndex == fpSpread1.Sheets.Count)
        {
          return;
        }
      }

      fpSpread1.ActiveSheetIndex = e.SheetTabIndex;
      //if (e.SheetTabIndex == fpSpread1.ActiveSheetIndex)
      //{

      //}
      SetupContextMenu(false, false, true);
    }

    void fpSpread1_SheetTabDoubleClick(object sender, SheetTabDoubleClickEventArgs e)
    {
      if (e.SheetTabIndex == -1) return;

      RenameSheetInPlace(e.SheetTabIndex);
      e.Cancel = true;
    }

    private void fpSpread1_ShapeActivated(object sender, EventArgs e)
    {
      if (fpSpread1.ActiveWindowlessObject is SpreadChart)
      {
        rtChartTools.Visible = true;
        c1Ribbon1.SelectedTab = rtChartTools;
        SpreadChart spreadChart = (SpreadChart)fpSpread1.ActiveWindowlessObject;
        switch (spreadChart.CanMove)
        {
          case Moving.Horizontal:
            rbChartMove.SelectedItem = rbChartMoveH;
            break;
          case Moving.HorizontalAndVertical:
            rbChartMove.SelectedItem = rbChartMoveHV;
            break;
          case Moving.None:
            rbChartMove.SelectedItem = rbChartMoveNone;
            break;
          case Moving.Vertical:
            rbChartMove.SelectedItem = rbChartMoveV;
            break;
        }
        switch (spreadChart.CanSize)
        {
          case Sizing.Height:
            rbChartSize.SelectedItem = rbChartSizeHeight;
            break;
          case Sizing.HeightAndWidth:
            rbChartSize.SelectedItem = rbChartSizeBoth;
            break;
          case Sizing.None:
            rbChartSize.SelectedItem = rbChartSizeNone;
            break;
          case Sizing.Width:
            rbChartSize.SelectedItem = rbChartSizeWidth;
            break;
        }
        rbChartSizeWithCell.Checked = spreadChart.DynamicSize;
        rbChartMoveWithCell.Checked = spreadChart.DynamicMove;
      }
      else
      {
        rtDrawingTools.Visible = true;
        c1Ribbon1.SelectedTab = rtDrawingTools;
        PSShape shape = fpSpread1.ActiveWindowlessObject as PSShape;
        if (shape != null)
        {
          this.RibbonColorChooser1.Color = shape.BackColor;
          this.RibbonColorChooser3.Color = shape.ShapeOutlineColor;
          this.RibbonColorChooser2.Color = shape.ForeColor;
          if (shape.Picture == null || shape is SpreadCameraShape)
          {
            this.rbClearPicture.Enabled = false;
          }
          else
          {
            this.rbClearPicture.Enabled = true;
          }
          if (shape is SpreadCameraShape)
          {
            this.rbSetPicture.Enabled = false;
          }
          else
          {
            this.rbSetPicture.Enabled = true;
          }
        }
      }
      rbDeleteShape.Enabled = true;
    }

    private void fpSpread1_ShapeDeactivated(object sender, EventArgs e)
    {
      rtDrawingTools.Visible = false;
      rtChartTools.Visible = false;
      if (c1Ribbon1.SelectedTab == rtDrawingTools || c1Ribbon1.SelectedTab == rtChartTools)
        c1Ribbon1.SelectedTab = rtHome;
      rbDeleteShape.Enabled = false;
    }

    void RenameTextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter | e.KeyCode == Keys.Escape)
      {
        if (e.KeyCode == Keys.Enter && RenameTextBox.Text.Length > 0)
        {
          try
          {
            string newSheetName = RenameTextBox.Text;
            RenameTextBox.Text = string.Empty;
            fpSpread1.ActiveSheet.SheetName = newSheetName;
          }
          catch (ArgumentException ex)
          {
            MessageBox.Show(ex.InnerException.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information);
          }

          HideRenameTextBox();

        }
      }
    }

    void RenameTextBox_LostFocus(object sender, System.EventArgs e)
    {
      if (RenameTextBox.Text.Length > 0)
      {
        try
        {
          fpSpread1.ActiveSheet.SheetName = RenameTextBox.Text;
        }
        catch (ArgumentException ex)
        {
          MessageBox.Show(ex.InnerException.Message, rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }

      HideRenameTextBox();
    }

    private SheetView getChildSheetFormPoint(SpreadView spreadView, Point point)
    {
      SheetView sheet = spreadView.GetSheetView();
      if (sheet == null || sheet.DrawingContainer == null)
      {
        return null;
      }

      if (spreadView.Bounds.Contains(point.X, point.X))
      {
        ArrayList childs = spreadView.GetChildWorkbooks();
        if (childs != null && childs.Count > 0)
        {
          for (int i = 0; i < childs.Count; i++)
          {
            SheetView tempSheet = getChildSheetFormPoint((childs[i] as SpreadView), point);
            if (tempSheet != null)
            {
              return tempSheet;
            }
          }
        }
        return spreadView.GetSheetView();
      }
      else
      {
        return null;
      }
    }

    private void HideRenameTextBox()
    {
      if (RenameTextBox.Visible == true)
      {
        RenameTextBox.Text = null;
        RenameTextBox.Visible = false;
        RenameTextBox.SendToBack();
      }

      fpSpread1.Focus();
    }

    private void fpSpread1_EditModeOff(object sender, EventArgs e)
    {
      foreach (RibbonTab tab in c1Ribbon1.Tabs)
        foreach (RibbonGroup group in tab.Groups)
          group.Enabled = true;
      btnFormulaCancel.Enabled = false;
      btnFormulaSet.Enabled = false;
    }

    private void fpSpread1_EditModeOn(object sender, EventArgs e)
    {
      foreach (RibbonTab tab in c1Ribbon1.Tabs)
        foreach (RibbonGroup group in tab.Groups)
          group.Enabled = false;
      btnFormulaCancel.Enabled = true;
      btnFormulaSet.Enabled = true;
    }

    #endregion

    #region  (Private function)

    private void InitializeRightClick()
    {
      //
      //contextMenu1
      //
//
      this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
	this.menuContext_Sep_Table,
	this.menuContext_Table,
	this.menuContext_Sep_Table1,
	this.menuContext_TableInsert,
	this.menuContext_TableDelete,
	this.menuContext_TableSelect,
 this.menuContext_Celltypes, 
this.menuContext_Sep1, 
this.menuContext_Cut, 
this.menuContext_Copy, 
this.menuContext_Paste, 
this.menuContext_Sep2, 
this.menuContext_Insert, 
this.menuContext_Delete, 
this.menuContext_DeleteSheet, 
this.menuContext_Clear, 
this.menuContext_SelectAll, 
this.menuContext_Sep3, 
this.menuContext_ColumnWidth, 
this.menuContext_RowHeight, 
this.menuContext_AutoFit, 
this.menuContext_Hide, 
this.menuContext_UnHide, 
this.menuContext_Sparkline, 
this.menuContext_Span, 
this.menuContext_Lock, 
this.menuContext_Borders, 
this.menuContext_Headers, 
this.menuContext_SheetSettings, 
this.menuContext_SheetPrintOptions, 
this.menuContext_SheetSkinDesigner, 
this.menuContext_SpreadSkinDesigner, 
this.menuContext_Sep4, 
this.menuContext_EditNote, 
this.menuContext_DeleteNote 
});
//
      //
      //sheetTabContextMenu
      //
      this.sheetTabContextMenu.MenuItems.Add(rm.GetString("menuContext_Copy"));
      this.sheetTabContextMenu.MenuItems.Add(rm.GetString("menuContext_Cut"));
      this.sheetTabContextMenu.MenuItems.Add(rm.GetString("menuContext_Paste"));
      this.sheetTabContextMenu.MenuItems.Add(3, menuContext_DeleteSheet);
      this.sheetTabContextMenu.MenuItems[0].Click += new EventHandler(SheetTabContextMenu_Copy_Click);
      this.sheetTabContextMenu.MenuItems[1].Click += new EventHandler(SheetTabContextMenu_Cut_Click);
      this.sheetTabContextMenu.MenuItems[2].Click += new EventHandler(SheetTabContextMenu_Paste_Click);

      //
      //menuContext_Celltypes
      //
      this.menuContext_Celltypes.Index = 0;
      //>>InputManCellType
      this.menuContext_Celltypes.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.menuContext_Button, this.menuContext_Checkbox, this.menuContext_Combobox, this.menuContext_Currency, this.menuContext_DateTime, this.menuContext_General, this.menuContext_Hyperlink, this.menuContext_Image, this.menuContext_Label, this.menuContext_Mask, this.menuContext_MultiOption, this.menuContext_Number, this.menuContext_Percent, this.menuContext_Progress, this.menuContext_RegEx, this.menuContext_RichText, this.menuContext_Slider, this.menuContext_Text, this.menuContext_GcTextBox, this.menuContext_GcDate, this.menuContext_Sep10, this.menuContext_ClearCellType });
      //<<InputManCellType
      this.menuContext_Celltypes.Text = rm.GetString("menuContext_Celltypes.Text");
      //
      //menuContext_Button
      //
      this.menuContext_Button.Index = 0;
      this.menuContext_Button.Text = rm.GetString("menuContext_Button.Text");
      //
      //menuContext_Checkbox
      //
      this.menuContext_Checkbox.Index = 1;
      this.menuContext_Checkbox.Text = rm.GetString("menuContext_Checkbox.Text");
      //
      //menuContext_Combobox
      //
      this.menuContext_Combobox.Index = 2;
      this.menuContext_Combobox.Text = rm.GetString("menuContext_Combobox.Text");
      //
      //menuContext_Currency
      //
      this.menuContext_Currency.Index = 3;
      this.menuContext_Currency.Text = rm.GetString("menuContext_Currency.Text");
      //
      //menuContext_DateTime
      //
      this.menuContext_DateTime.Index = 4;
      this.menuContext_DateTime.Text = rm.GetString("menuContext_DateTime.Text");
      //
      //menuContext_General
      //
      this.menuContext_General.Index = 5;
      this.menuContext_General.Text = rm.GetString("menuContext_General.Text");
      //
      //menuContext_Hyperlink
      //
      this.menuContext_Hyperlink.Index = 6;
      this.menuContext_Hyperlink.Text = rm.GetString("menuContext_Hyperlink.Text");
      //
      //menuContext_Image
      //
      this.menuContext_Image.Index = 7;
      this.menuContext_Image.Text = rm.GetString("menuContext_Image.Text");
      //
      //menuContext_Label
      //
      this.menuContext_Label.Index = 8;
      this.menuContext_Label.Text = rm.GetString("menuContext_Label.Text");
      //
      //menuContext_Mask
      //
      this.menuContext_Mask.Index = 9;
      this.menuContext_Mask.Text = rm.GetString("menuContext_Mask.Text");
      //
      //menuContext_MultiOption
      //
      this.menuContext_MultiOption.Index = 10;
      this.menuContext_MultiOption.Text = rm.GetString("menuContext_MultiOption.Text");
      //
      //menuContext_Number
      //
      this.menuContext_Number.Index = 11;
      this.menuContext_Number.Text = rm.GetString("menuContext_Number.Text");
      //
      //menuContext_Percent
      //
      this.menuContext_Percent.Index = 12;
      this.menuContext_Percent.Text = rm.GetString("menuContext_Percent.Text");
      //
      //menuContext_Progress
      //
      this.menuContext_Progress.Index = 13;
      this.menuContext_Progress.Text = rm.GetString("menuContext_Progress.Text");
      //
      //menuContext_RegEx
      //
      this.menuContext_RegEx.Index = 14;
      this.menuContext_RegEx.Text = rm.GetString("menuContext_RegEx.Text");
      //
      //menuContext_RichText
      //
      this.menuContext_RichText.Index = 15;
      this.menuContext_RichText.Text = rm.GetString("menuContext_RichText.Text");
      //
      //menuContext_Slider
      //
      this.menuContext_Slider.Index = 16;
      this.menuContext_Slider.Text = rm.GetString("menuContext_Slider.Text");
      //
      //menuContext_Text
      //
      this.menuContext_Text.Index = 17;
      this.menuContext_Text.Text = rm.GetString("menuContext_Text.Text");
      //>>InputManCellType
      this.menuContext_GcDate.Index = 18;
      this.menuContext_GcDate.Text = rm.GetString("menuContext_GcDate.Text");
      this.menuContext_GcTextBox.Index = 19;
      this.menuContext_GcTextBox.Text = rm.GetString("menuContext_GcTextBox.Text");
      //<<InputManCellType
      //
      //menuContext_Sep10
      //
      this.menuContext_Sep10.Index = 20;
      this.menuContext_Sep10.Text = rm.GetString("menuContext_Sep10.Text");
      //
      //menuContext_ClearCellType
      //
      this.menuContext_ClearCellType.Index = 21;
      this.menuContext_ClearCellType.Text = rm.GetString("menuContext_ClearCellType.Text");
      //
      //menuContext_Sep1
      //
      this.menuContext_Sep1.Index = 1;
      this.menuContext_Sep1.Text = rm.GetString("menuContext_Sep1.Text");
      //
      //menuContext_Cut
      //
      this.menuContext_Cut.Index = 2;
      this.menuContext_Cut.Text = rm.GetString("menuContext_Cut.Text");
      //
      //menuContext_Copy
      //
      this.menuContext_Copy.Index = 3;
      this.menuContext_Copy.Text = rm.GetString("menuContext_Copy.Text");
      //
      //menuContext_Paste
      //
      this.menuContext_Paste.Index = 4;
      this.menuContext_Paste.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.menuContext_PasteAll, this.menuContext_PasteData, this.menuContext_PasteFormatting, this.menuContext_PasteFormulas, this.menuContext_PasteAsLink, this.menuContext_PasteAsString, this.menuContext_PasteShape, this.menuContext_PasteChart });
      this.menuContext_Paste.Text = rm.GetString("menuContext_Paste.Text");
      //
      //menuContext_PasteAll
      //
      this.menuContext_PasteAll.Index = 0;
      this.menuContext_PasteAll.Text = rm.GetString("menuContext_PasteAll.Text");
      //
      //menuContext_PasteData
      //
      this.menuContext_PasteData.Index = 1;
      this.menuContext_PasteData.Text = rm.GetString("menuContext_PasteData.Text");
      //
      //menuContext_PasteFormatting
      //
      this.menuContext_PasteFormatting.Index = 2;
      this.menuContext_PasteFormatting.Text = rm.GetString("menuContext_PasteFormatting.Text");
      //
      //menuContext_PasteFormulas
      //
      this.menuContext_PasteFormulas.Index = 3;
      this.menuContext_PasteFormulas.Text = rm.GetString("menuContext_PasteFormulas.Text");
      //
      //menuContext_PasteAsLink
      //
      this.menuContext_PasteAsLink.Index = 4;
      this.menuContext_PasteAsLink.Text = rm.GetString("menuContext_PasteAsLink.Text");
      //
      //menuContext_PasteAsString
      //
      this.menuContext_PasteAsString.Index = 5;
      this.menuContext_PasteAsString.Text = rm.GetString("menuContext_PasteAsString.Text");
      //
      //menuContext_PasteShape
      //
      this.menuContext_PasteShape.Index = 6;
      this.menuContext_PasteShape.Text = rm.GetString("menuContext_PasteShape.Text");
      //
      //menuContext_PasteChart
      //
      this.menuContext_PasteChart.Index = 7;
      this.menuContext_PasteChart.Text = rm.GetString("menuContext_PasteChart.Text");
      //
      //menuContext_Sep2
      //
      this.menuContext_Sep2.Index = 5;
      this.menuContext_Sep2.Text = rm.GetString("menuContext_Sep2.Text");
      //
      //menuContext_Insert
      //
      this.menuContext_Insert.Index = 6;
      this.menuContext_Insert.Text = rm.GetString("menuContext_Insert.Text");
      //
      //menuContext_Delete
      //
      this.menuContext_Delete.Index = 7;
      this.menuContext_Delete.Text = rm.GetString("menuContext_Delete.Text");
      //
      //menuContext_DeleteSheet
      //
      this.menuContext_DeleteSheet.Index = 3;
      this.menuContext_DeleteSheet.Text = rm.GetString("menuContext_DeleteSheet.Text");
      //
      //menuContext_Clear
      //
      this.menuContext_Clear.Index = 9;
      this.menuContext_Clear.Text = rm.GetString("menuContext_Clear.Text");
      //
      //menuContext_SelectAll
      //
      this.menuContext_SelectAll.Index = 10;
      this.menuContext_SelectAll.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.menuContext_SelectAllSheet, this.menuContext_SelectAllCells, this.menuContext_SelectAllData });
      this.menuContext_SelectAll.Text = rm.GetString("menuContext_SelectAll.Text");
      //
      //menuContext_SelectAllSheet
      //
      this.menuContext_SelectAllSheet.Index = 0;
      this.menuContext_SelectAllSheet.Text = rm.GetString("menuContext_SelectAllSheet.Text");
      //
      //menuContext_SelectAllCells
      //
      this.menuContext_SelectAllCells.Index = 1;
      this.menuContext_SelectAllCells.Text = rm.GetString("menuContext_SelectAllCells.Text");
      //
      //menuContext_SelectAllData
      //
      this.menuContext_SelectAllData.Index = 2;
      this.menuContext_SelectAllData.Text = rm.GetString("menuContext_SelectAllData.Text");
      //
      //menuContext_Sep3
      //
      this.menuContext_Sep3.Index = 11;
      this.menuContext_Sep3.Text = rm.GetString("menuContext_Sep3.Text");
      //
      //menuContext_ColumnWidth
      //
      this.menuContext_ColumnWidth.Index = 12;
      this.menuContext_ColumnWidth.Text = rm.GetString("menuContext_ColumnWidth.Text");
      //
      //menuContext_RowHeight
      //
      this.menuContext_RowHeight.Index = 13;
      this.menuContext_RowHeight.Text = rm.GetString("menuContext_RowHeight.Text");
      //
      //menuContext_AutoFit
      //
      this.menuContext_AutoFit.Index = 14;
      this.menuContext_AutoFit.Text = rm.GetString("menuContext_AutoFit.Text");
      //
      //menuContext_Hide
      //
      this.menuContext_Hide.Index = 15;
      this.menuContext_Hide.Text = rm.GetString("menuContext_Hide.Text");
      //
      //menuContext_UnHide
      //
      this.menuContext_UnHide.Index = 16;
      this.menuContext_UnHide.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.menuContext_UnHideSelection, this.menuContext_UnHideAllHidden, this.menuContext_UnHideSpecific });
      this.menuContext_UnHide.Text = rm.GetString("menuContext_UnHide.Text");
      //
      //menuContext_UnHideSelection
      //
      this.menuContext_UnHideSelection.Index = 0;
      this.menuContext_UnHideSelection.Text = rm.GetString("menuContext_UnHideSelection.Text");
      //
      //menuContext_UnHideAllHidden
      //
      this.menuContext_UnHideAllHidden.Index = 1;
      this.menuContext_UnHideAllHidden.Text = rm.GetString("menuContext_UnHideAllHidden.Text");
      //
      //menuContext_UnHideSpecific
      //
      this.menuContext_UnHideSpecific.Index = 2;
      this.menuContext_UnHideSpecific.Text = rm.GetString("menuContext_UnHideSpecific.Text");

      //
      //menuContext_Sparkline
      //
      this.menuContext_Sparkline.Index = 17;
      this.menuContext_Sparkline.MenuItems.AddRange(new MenuItem[] { this.menuContext_SparklineEditGroup, this.menuContext_SparklineEditSingle, new MenuItem("-"), this.menuContext_SparklineGroup, this.menuContext_SparklineUngroup, new MenuItem("-"), this.menuContext_SparklineClear, this.menuContext_SparklineClearGroup, new MenuItem("-"), this.menuContext_SparklineSwitch });
      this.menuContext_Sparkline.Text = rm.GetString("menuContext_Sparkline.Text");
      this.menuContext_Sparkline.Visible = false;
      //
      //menuContext_SparklineEditGroup
      //
      this.menuContext_SparklineEditGroup.Index = 0;
      this.menuContext_SparklineEditGroup.Text = rm.GetString("menuContext_SparklineEditGroup.Text");
      this.menuContext_SparklineEditGroup.Name = "SparklineEditDataGroup";
      //
      //menuContext_SparklineEditSingle
      //
      this.menuContext_SparklineEditSingle.Index = 1;
      this.menuContext_SparklineEditSingle.Text = rm.GetString("menuContext_SparklineEditSingle.Text");
      this.menuContext_SparklineEditSingle.Name = "SparklineEditDataSingle";
      //
      //menuContext_SparklineGroup
      //
      this.menuContext_SparklineGroup.Index = 3;
      this.menuContext_SparklineGroup.Text = rm.GetString("menuContext_SparklineGroup.Text");
      this.menuContext_SparklineGroup.Name = "SparklineGroup";
      //
      //menuContext_SparklineUngroup
      //
      this.menuContext_SparklineUngroup.Index = 4;
      this.menuContext_SparklineUngroup.Text = rm.GetString("menuContext_SparklineUngroup.Text");
      this.menuContext_SparklineUngroup.Name = "SparklineUngroup";
      //
      //menuContext_SparklineClear
      //
      this.menuContext_SparklineClear.Index = 6;
      this.menuContext_SparklineClear.Text = rm.GetString("menuContext_SparklineClear.Text");
      this.menuContext_SparklineClear.Name = "ClearSparklines";
      //
      //menuContext_SparklineClearGroup
      //
      this.menuContext_SparklineClearGroup.Index = 7;
      this.menuContext_SparklineClearGroup.Text = rm.GetString("menuContext_SparklineClearGroup.Text");
      this.menuContext_SparklineClearGroup.Name = "SparklineClearGroup";
      //
      //menuContext_SparklineSwitch
      //
      this.menuContext_SparklineSwitch.Index = 9;
      this.menuContext_SparklineSwitch.Text = rm.GetString("menuContext_SparklineSwitch.Text");
      this.menuContext_SparklineSwitch.Name = "SparklineEditDataSwitch";
      //
      //menuContext_Span
      //
      this.menuContext_Span.Index = 17;
      this.menuContext_Span.Text = rm.GetString("menuContext_Span.Text");
      //
      //menuContext_Lock
      //
      this.menuContext_Lock.Index = 18;
      this.menuContext_Lock.Text = rm.GetString("menuContext_Lock.Text");
      //
      //menuContext_Borders
      //
      this.menuContext_Borders.Index = 19;
      this.menuContext_Borders.Text = rm.GetString("menuContext_Borders.Text");
      //
      //menuContext_Headers
      //
      this.menuContext_Headers.Index = 20;
      this.menuContext_Headers.Text = rm.GetString("menuContext_Headers.Text");
      //
      //menuContext_SheetSettings
      //
      this.menuContext_SheetSettings.Index = 21;
      this.menuContext_SheetSettings.Text = rm.GetString("menuContext_SheetSettings");
      //
      //menuContext_SheetPrintOptions
      //
      this.menuContext_SheetPrintOptions.Index = 22;
      this.menuContext_SheetPrintOptions.Text = rm.GetString("menuContext_SheetPrintOptions");
      //
      //menuContext_SheetSkinDesigner
      //
      this.menuContext_SheetSkinDesigner.Index = 23;
      this.menuContext_SheetSkinDesigner.Text = rm.GetString("menuContext_SheetSkinDesigner");
      //
      //menuContext_SpreadSkinDesigner
      //
      this.menuContext_SpreadSkinDesigner.Index = 24;
      this.menuContext_SpreadSkinDesigner.Text = rm.GetString("menuContext_SpreadSkinDesigner");

      //menuContext_Sep4
      //
      this.menuContext_Sep4.Index = 25;
      this.menuContext_Sep4.Text = rm.GetString("menuContext_Sep4.Text");
      //
      //menuContext_EditNote
      //
      this.menuContext_EditNote.Index = 26;
      this.menuContext_EditNote.Text = rm.GetString("menuContext_EditNote.Text");
      //
      //menuContext_DeleteNote
      //
      this.menuContext_DeleteNote.Index = 27;
      this.menuContext_DeleteNote.Text = rm.GetString("menuContext_DeleteNote.Text");

      this.menuContext_ListBox.Text = rm.GetString("menuContext_ListBox");
      this.menuContext_Celltypes.MenuItems.Add(9, menuContext_ListBox);

      this.menuContext_Barcode.Text = rm.GetString("menuContext_Barcode");
      this.menuContext_Celltypes.MenuItems.Add(0, menuContext_Barcode);

      this.menuContext_MultiColumnComboBox.Text = rm.GetString("menuContext_MultiColumnComboBox");
      this.menuContext_Celltypes.MenuItems.Add(11, menuContext_MultiColumnComboBox);

      this.menuContext_ColorPicker.Text = rm.GetString("menuContext_ColorPicker");
      this.menuContext_Celltypes.MenuItems.Add(2, menuContext_ColorPicker);


      this.shapeContextDelete.Text = rm.GetString("shapeContextDelete.Text");
      this.shapeContextProperties.Text = rm.GetString("shapeContextProperties.Text");
      this.shapeContextFormula.Text = rm.GetString("shapeContextFormula.Text");
      this.shapeContextSep1.Text = rm.GetString("shapeContextSep1.Text");
      this.shapeContextLocked.Text = rm.GetString("shapeContextLocked.Text");
      this.shapeContextSep2.Text = rm.GetString("shapeContextSep2.Text");
      this.shapeContextCut.Text = rm.GetString("shapeContextCut.Text");
      this.shapeContextCopy.Text = rm.GetString("shapeContextCopy.Text");
      this.shapeContextPaste.Text = rm.GetString("shapeContextPaste.Text");

      this.menuContext_Button.Click += new EventHandler(menuContext_Button_Click);
      this.menuContext_Checkbox.Click += new EventHandler(menuContext_Checkbox_Click);
      this.menuContext_Combobox.Click += new EventHandler(menuContext_Combobox_Click);
      this.menuContext_Currency.Click += new EventHandler(menuContext_Currency_Click);
      this.menuContext_DateTime.Click += new EventHandler(menuContext_DateTime_Click);
      this.menuContext_Text.Click += new EventHandler(menuContext_Text_Click);
      this.menuContext_Hyperlink.Click += new EventHandler(menuContext_Hyperlink_Click);
      this.menuContext_Label.Click += new EventHandler(menuContext_Label_Click);
      this.menuContext_Mask.Click += new EventHandler(menuContext_Mask_Click);
      this.menuContext_Number.Click += new EventHandler(menuContext_Number_Click);
      this.menuContext_Percent.Click += new EventHandler(menuContext_Percent_Click);
      this.menuContext_MultiOption.Click += new EventHandler(menuContext_MultiOption_Click);
      this.menuContext_Progress.Click += new EventHandler(menuContext_Progress_Click);
      this.menuContext_Slider.Click += new EventHandler(menuContext_Slider_Click);
      this.menuContext_Image.Click += new EventHandler(menuContext_Image_Click);
      this.menuContext_General.Click += new EventHandler(menuContext_General_Click);
      this.menuContext_GcTextBox.Click += new EventHandler(menuContext_GcTextBox_Click);
      this.menuContext_GcDate.Click += new EventHandler(menuContext_GcDate_Click);

      this.menuContext_Cut.Click += new EventHandler(menuContext_Cut_Click);
      this.menuContext_Copy.Click += new EventHandler(menuContext_Copy_Click);
      this.menuContext_Paste.Click += new EventHandler(menuContext_Paste_Click);
      this.menuContext_PasteAll.Click += new EventHandler(menuContext_PasteAll_Click);
      this.menuContext_PasteData.Click += new EventHandler(menuContext_PasteData_Click);
      this.menuContext_PasteFormatting.Click += new EventHandler(menuContext_PasteFormatting_Click);
      this.menuContext_PasteFormulas.Click += new EventHandler(menuContext_PasteFormulas_Click);
      this.menuContext_PasteAsLink.Click += new EventHandler(menuContext_PasteAsLink_Click);
      this.menuContext_PasteAsString.Click += new EventHandler(menuContext_PasteAsString_Click);
      this.menuContext_Clear.Click += new EventHandler(menuContext_Clear_Click);
      this.menuContext_SelectAllSheet.Click += new EventHandler(menuContext_SelectAllSheet_Click);
      this.menuContext_SelectAllCells.Click += new EventHandler(menuContext_SelectAllCells_Click);
      this.menuContext_SelectAllData.Click += new EventHandler(menuContext_SelectAllData_Click);

      this.menuContext_SparklineEditGroup.Click += new EventHandler(menuContext_SparklineEditGroup_Click);
      this.menuContext_SparklineEditSingle.Click += new EventHandler(menuContext_SparklineEditSingle_Click);
      this.menuContext_SparklineGroup.Click += new EventHandler(menuContext_SparklineGroup_Click);
      this.menuContext_SparklineUngroup.Click += new EventHandler(menuContext_SparklineUngroup_Click);
      this.menuContext_SparklineClear.Click += new EventHandler(menuContext_SparklineClear_Click);
      this.menuContext_SparklineClearGroup.Click += new EventHandler(menuContext_SparklineClearGroup_Click);
      this.menuContext_SparklineSwitch.Click += new EventHandler(menuContext_SparklineSwitch_Click);

      this.menuContext_Span.Click += new EventHandler(menuContext_Span_Click);
      this.menuContext_Lock.Click += new EventHandler(menuContext_Lock_Click);
      this.menuContext_Borders.Click += new EventHandler(menuContext_Borders_Click);

      this.menuContext_Insert.Click += new EventHandler(menuContext_Insert_Click);
      this.menuContext_Delete.Click += new EventHandler(menuContext_Delete_Click);
      this.menuContext_Headers.Click += new EventHandler(menuContext_Headers_Click);
      this.menuContext_ColumnWidth.Click += new EventHandler(menuContext_ColumnWidth_Click);
      this.menuContext_RowHeight.Click += new EventHandler(menuContext_RowHeight_Click);
      this.menuContext_Hide.Click += new EventHandler(menuContext_Hide_Click);
      this.menuContext_UnHideSelection.Click += new EventHandler(menuContext_UnHideSelection_Click);
      this.menuContext_UnHideAllHidden.Click += new EventHandler(menuContext_UnHideAllHidden_Click);
      this.menuContext_UnHideSpecific.Click += new EventHandler(menuContext_UnHideSpecific_Click);
      this.menuContext_ClearCellType.Click += new EventHandler(menuContext_ClearCellType_Click);
      this.menuContext_SheetSettings.Click += new EventHandler(menuContext_SheetSettings_Click);
      this.menuContext_SheetSkinDesigner.Click += new EventHandler(menuContext_SheetSkinDesigner_Click);
      this.menuContext_SpreadSkinDesigner.Click += new EventHandler(menuContext_SpreadSkinDesigner_Click);
      this.menuContext_SheetPrintOptions.Click += new EventHandler(menuContext_SheetPrintOptions_Click);

      this.menuContext_AutoFit.Click += new EventHandler(menuContext_AutoFit_Click);

      this.shapeContextDelete.Click += new EventHandler(shapeContextDelete_Click);
      this.shapeContextProperties.Click += new EventHandler(shapeContextProperties_Click);
      this.shapeContextFormula.Click += new EventHandler(shapeContextFormula_Click);
      this.shapeContextLocked.Click += new EventHandler(shapeContextLocked_Click);
      this.shapeContextCut.Click += new EventHandler(shapeContextCut_Click);
      this.shapeContextCopy.Click += new EventHandler(shapeContextCopy_Click);
      this.shapeContextPaste.Click += new EventHandler(shapeContextPaste_Click);
      this.shapeContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.shapeContextProperties, this.shapeContextFormula, this.shapeContextSep1, this.shapeContextCut, this.shapeContextCopy, this.shapeContextPaste, this.shapeContextSep2, this.shapeContextLocked, this.shapeContextDelete });
      this.shapeContextMenu.Popup += new EventHandler(shapeContextMenu_Popup);
    
      this.menuContext_RichText.Click += new EventHandler(menuContext_RichText_Click);
      this.menuContext_RegEx.Click += new EventHandler(menuContext_RegEx_Click);
      this.menuContext_PasteShape.Click += new EventHandler(menuContext_PasteShape_Click);
      this.menuContext_PasteChart.Click += new EventHandler(menuContext_PasteChart_Click);
    

      this.menuContext_EditNote.Click += new EventHandler(menuContext_EditNote_Click);
      this.menuContext_DeleteNote.Click += new EventHandler(menuContext_DeleteNote_Click);
      this.menuContext_DeleteSheet.Click += new EventHandler(menuContext_DeleteSheet_Click);
      this.menuContext_ListBox.Click += new EventHandler(menuContext_ListBox_Click);
      this.menuContext_MultiColumnComboBox.Click += new EventHandler(menuContext_MultiColumnComboBox_Click);
      this.menuContext_Barcode.Click += new EventHandler(menuContext_Barcode_Click);
      this.menuContext_ColorPicker.Click += new EventHandler(menuContext_ColorPicker_Click);
      this.contextMenu1.Popup += new EventHandler(contextMenu1_Popup);

    }

    void shapeContextMenu_Popup(object sender, EventArgs e)
    {
      if (psObjMenu == null)
      {
        return;
      }

      if (psObjMenu is SpreadCameraShape)
      {
        this.shapeContextFormula.Index = 2;
        this.shapeContextFormula.Visible = true;
      }
      else
      {
        this.shapeContextFormula.Visible = false;
      }

      shapeContextLocked.Checked = psObjMenu.Locked;

      IDataObject data = Clipboard.GetDataObject();

      shapeContextPaste.Enabled = false;

      if (data.GetDataPresent("FarPoint.Win.Spread.ShapeClipboardObject"))
      {
        shapeContextPaste.Enabled = true;
      }

    }

    void shapeContextPaste_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      fpSpread1.ActiveSheet.ClipboardPasteShape();
    }

    void shapeContextCopy_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      fpSpread1.ActiveSheet.ClipboardCopyShape();
    }

    void shapeContextCut_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      fpSpread1.ActiveSheet.ClipboardCutShape();
    }

    
    void shapeContextLocked_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      if (psObjMenu.Locked == true)
      {
        psObjMenu.Locked = false;
      }
      else
      {
        psObjMenu.Locked = true;
      }
    }


    private PSObject cachedActiveShape;
    void shapeContextFormula_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      cachedActiveShape = psObjMenu;
      Form selectDataDialog = SpreadCameraShape.ShowSelectDataForm(this, fpSpread1, psObjMenu as SpreadCameraShape);
      selectDataDialog.FormClosed += new FormClosedEventHandler(CameraShapeCreateDialong_Closed);
    }

    void CameraShapeCreateDialong_Closed(object sender, FormClosedEventArgs e)
    {
      if (cachedActiveShape != null)
      {
        cachedActiveShape.Focus(true);
      }
      (sender as Form).FormClosed -= CameraShapeCreateDialong_Closed;
    }

    void shapeContextProperties_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      ShapeProps props = new ShapeProps(fpSpread1);
      props.Shape = psObjMenu as PSShape;
      props.ShowDialog(this);
    }

  
    void shapeContextDelete_Click(object sender, EventArgs e)
    {
      if (psObjMenu == null) return;

      if (MessageBox.Show(rm.GetString("DeleteShapePrompt"), rm.GetString("DesignerErrorTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
      {

        psObjMenu.Parent = null;
        fpSpread1.ActiveSheet.DrawingContainer.ContainedObjects.Remove(psObjMenu);

        fpSpread1.ActiveWindowlessObject = null;

        fpSpread1.Invalidate();
      }

    }

    /// <summary>
    /// Get current spread select cellranges.
    /// </summary>
    /// <returns>Current selections.</returns>
    private CellRange[] GetSpreadSelections()
    {
      SheetView sheet = this.fpSpread1.ActiveSheet;
      CellRange[] cellRanges = sheet.GetSelections();
      if (cellRanges.Length == 0)
        return new CellRange[] { new CellRange(sheet.ActiveRowIndex, sheet.ActiveColumnIndex, 1, 1) };
      else
        for (int i = 0; i < cellRanges.Length; i++)
        {
          if (cellRanges[i].Row == -1 && cellRanges[i].Column == -1)
          {
            cellRanges[i] = new CellRange(0, 0, sheet.RowCount, sheet.ColumnCount);
          }
          else if (cellRanges[i].Row == -1)
          {
            cellRanges[i] = new CellRange(0, cellRanges[i].Column, sheet.RowCount, cellRanges[i].ColumnCount);
          }
          else if (cellRanges[i].Column == -1)
          {
            cellRanges[i] = new CellRange(cellRanges[i].Row, 0, cellRanges[i].RowCount, sheet.ColumnCount);
          }
        }
      return cellRanges;
    }

    private CellRange GetSpreadSelection()
    {
      return GetSpreadSelections()[0];
    }

    private void InitializeLocalUI()
    {
      if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == "en-US")
        return;
      if (FarPoint.Win.Spread.Design.common.rm != null)
      {
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      else
      {
        FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
        rm = FarPoint.Win.Spread.Design.common.rm;
      }

      if (System.Threading.Thread.CurrentThread.CurrentUICulture.Name == "zh-CN")
      {
        this.Text = "Spread 表格设计器";
      }
      else
      {
        this.Text = rm.GetString("DesignerMain2008");
      }
      this.rbPaste.Text = rm.GetString("rbPaste.Text");
      this.rbPasteAll.Text = rm.GetString("rbPasteAll.Text");
      this.rbPasteValues.Text = rm.GetString("rbPasteValues.Text");
      this.rbPasteFormatting.Text = rm.GetString("rbPasteFormatting.Text");
      this.rbPasteFormulas.Text = rm.GetString("rbPasteFormulas.Text");
      this.rbPasteAsLink.Text = rm.GetString("rbPasteAsLink.Text");
      this.rbPasteAsString.Text = rm.GetString("rbPasteAsString.Text");
      this.rbPasteAsShape.Text = rm.GetString("rbPasteAsShape.Text");
      this.rbPasteAsChart.Text = rm.GetString("rbPasteAsChart.Text");
      this.rbCut.Text = rm.GetString("rbCut.Text");
      this.rbCopy.Text = rm.GetString("rbCopy.Text");
      this.rbBottomBorder.Text = rm.GetString("rbBottomBorder.Text");
      this.rbTopBorder.Text = rm.GetString("rbTopBorder.Text");
      this.rbLeftBorder.Text = rm.GetString("rbLeftBorder.Text");
      this.rbRightBorder.Text = rm.GetString("rbRightBorder.Text");
      this.rbNoBorder.Text = rm.GetString("rbNoBorder.Text");
      this.rbAllBorder.Text = rm.GetString("rbAllBorder.Text");
      this.rbOutsideBorder.Text = rm.GetString("rbOutsideBorder.Text");
      this.rbThickBoxBorder.Text = rm.GetString("rbThickBoxBorder.Text");
      this.rbBottomDoubleBorder.Text = rm.GetString("rbBottomDoubleBorder.Text");
      this.rbThickBottomBorder.Text = rm.GetString("rbThickBottomBorder.Text");
      this.rbTopAndBottomBorder.Text = rm.GetString("rbTopAndBottomBorder.Text");
      this.rbTopAndThickBottomBorder.Text = rm.GetString("rbTopAndThickBottomBorder.Text");
      this.rbTopAndDoubleBottomBorder.Text = rm.GetString("rbTopAndDoubleBottomBorder.Text");
      this.rbMoreBorder.Text = rm.GetString("rbMoreBorder.Text");
      this.rbWrapText.Text = rm.GetString("rbWrapText.Text");
      this.rbMerge.Text = rm.GetString("rbMerge.Text");
      this.rbdwNumber.Text = rm.GetString("rbdwNumber.Text");
      this.rbdwPercent.Text = rm.GetString("rbdwPercent.Text");
      this.rbdwCurrency.Text = rm.GetString("rbdwCurrency.Text");
      this.rbdwRegularExpression.Text = rm.GetString("rbdwRegularExpression.Text");
      this.rbdwBarCode.Text = rm.GetString("rbdwBarCode.Text");
      this.rbdwButton.Text = rm.GetString("rbdwButton.Text");
      this.rbdwCheckBox.Text = rm.GetString("rbdwCheckBox.Text");
      this.rbdwMultiOption.Text = rm.GetString("rbdwMultiOption.Text");
      this.rbdwComboBox.Text = rm.GetString("rbdwComboBox.Text");
      this.rbdwMultiColumnComboBox.Text = rm.GetString("rbdwMultiColumnComboBox.Text");
      this.rbdwListBox.Text = rm.GetString("rbdwListBox.Text");
      this.rbdwRichText.Text = rm.GetString("rbdwRichText.Text");
      this.rbdwDateTime.Text = rm.GetString("rbdwDateTime.Text");

      this.rbdwGeneral.Text = rm.GetString("rbdwGeneral.Text");
      this.rbdwText.Text = rm.GetString("rbdwText.Text");
      this.rbdwPicture.Text = rm.GetString("rbdwPicture.Text");
      this.rbdwColorPicker.Text = rm.GetString("rbdwColorPicker.Text");
      this.rbdwProgress.Text = rm.GetString("rbdwProgress.Text");
      this.rbdwSlider.Text = rm.GetString("rbdwSlider.Text");
      this.rbdwMask.Text = rm.GetString("rbdwMask.Text");
      this.rbdwHyperLink.Text = rm.GetString("rbdwHyperLink.Text");
      this.rbdwGcDate.Text = rm.GetString("rbdwGcDate.Text");
      this.rbdwGcTextBox.Text = rm.GetString("rbdwGcTextBox.Text");
      this.rbClearCellType.Text = rm.GetString("rbClearCellType.Text");
      this.rbConditionalFormat.Text = rm.GetString("rbConditionalFormat.Text");
      this.rbCFHighLightCellsRules.Text = rm.GetString("rbCFHighLightCellsRules.Text");
      this.rbGreaterThan.Text = rm.GetString("rbGreaterThan.Text");
      this.rbLessThan.Text = rm.GetString("rbLessThan.Text");
      this.rbBetween.Text = rm.GetString("rbBetween.Text");
      this.rbCFEqualTo.Text = rm.GetString("rbCFEqualTo.Text");
      this.rbCFTextThatContains.Text = rm.GetString("rbCFTextThatContains.Text");
      this.rbCFADateOccurring.Text = rm.GetString("rbCFADateOccurring.Text");
      this.rbCFDuplicateValues.Text = rm.GetString("rbCFDuplicateValues.Text");
      this.rbCFHighLightMoreRules.Text = rm.GetString("rbCFHighLightMoreRules.Text");
      this.rbCFTopBottomRules.Text = rm.GetString("rbCFTopBottomRules.Text");
      this.rbCFTop10Items.Text = rm.GetString("rbCFTop10Items.Text");
      this.rbCFTop10Per.Text = rm.GetString("rbCFTop10Per.Text");
      this.rbCFBottom10Items.Text = rm.GetString("rbCFBottom10Items.Text");
      this.rbCFBottom10Per.Text = rm.GetString("rbCFBottom10Per.Text");
      this.rbCFAboveAverage.Text = rm.GetString("rbCFAboveAverage.Text");
      this.rbCFBelowAverage.Text = rm.GetString("rbCFBelowAverage.Text");
      this.rbCFTopBottomMoreRules.Text = rm.GetString("rbCFTopBottomMoreRules.Text");
      this.rbCFDataBars.Text = rm.GetString("rbCFDataBars.Text");
      this.rbCFDataBarMoreRules.Text = rm.GetString("rbCFDataBarMoreRules.Text");
      this.rbCFColorScales.Text = rm.GetString("rbCFColorScales.Text");
      this.rbCFColorScaleMoreRule.Text = rm.GetString("rbCFColorScaleMoreRule.Text");
      this.rbCFIconSet.Text = rm.GetString("rbCFIconSet.Text");
      this.rbCFIconSetsMoreRule.Text = rm.GetString("rbCFIconSetsMoreRule.Text");
      this.rbCFNewRule.Text = rm.GetString("rbCFNewRule.Text");
      this.rbCFClearRules.Text = rm.GetString("rbCFClearRules.Text");
      this.rbCFClearFromSelectedCell.Text = rm.GetString("rbCFClearFromSelectedCell.Text");
      this.rbCFClearEntireSheet.Text = rm.GetString("rbCFClearEntireSheet.Text");
      this.rbCFManagerRules.Text = rm.GetString("rbCFManagerRules.Text");
      this.rbLock.Text = rm.GetString("rbLock.Text");
      this.rbClearAll.Text = rm.GetString("rbClearAll.Text");
      this.rbSelectAll.Text = rm.GetString("rbSelectAll.Text");
      this.rbSortAToZ.Text = rm.GetString("rbSortAToZ.Text");
      this.rbSortZToA.Text = rm.GetString("rbSortZToA.Text");
      this.rbCustomSort.Text = rm.GetString("rbCustomSort.Text");
      this.rbFind2.Text = rm.GetString("rbFind2.Text");
      this.rbFind.Text = rm.GetString("rbFind.Text");
      this.rbGoto.Text = rm.GetString("rbGoto.Text");
      this.rbSymbol.Text = rm.GetString("rbSymbol.Text");
      this.rbWordArt.Text = rm.GetString("rbWordArt.Text");
      this.rbPicture.Text = rm.GetString("rbPicture.Text");
      this.rbShapes.Text = rm.GetString("rbShapes.Text");
      this.rbAnnotactionMode.Text = rm.GetString("rbAnnotactionMode.Text");
      this.rbDeleteShape.Text = rm.GetString("rbDeleteShape.Text");
      this.rbDeleteActiveShape.Text = rm.GetString("rbDeleteActiveShape.Text");
      this.rbDeleteAllShape.Text = rm.GetString("rbDeleteAllShape.Text");
      this.rbSparklineLine.Text = rm.GetString("rbSparklineLine.Text");
      this.rbSparklineColumn.Text = rm.GetString("rbSparklineColumn.Text");
      this.rbSparklineWinLoss.Text = rm.GetString("rbSparklineWinLoss.Text");
      this.rbCameraShape.Text = rm.GetString("rbCameraShape.Text");
      this.rbMargins.Text = rm.GetString("rbMargins.Text");
      this.rbOrientation.Text = rm.GetString("rbOrientation.Text");
      this.rbOrientationNormal.Text = rm.GetString("rbOrientationNormal.Text");
      this.rbOrientationPortraint.Text = rm.GetString("rbOrientationPortraint.Text");
      this.rbOrientationLandscape.Text = rm.GetString("rbOrientationLandscape.Text");
      this.rbPrintAreaSet.Text = rm.GetString("rbPrintAreaSet.Text");
      this.rbMarginsTemp.Text = rm.GetString("prt_TabMargins");
      this.rbPrintAreaClear.Text = rm.GetString("rbPrintAreaClear.Text");
      this.rbBackGround.Text = rm.GetString("rbBackGround.Text");
      this.rbPrintTitles.Text = rm.GetString("rbPrintTitles.Text");
      this.rbSmartPrint.Text = rm.GetString("rbSmartPrint.Text");
      this.rbGroup.Text = rm.GetString("rbGroup.Text");
      this.rbUnGroup.Text = rm.GetString("rbUnGroup.Text");
      this.rbExpandGroup.Text = rm.GetString("rbExpandGroup.Text");
      this.rbCollapseGroup.Text = rm.GetString("rbCollapseGroup.Text");
      this.rbSort.Text = rm.GetString("rbSort.Text");
      this.rbAddCustomname.Text = rm.GetString("rbAddCustomname.Text");
      this.rbRowHeader.Text = rm.GetString("rbRowHeader.Text");
      this.rbColumnHeader.Text = rm.GetString("rbColumnHeader.Text");
      this.rbVerticalGridLine.Text = rm.GetString("rbVerticalGridLine.Text");
      this.rbHorizontalGridLine.Text = rm.GetString("rbHorizontalGridLine.Text");
      this.rbFormulaBar.Text = rm.GetString("rbFormulaBar.Text");
      this.rbZoom.Text = rm.GetString("rbZoom.Text");
      this.rb100Percent.Text = rm.GetString("rb100Percent.Text");
      this.rbZoomToSelection.Text = rm.GetString("rbZoomToSelection.Text");
      this.rbFreezePanes.Text = rm.GetString("rbFreezePanes.Text");
      this.rbFreezeRowsAndColumns.Text = rm.GetString("rbFreezeRowsAndColumns.Text");
      this.rbFreezeTopRow.Text = rm.GetString("rbFreezeTopRow.Text");
      this.rbFreezeFirstColumn.Text = rm.GetString("rbFreezeFirstColumn.Text");
      this.rbFreezeTrailingRowsAndColumns.Text = rm.GetString("rbFreezeTrailingRowsAndColumns.Text");
      this.rbSpreadSettingGeneral.Text = rm.GetString("rbSpreadSettingGeneral.Text");
      this.rbSpreadSettingEdit.Text = rm.GetString("rbSpreadSettingEdit.Text");
      this.rbSpreadSettingScrollBar.Text = rm.GetString("rbSpreadSettingScrollBar.Text");
      this.rbSpreadSettingSplitBox.Text = rm.GetString("rbSpreadSettingSplitBox.Text");
      this.rbSpreadSettingView.Text = rm.GetString("rbSpreadSettingView.Text");
      this.rbSpreadSettingTitles.Text = rm.GetString("rbSpreadSettingTitles.Text");
      this.rbSheetSettingGeneral.Text = rm.GetString("rbSheetSettingGeneral.Text");
      this.rbSheetSettingColor.Text = rm.GetString("rbSheetSettingColor.Text");
      this.rbSheetSettingHeaders.Text = rm.GetString("rbSheetSettingHeaders.Text");
      this.rbSheetSettingGridLines.Text = rm.GetString("rbSheetSettingGridLines.Text");
      this.rbSheetSettingCalculation.Text = rm.GetString("rbSheetSettingCalculation.Text");
      this.rbSheetSettingFonts.Text = rm.GetString("rbSheetSettingFonts.Text");
      this.rbSpreadSkin.Text = rm.GetString("rbSpreadSkin.Text");
      this.rbSheetSkin.Text = rm.GetString("rbSheetSkin.Text");
      this.rbFocusIndicator.Text = rm.GetString("rbFocusIndicator.Text");
      this.rbStyle.Text = rm.GetString("rbStyle.Text");
      this.rbTabStrip.Text = rm.GetString("rbTabStrip.Text");
      this.rbAlternatingRow.Text = rm.GetString("rbAlternatingRow.Text");
      this.rbGroupInfo.Text = rm.GetString("rbGroupInfo.Text");
      this.rbCellColumnsandRows.Text = rm.GetString("rbCellColumnsandRows.Text");
      this.rbInputMap.Text = rm.GetString("rbInputMap.Text");
      this.rbPreferences.Text = rm.GetString("rbPreferences.Text");
      this.rbShowStart.Text = rm.GetString("rbShowStart.Text");
      this.rbAllowDrag.Text = rm.GetString("rbAllowDrag.Text");
      this.rbAutoLaunch.Text = rm.GetString("rbAutoLaunch.Text");
      this.rbAutomticSelectContextMenu.Text = rm.GetString("rbAutomticSelectContextMenu.Text");
      this.rbShowPropertyGrid.Text = rm.GetString("rbShowPropertyGrid.Text");
      this.rbShowAllConditionalFormat.Text = rm.GetString("rbShowAllConditionalFormat.Text");
      this.rbSetPicture.Text = rm.GetString("rbSetPicture.Text");
      this.rbClearPicture.Text = rm.GetString("rbClearPicture.Text");
      this.rbChartChangeChartType.Text = rm.GetString("rbChartChangeChartType.Text");
      this.rbChartSwitchRowColumn.Text = rm.GetString("rbChartSwitchRowColumn.Text");
      this.rbChartSelectData.Text = rm.GetString("rbChartSelectData.Text");
      this.rbChartSendToBack.Text = rm.GetString("rbChartSendToBack.Text");
      this.rbChartBringToFront.Text = rm.GetString("rbChartBringToFront.Text");
      this.rbMoveChart.Text = rm.GetString("rbMoveChart.Text");
      this.rbChartMove.Label = rm.GetString("rbChartMove.Text");
      this.rbChartSize.Label = rm.GetString("rbChartSize.Text");
      this.rbChartMoveWithCell.Text = rm.GetString("rbChartMoveWithCell.Text");
      this.rbChartSizeWithCell.Text = rm.GetString("rbChartSizeWithCell.Text");
      this.rbSparklineEditData.Text = rm.GetString("rbSparklineEditData.Text");
      this.rbSparklineEditDataGroup.Text = rm.GetString("rbSparklineEditDataGroup.Text");
      this.rbSparklineEditDataSingle.Text = rm.GetString("rbSparklineEditDataSingle.Text");
      this.rbSparklineEditDataHidden.Text = rm.GetString("rbSparklineEditDataHidden.Text");
      this.rbSparklineEditDataSwitch.Text = rm.GetString("rbSparklineEditDataSwitch.Text");
      this.rbSparklineTypeLine.Text = rm.GetString("rbSparklineTypeLine.Text");
      this.rbSparklineTypeColumn.Text = rm.GetString("rbSparklineTypeColumn.Text");
      this.rbSparklineTypeWinLoss.Text = rm.GetString("rbSparklineTypeWinLoss.Text");
      this.rbSparklineHighPoint.Text = rm.GetString("rbSparklineHighPoint.Text");
      this.rbSparklineLowPoint.Text = rm.GetString("rbSparklineLowPoint.Text");
      this.rbSparklineMarkerColorNegativePoint.Text = rm.GetString("rbSparklineMarkerColorNegativePoint.Text");
      this.rbSparklineFirstPoint.Text = rm.GetString("rbSparklineFirstPoint.Text");
      this.rbSparklineLastPoint.Text = rm.GetString("rbSparklineLastPoint.Text");
      this.rbSparklineMarkerColorMarker.Text = rm.GetString("rbSparklineMarkerColorMarker.Text");
      this.rbSparklineColor.Text = rm.GetString("rbSparklineColor.Text");
      this.rbSparklineWeight.Text = rm.GetString("rbSparklineWeight.Text");
      this.rbSparklineMarkerColor.Text = rm.GetString("rbSparklineMarkerColor.Text");
      this.rbSparklineAxis.Text = rm.GetString("rbSparklineAxis.Text");
      this.rbSparklineAxisSubCaption1.Text = rm.GetString("rbSparklineAxisSubCaption1.Text");
      this.rbSparklineAxisSub1.Text = rm.GetString("rbSparklineAxisSub1.Text");
      this.rbSparklineAxisSub2.Text = rm.GetString("rbSparklineAxisSub2.Text");
      this.rbSparklineAxisSub3.Text = rm.GetString("rbSparklineAxisSub3.Text");
      this.rbSparklineAxisSub4.Text = rm.GetString("rbSparklineAxisSub4.Text");
      this.rbSparklineAxisSubCaption2.Text = rm.GetString("rbSparklineAxisSubCaption2.Text");
      this.rbSparklineAxisSub5.Text = rm.GetString("rbSparklineAxisSub5.Text");
      this.rbSparklineAxisSub6.Text = rm.GetString("rbSparklineAxisSub6.Text");
      this.rbSparklineAxisSub7.Text = rm.GetString("rbSparklineAxisSub7.Text");
      this.rbSparklineAxisSubCaption3.Text = rm.GetString("rbSparklineAxisSubCaption3.Text");
      this.rbSparklineAxisSub8.Text = rm.GetString("rbSparklineAxisSub8.Text");
      this.rbSparklineAxisSub9.Text = rm.GetString("rbSparklineAxisSub9.Text");
      this.rbSparklineAxisSub10.Text = rm.GetString("rbSparklineAxisSub10.Text");
      this.rbSparklineGroup.Text = rm.GetString("rbSparklineGroup.Text");
      this.rbSparklineUngroup.Text = rm.GetString("rbSparklineUngroup.Text");
      this.rbSparklineClear.Text = rm.GetString("rbSparklineClear.Text");
      this.rbSparklineClearSingle.Text = rm.GetString("rbSparklineClearSingle.Text");
      this.rbSparklineClearGroup.Text = rm.GetString("rbSparklineClearGroup.Text");

      this.rtHome.Text = rm.GetString("rtHome.Text");
      this.rtInsert.Text = rm.GetString("rtInsert.Text");
      this.rtpageLayout.Text = rm.GetString("rtpageLayout.Text");
      this.rtData.Text = rm.GetString("rtData.Text");
      this.rtView.Text = rm.GetString("rtView.Text");
      this.rtSetting.Text = rm.GetString("rtSetting.Text");
      this.rtDrawingTools.Text = rm.GetString("rtDrawingTools.Text");
      this.rtChartTools.Text = rm.GetString("rtChartTools.Text");
      this.rtSparkline.Text = rm.GetString("rtSparkline.Text");

      this.rpClipboard.Text = rm.GetString("rpClipboard.Text");
      this.rpFont.Text = rm.GetString("rpFont.Text");
      this.rpAlignment.Text = rm.GetString("rpAlignment.Text");
      this.rpCellType.Text = rm.GetString("rpCellType.Text");
      this.rpStyle.Text = rm.GetString("rpStyle.Text");
      this.rpEditing.Text = rm.GetString("rpEditing.Text");

      this.rbSortFilter.Text = rm.GetString("rbSortFilter.Text");
      this.rbSelectAllSheet.Text = rm.GetString("rbSelectAllSheet.Text");
      this.rbSelectAllCells.Text = rm.GetString("rbSelectAllCells.Text");
      this.rbSelectAllData.Text = rm.GetString("rbSelectAllData.Text");
      this.rcbCellType.Text = rm.GetString("rcbCellType.Text");

      this.rpText.Text = rm.GetString("rpText.Text");
      this.rplllustrations.Text = rm.GetString("rplllustrations.Text");
      this.rpChart.Text = rm.GetString("rpChart.Text");
      this.rpDelete.Text = rm.GetString("rpDelete.Text");
      this.rpSparkline.Text = rm.GetString("rpSparkline.Text");
      this.rpCameraShape.Text = rm.GetString("rpCameraShape.Text");

      this.rpPageSetup.Text = rm.GetString("rpPageSetup.Text");
      this.rpOutline.Text = rm.GetString("rpOutline.Text");
      this.rpSort.Text = rm.GetString("rpSort.Text");
      this.rpCustomName.Text = rm.GetString("rpCustomName.Text");
      this.rbAddCustomname.Text = rm.GetString("rbAddCustomname.Text");

      this.rpShowHide.Text = rm.GetString("rpShowHide.Text");
      this.rpZoom.Text = rm.GetString("rpZoom.Text");
      this.rpWindow.Text = rm.GetString("rpWindow.Text");

      this.rpSpreadSettings.Text = rm.GetString("rpSpreadSettings.Text");
      this.rpSheetSettings.Text = rm.GetString("rpSheetSettings.Text");
      this.rpAppearanceSettings.Text = rm.GetString("rpAppearanceSettings.Text");
      this.rpOtherSettings.Text = rm.GetString("rpOtherSettings.Text");
      this.rpDesignerSettings.Text = rm.GetString("rpDesignerSettings.Text");

      this.RibbonPanel13.Text = rm.GetString("RibbonPanel13.Text");
      this.RibbonPanel14.Text = rm.GetString("RibbonPanel14.Text");

      this.rpChartType.Text = rm.GetString("rpChartType.Text");
      this.rpChartData.Text = rm.GetString("rpChartData.Text");
      this.rpChartArrange.Text = rm.GetString("rpChartArrange.Text");
      this.rpChartLocation.Text = rm.GetString("rpChartLocation.Text");
      this.rpChartMove.Text = rm.GetString("rpChartMove.Text");

      this.rpSparklineEditData.Text = rm.GetString("rpSparklineEditData.Text");
      this.rpSparklineType.Text = rm.GetString("rpSparklineType.Text");
      this.rpSparklineShow.Text = rm.GetString("rpSparklineShow.Text");
      this.rpSparklineStyle.Text = rm.GetString("rpSparklineStyle.Text");
      this.rpSparklineGroup.Text = rm.GetString("rpSparklineGroup.Text");

      this.RibbonButtonChartColumn.Text = rm.GetString("RibbonButtonChartColumn.Text");
      this.RibbonButtonChartLine.Text = rm.GetString("RibbonButtonChartLine.Text");
      this.RibbonButtonChartPie.Text = rm.GetString("RibbonButtonChartPie.Text");
      this.RibbonButtonChartBar.Text = rm.GetString("RibbonButtonChartBar.Text");
      this.RibbonButtonChartArea.Text = rm.GetString("RibbonButtonChartArea.Text");
      this.RibbonButtonChartScatter.Text = rm.GetString("RibbonButtonChartScatter.Text");
      this.RibbonButtonChartOthers.Text = rm.GetString("RibbonButtonChartOthers.Text");


      this.RibbonButton263.Text = rm.GetString("RibbonButton263.Text");
      this.RibbonButton10.Text = rm.GetString("RibbonButton10.Text");
      this.RibbonButton11.Text = rm.GetString("RibbonButton11.Text");
      this.RibbonButton12.Text = rm.GetString("RibbonButton12.Text");
      this.RibbonButton13.Text = rm.GetString("RibbonButton13.Text");
      this.RibbonButton14.Text = rm.GetString("RibbonButton14.Text");
      this.RibbonButton15.Text = rm.GetString("RibbonButton15.Text");
      this.RibbonButton50.Text = rm.GetString("RibbonButton50.Text");

      this.RibbonColorChooser1.Text = rm.GetString("RibbonColorChooser1.Text");
      this.RibbonColorChooser2.Text = rm.GetString("RibbonColorChooser2.Text");
      this.RibbonColorChooser3.Text = rm.GetString("RibbonColorChooser3.Text");
      this.RibbonButton110.Text = rm.GetString("RibbonButton110.Text");
      this.RibbonButton111.Text = rm.GetString("RibbonButton111.Text");
      this.RibbonButton137.Text = rm.GetString("RibbonButton137.Text");

      this.RibbonButton138.Text = rm.GetString("RibbonButton138.Text");
      this.RibbonButton139.Text = rm.GetString("RibbonButton139.Text");
      this.RibbonButton140.Text = rm.GetString("RibbonButton140.Text");
      this.RibbonButton141.Text = rm.GetString("RibbonButton141.Text");
      this.RibbonButton142.Text = rm.GetString("RibbonButton142.Text");

      this.RibbonButton197.Text = rm.GetString("RibbonButton197.Text");
      this.RibbonButton187.Text = rm.GetString("RibbonButton187.Text");

      this.RibbonButton180.Text = rm.GetString("RibbonButton180.Text");
      this.RibbonButton189.Text = rm.GetString("RibbonButton189.Text");
      this.RibbonButton188.Text = rm.GetString("RibbonButton188.Text");

      this.rbSparklineNegativePoints.Text = rm.GetString("rbSparklineNegativePoints.Text");
      this.rbSparklineMarkers.Text = rm.GetString("rbSparklineMarkers.Text");
      this.rbSparklineMarkerColorHighPoint.Text = rm.GetString("rbSparklineMarkerColorHighPoint.Text");
      this.rbSparklineMarkerColorLowPoint.Text = rm.GetString("rbSparklineMarkerColorLowPoint.Text");
      this.rbSparklineMarkerColorFirstPoint.Text = rm.GetString("rbSparklineMarkerColorFirstPoint.Text");
      this.rbSparklineMarkerColorLastPoint.Text = rm.GetString("rbSparklineMarkerColorLastPoint.Text");

      this.RibbonButton159.Text = rm.GetString("RibbonButton159.Text");
      this.RibbonButton160.Text = rm.GetString("RibbonButton160.Text");
      this.RibbonButton161.Text = rm.GetString("RibbonButton161.Text");
      this.RibbonButton162.Text = rm.GetString("RibbonButton162.Text");
      this.RibbonButton163.Text = rm.GetString("RibbonButton163.Text");
      this.RibbonButton164.Text = rm.GetString("RibbonButton164.Text");
      this.rbShapeThickNone.Text = rm.GetString("rbShapeThickNone.Text");
      this.rbShapeThickCustom.Text = rm.GetString("rbShapeThickCustom.Text");

      this.rbShapeOutlineSolid.Text = rm.GetString("rbShapeOutlineSolid.Text");
      this.rbShapeOutlineDash.Text = rm.GetString("rbShapeOutlineDash.Text");
      this.rbShapeOutlineDot.Text = rm.GetString("rbShapeOutlineDot.Text");
      this.rbShapeOutlineDashDot.Text = rm.GetString("rbShapeOutlineDashDot.Text");
      this.rbShapeOutlineDashDotDot.Text = rm.GetString("rbShapeOutlineDashDotDot.Text");

      this.rbShapeShadowNone.Text = rm.GetString("rbShapeShadowNone.Text");
      this.rbShapeShadowRight.Text = rm.GetString("rbShapeShadowRight.Text");
      this.rbShapeShadowBottomRight.Text = rm.GetString("rbShapeShadowBottomRight.Text");
      this.rbShapeShadowBottomLeft.Text = rm.GetString("rbShapeShadowBottomLeft.Text");
      this.rbShapeShadowBottom.Text = rm.GetString("rbShapeShadowBottom.Text");
      this.rbShapeShadowLeft.Text = rm.GetString("rbShapeShadowLeft.Text");
      this.rbShapeShadowTopLeft.Text = rm.GetString("rbShapeShadowTopLeft.Text");
      this.rbShapeShadowTop.Text = rm.GetString("rbShapeShadowTop.Text");
      this.rbShapeShadowTopRight.Text = rm.GetString("rbShapeShadowTopRight.Text");
      this.rbShapeShadowCustom.Text = rm.GetString("rbShapeShadowCustom.Text");

      this.rbChartMove.Text = rm.GetString("rbChartMoveHV.Text");
      this.rbChartSize.Text = rm.GetString("rbChartSizeBoth.Text");
      this.rbChartMoveHV.Text = rm.GetString("rbChartMoveHV.Text");
      this.rbChartMoveH.Text = rm.GetString("rbChartMoveH.Text");
      this.rbChartMoveV.Text = rm.GetString("rbChartMoveV.Text");
      this.rbChartMoveNone.Text = rm.GetString("rbChartMoveNone.Text");

      this.rbChartSizeBoth.Text = rm.GetString("rbChartSizeBoth.Text");
      this.rbChartSizeWidth.Text = rm.GetString("rbChartSizeWidth.Text");
      this.rbChartSizeHeight.Text = rm.GetString("rbChartSizeHeight.Text");
      this.rbChartSizeNone.Text = rm.GetString("rbChartSizeNone.Text");
    }

    private void InitializeUndoRedoEvent()
    {

      fpSpread1.UndoManager.ActionComplete += OnActionComplete;
      fpSpread1.UndoManager.UndoComplete += OnUndoComplete;
      fpSpread1.UndoManager.RedoComplete += OnRedoComplete;

    }

    private void RenameSheetInPlace(int sheetIndex)
    {
      System.Drawing.Rectangle rect = fpSpread1.GetSheetTabRectangle(sheetIndex);
      RenameTextBox.AutoSize = false;
      RenameTextBox.BorderStyle = BorderStyle.FixedSingle;
      RenameTextBox.Left = rect.Left + 5;
      RenameTextBox.Top = rect.Top + System.Windows.Forms.SystemInformation.CaptionHeight + System.Windows.Forms.SystemInformation.MenuHeight - 10;

      if (c1Ribbon1.Visible == true)
      {
        RenameTextBox.Top = RenameTextBox.Top + c1Ribbon1.Height;
      }

      if (FormulaPanel.Visible == true)
      {
        RenameTextBox.Top = RenameTextBox.Top + FormulaPanel.Height;
      }

      RenameTextBox.Top = RenameTextBox.Top - c1StatusBar1.Height - 30;

      RenameTextBox.Width = rect.Width;
      RenameTextBox.Height = rect.Height + 5;
      RenameTextBox.Text = fpSpread1.ActiveSheet.SheetName;
      RenameTextBox.SelectAll();
      RenameTextBox.Visible = true;
      RenameTextBox.BringToFront();
      RenameTextBox.Focus();
    }

    private void ShowCellTypeDialog(FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum cellType, IPluginCellTypeSetting pluginCellTypeSetting)
    {

      if ((fpSpread1.ActiveSheet != null))
      {
        if (((fpSpread1.ActiveSheet.ActiveCell != null)) | ((fpSpread1.ActiveSheet.GetSelections() != null) && fpSpread1.ActiveSheet.GetSelections().Length > 0))
        {
          if (fpSpread1.EditMode == true)
            fpSpread1.StopCellEditing();

          FarPoint.Win.Spread.Design.CellTypeDlgV3 ct = new FarPoint.Win.Spread.Design.CellTypeDlgV3(this.fpSpread1);


          //#If Not JAPAN Then  'InputManCellType
          if ((_pluginCellTypeSettings != null))
          {
            ct.PluginCellTypeSettings = _pluginCellTypeSettings;
          }
          if ((cellType == FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PluginCellType))
          {
            ct.PluginCellTypeSetting = pluginCellTypeSetting;
          }
          ct.celltype = cellType;
          FarPoint.Win.Spread.CellType.ICellType iCellType = MergeSelectedCellTypes();
          if (((iCellType != null)) & (cellType == FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PluginCellType))
          {
            object objPluginSetting = pluginCellTypeSetting;
            object objTmpPluginSetting = GetCellTypeSetting(iCellType);

            if ((objTmpPluginSetting != null))
            {
              if (objPluginSetting.GetType().FullName.Equals(objTmpPluginSetting.GetType().FullName))
              {
                ct.PluginCellTypeSetting.CellType = iCellType as BaseCellType;
                //ct.PluginCellTypeSetting.CellType = MergeSelectedCellTypes()
              }
            }

          }
          else if ((iCellType == null) & (cellType == FarPoint.Win.Spread.Design.DesignerMain.CellTypeEnum.PluginCellType))
          {
            string temName = pluginCellTypeSetting.GetProperty("DisplayName").ToString();
            _pluginCellTypeSettings = null;
            LoadInputManPluginCellType();
            int i = 0;
            for (i = 0; i <= _pluginCellTypeSettings.Count - 1; i++)
            {
              string displayName = ((IPluginCellTypeSetting)_pluginCellTypeSettings[i]).GetProperty("DisplayName").ToString();
              if ((displayName.IndexOf(temName) >= 0))
              {
                ct.PluginCellTypeSetting = (IPluginCellTypeSetting)_pluginCellTypeSettings[i];
                break; // TODO: might not be correct. Was : Exit For
              }
            }
          }


          //      ct.InitValues(SpreadWrapper1.ActiveSheet.ActiveCell.CellType, SpreadWrapper1.ActiveSheet.ActiveCell.Value)
          //>>Eric bug #99902988 20081029 :avoid execption when select row/column but columncount/rowcount is zero
          if ((fpSpread1.ActiveSheet.ActiveCell != null))
          {
            ct.InitValues(MergeSelectedCellTypes(), fpSpread1.ActiveSheet.ActiveCell.Value);
          }
          else
          {
            ct.InitValues(MergeSelectedCellTypes(), null);
          }
          //<< bug #99902988 
          if (ct.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
          {

          }
          return;
        }
      }
      // fall thru to error condition
      MessageBox.Show(rm.GetString("NoSelectedItemError"), rm.GetString("DesignerErrorTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning);
    }

    public FarPoint.Win.Spread.CellType.ICellType MergeSelectedCellTypes()
    {

      FarPoint.Win.Spread.Model.CellRange CellRange = default(FarPoint.Win.Spread.Model.CellRange);
      int SelectionCount = 0;
      int i = 0;
      int ColumnIndex = 0;
      int RowIndex = 0;
      FarPoint.Win.Spread.CellType.ICellType ct = null;
      FarPoint.Win.Spread.CellType.ICellType tmpCt = null;

      // Query the selection count in the spreadsheet and make sure there is at least one selection
      SelectionCount = fpSpread1.ActiveSheet.SelectionCount;
      if ((SelectionCount > 0))
      {
        // Loop through all selections
        for (i = 0; i <= SelectionCount - 1; i++)
        {
          CellRange = fpSpread1.ActiveSheet.GetSelection(i);
          //CellRange = fpSpread1.GetCorrectCellRange(CellRange);
          // Entire spread selection
          if ((CellRange.Column == -1 && CellRange.Row == -1))
          {
            ct = fpSpread1.ActiveSheet.DefaultStyle.CellType;
            // Row(s) selected
          }
          else if ((CellRange.Column == -1))
          {
            for (RowIndex = 0; RowIndex <= CellRange.RowCount - 1; RowIndex++)
            {
              tmpCt = fpSpread1.ActiveSheet.Rows[RowIndex + CellRange.Row].CellType;
              if ((ct == null))
              {
                ct = tmpCt;
              }
              else if ((tmpCt == null))
              {
                return null;
              }
              else
              {
                if ((!object.ReferenceEquals(ct, tmpCt)))
                {
                  return null;
                }
              }
            }
            // Column(s) selected
          }
          else if ((CellRange.Row == -1))
          {
            for (ColumnIndex = 0; ColumnIndex <= CellRange.ColumnCount - 1; ColumnIndex++)
            {
              tmpCt = fpSpread1.ActiveSheet.Columns[ColumnIndex + CellRange.Column].CellType;
              if ((ct == null))
              {
                ct = tmpCt;
              }
              else if ((tmpCt == null))
              {
                return null;
              }
              else
              {
                if ((!object.ReferenceEquals(ct, tmpCt)))
                {
                  return null;
                }
              }
            }
            // Cell(s) selected
          }
          else
          {
            // Loop through all rows
            for (RowIndex = 0; RowIndex <= CellRange.RowCount - 1; RowIndex++)
            {
              // Loop through all columns in each row
              for (ColumnIndex = 0; ColumnIndex <= CellRange.ColumnCount - 1; ColumnIndex++)
              {
                tmpCt = fpSpread1.ActiveSheet.Cells[RowIndex + CellRange.Row, ColumnIndex + CellRange.Column].CellType;
                if ((ct == null))
                {
                  ct = tmpCt;
                }
                else if ((tmpCt == null))
                {
                  return null;
                }
                else
                {
                  if ((!object.ReferenceEquals(ct, tmpCt)))
                  {
                    return null;
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        if ((ct == null))
        {
          ct = fpSpread1.ActiveSheet.Cells[fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex].CellType;
        }
      }
      return ct;
    }

    private IPluginCellTypeSetting GetCellTypeSetting(object cellType)
    {
      if (_pluginCellTypeSettings == null)
      {
        LoadInputManPluginCellType();
      }

      if (_pluginCellTypeSettings != null)
      {
        for (int i = 0; i < _pluginCellTypeSettings.Count; i++)
        {
          Object objCellType = (_pluginCellTypeSettings[i] as IPluginCellTypeSetting).CellType;
          System.Type ojbType = objCellType.GetType();
          if (ojbType.FullName.Equals(cellType.GetType().FullName))
            return _pluginCellTypeSettings[i] as IPluginCellTypeSetting;

        }
      }

      return null;
    }


    public FarPoint.Win.Spread.Model.CellRange GetCorrectCellRange(FarPoint.Win.Spread.Model.CellRange oldCellRange)
    {
      if ((oldCellRange == null))
      {
        return null;
      }

      FarPoint.Win.Spread.Model.CellRange newCellRange = null;

      int rowIndex = oldCellRange.Row;
      int columnIndex = oldCellRange.Column;
      int rowCount = oldCellRange.RowCount;
      int columnCount = oldCellRange.ColumnCount;

      if ((rowIndex == -1 | columnIndex == -1))
      {
        return oldCellRange;
      }

      if ((rowIndex >= fpSpread1.ActiveSheet.RowCount | columnIndex >= fpSpread1.ActiveSheet.ColumnCount))
      {
        return oldCellRange;
      }


      int rowSpan = fpSpread1.ActiveSheet.Cells[rowIndex, columnIndex].RowSpan;
      int columnSpan = fpSpread1.ActiveSheet.Cells[rowIndex, columnIndex].ColumnSpan;

      if ((rowSpan == 1 & columnSpan == 1))
      {
        return oldCellRange;
      }

      if ((rowCount <= rowSpan & columnCount <= columnSpan))
      {
        newCellRange = new FarPoint.Win.Spread.Model.CellRange(rowIndex, columnIndex, 1, 1);
      }
      else
      {
        newCellRange = oldCellRange;
      }

      return newCellRange;

    }

    public FarPoint.Win.Spread.Model.CellRange[] GetCorrectCellRanges(FarPoint.Win.Spread.Model.CellRange[] oldCellRange)
    {
      if ((oldCellRange == null))
      {
        return null;
      }

      int i = 0;
      FarPoint.Win.Spread.Model.CellRange cellrange = default(FarPoint.Win.Spread.Model.CellRange);
      System.Collections.Generic.List<FarPoint.Win.Spread.Model.CellRange> newCellRange = new System.Collections.Generic.List<FarPoint.Win.Spread.Model.CellRange>();

      for (i = 0; i <= oldCellRange.Length - 1; i++)
      {
        cellrange = GetCorrectCellRange(oldCellRange[i]);
        newCellRange.Add(cellrange);
      }

      return newCellRange.ToArray();
    }

    private void LoadInputManPluginCellType()
    {
      try
      {
        int i = 0;
        int j = 0;
        int t = 0;
        if ((_pluginAssemblyNames == null))
        {
          _pluginAssemblyNames = new ArrayList();
          _pluginAssemblyNames.Add("GrapeCity.Win.PluginInputMan");
          try
          {
            System.Configuration.ConfigXmlDocument cXmlDocument = new System.Configuration.ConfigXmlDocument();
            string path = Application.StartupPath;
            cXmlDocument.Load(path + "/FarPoint.Win.Spread.Design.dll.config");
            System.Xml.XmlNode AsemblyPluginNode = cXmlDocument.DocumentElement.SelectSingleNode("/configuration/Plugins");
            if (((AsemblyPluginNode != null)) & (AsemblyPluginNode.HasChildNodes))
            {
              for (i = 0; i <= AsemblyPluginNode.ChildNodes.Count - 1; i++)
              {
                System.Xml.XmlNode childNode = AsemblyPluginNode.ChildNodes[i];
                if ((childNode != null))
                {
                  System.Xml.XmlAttribute assemblyNameAttribute = childNode.Attributes[ASSEMBLY_NAME_ATTRIBUTE];
                  string assemblyName = assemblyNameAttribute.Value;
                  _pluginAssemblyNames.Add(assemblyName);
                }
              }
            }
          }
          catch (Exception ex) { }
        }
        if ((_pluginAssemblyNames == null))
        {
          return;
        }
        bool hasInputManAssembly = false;
        for (i = 0; i <= _pluginAssemblyNames.Count - 1; i++)
        {
          string assemblyName = _pluginAssemblyNames[i].ToString();

          System.Reflection.Assembly fAssembly = null;
          try
          {
            //fAssembly = System.Reflection.Assembly.Load(assemblyName);
            fAssembly = System.Reflection.Assembly.LoadFile(Application.StartupPath + "\\GrapeCity.Win.PluginInputMan.dll");
          }
          catch (Exception ex)
          {
            continue;
          }

          System.Type[] types = fAssembly.GetExportedTypes();
          for (j = 0; j <= types.Length - 1; j++)
          {
            System.Type type = types[j];
            System.Type[] interfaceTypes = type.GetInterfaces();
            for (t = 0; t <= interfaceTypes.Length - 1; t++)
            {
              System.Type interfaceType = interfaceTypes[t];

              if ((interfaceType != null & interfaceType.FullName != null))
              {
                if ((interfaceType.FullName.IndexOf(IPLUGIN_INTERFACE) >= 0))
                {
                  try
                  {
                    FarPoint.Win.Spread.CellType.IPluginCellTypeSetting cellType = (FarPoint.Win.Spread.CellType.IPluginCellTypeSetting)Activator.CreateInstance(assemblyName, type.FullName).Unwrap();
                    if ((_pluginCellTypeSettings == null))
                    {
                      _pluginCellTypeSettings = new ArrayList();
                    }
                    _pluginCellTypeSettings.Add(cellType);
                    hasInputManAssembly = true;

                  }
                  catch (Exception ex)
                  {
                  }
                }
              }
            }
          }
        }
        if (!hasInputManAssembly)
        {
          menuContext_GcTextBox.Visible = false;
          menuContext_GcDate.Visible = false;
        }
      }
      catch (Exception ex)
      {
      }
    }

    private TableView cachedTableView = null;
    private void LoadTableRibbon()
    {
      //Create table button state
      CellRange cr = fpSpread1.ActiveSheet.GetSelection(0);
      if (cr != null && cr.Row >= 0 && cr.Column >= 0 && cr.RowCount > 0 && cr.ColumnCount > 0)
      {
        ITableModelSupport model = fpSpread1.ActiveSheet.Models.Data as ITableModelSupport;
        List<ITableRange> tables = model.TableRangeManager.Find(cr.Row, cr.Column, cr.RowCount, cr.ColumnCount);
        rbTable.Enabled = tables == null || tables.Count == 0;
      }

      //Table ribbon tab
      TableView table = fpSpread1.ActiveSheet.GetTable(fpSpread1.ActiveSheet.ActiveRowIndex, fpSpread1.ActiveSheet.ActiveColumnIndex);
      if (table == cachedTableView)
        return;
      cachedTableView = table;
      if (table != null)
      {
        if (!rtTable.Visible)
        {
          rtTable.Visible = true;
          if (c1Ribbon1.SelectedTab != rtTable)
            c1Ribbon1.SelectedTab = rtTable;
        }
        rtbTableName.Text = table.Name;
        rcTableHeaderRow.Checked = table.HeaderRowVisible;
        rcTableTotalRow.Checked = table.TotalRowVisible;
        rcTableBandedRow.Checked = table.BandedRows;
        rcTableFirstColumn.Checked = table.FirstColumn;
        rcTableLastColumn.Checked = table.LastColumn;
        rcTableBandedColumn.Checked = table.BandedColumns;
        if (table.HeaderRowVisible)
        {
          rcTableFilterButton.Enabled = true;
        rcTableFilterButton.Checked = table.FilterButtonVisible;
        }          
        else
        {
          rcTableFilterButton.Checked = false;
          rcTableFilterButton.Enabled = false;
        }

        UpdateTableStylesRibbonGallery();
      }
      else if (table == null)
      {
        if (rtTable.Visible)
        {
          rtTable.Visible = false;
          if (c1Ribbon1.SelectedTab == rtTable)
            c1Ribbon1.SelectedTab = rtHome;
        }
      }
    }

    [Flags]
    private enum RibbonTableStyles
    {
      HeaderRow = 1,
      TotalRow = 2,
      BandedRow = 4,
      FirstColumn = 8,
      LastColumn = 16,
      BandedColumn = 32
    }

    Hashtable cachedTableStyleImage = new Hashtable();
    private void UpdateTableStylesRibbonGallery()
    {
      if (cachedTableView == null)
        return;
      RibbonTableStyles styleImageFlag = 0;
      if (cachedTableView.HeaderRowVisible)
        styleImageFlag |= RibbonTableStyles.HeaderRow;
      if (cachedTableView.TotalRowVisible)
        styleImageFlag |= RibbonTableStyles.TotalRow;
      if (cachedTableView.BandedRows)
        styleImageFlag |= RibbonTableStyles.BandedRow;
      if (cachedTableView.FirstColumn)
        styleImageFlag |= RibbonTableStyles.FirstColumn;
      if (cachedTableView.LastColumn)
        styleImageFlag |= RibbonTableStyles.LastColumn;
      if (cachedTableView.BandedColumns)
        styleImageFlag |= RibbonTableStyles.BandedColumn;

      if (!cachedTableStyleImage.ContainsKey(styleImageFlag))
      {
        FpSpread spreadTemp = new FpSpread();
        spreadTemp.SuspendLayout();
        spreadTemp.AccessibleDescription = "";
        spreadTemp.Location = new System.Drawing.Point(13, 13);
        spreadTemp.Name = "fpSpread1";
        spreadTemp.Size = new System.Drawing.Size(361, 143);
        SheetView sheetTemp = new SheetView("TableSheet");
        //sheetTemp.Reset()
        sheetTemp.SheetName = "Sheet1";
        sheetTemp.ColumnCount = 5;
        sheetTemp.RowCount = 5;
        spreadTemp.Sheets.Add(sheetTemp);
        TableView table = new TableView("SampleTable", TableStyle.None, 0, 0, 5, 5, cachedTableView.HeaderRowVisible, cachedTableView.TotalRowVisible);
        spreadTemp.ActiveSheet.AddTable(table);
        table.FirstColumn = cachedTableView.FirstColumn;
        table.LastColumn = cachedTableView.LastColumn;
        table.BandedRows = cachedTableView.BandedRows;
        table.BandedColumns = cachedTableView.BandedColumns;

        Hashtable images = new Hashtable();
        Rectangle cRect = new Rectangle(0, 0, 13, 10);
        for (int i = 0; i < fpSpread1.TableStyleCollection.BuiltInTableStyleName.Count; i++)
        {
          string styleName = fpSpread1.TableStyleCollection.BuiltInTableStyleName[i];
          if (styleName.Length > 0 && !styleName.Contains("TableStyleMedium"))
            continue;
          table.StyleName = styleName;
          Bitmap image = new Bitmap(61, 46);
          using (Graphics g = Graphics.FromImage(image))
          {
            Brush br = null;
            Rectangle currentRect = cRect;
            for (int r = 0; r < 5; r++)
            {
              currentRect.X = 0;
              currentRect.Y = cRect.Height * r - r;
              for (int c = 0; c < 5; c++)
              {
                currentRect.X = currentRect.Width * c - c;
                StyleInfo styleCell = sheetTemp.GetStyleInfo(r, c);
                br = new SolidBrush(styleCell.BackColor);
                g.FillRectangle(br, currentRect);
                br.Dispose();
                Color foreColor = styleCell.ForeColor.IsEmpty ? Color.Black : styleCell.ForeColor;
                br = new SolidBrush(foreColor);
                g.FillRectangle(br, currentRect.X + 3, currentRect.Y + currentRect.Height / 2, currentRect.Width - 6, 1);
                br.Dispose();
                if ((styleCell.Border != null))
                {
                  ComplexBorder border = styleCell.Border as ComplexBorder;
                  if (border.Inset.Top == 1)
                  {
                    br = new SolidBrush(border.Top.Color);
                    g.FillRectangle(br, currentRect.X, currentRect.Y, currentRect.Width, 1);
                    br.Dispose();
                  }
                  if (border.Inset.Bottom == 1)
                  {
                    br = new SolidBrush(border.Bottom.Color);
                    g.FillRectangle(br, currentRect.X, currentRect.Bottom - 1, currentRect.Width, 1);
                    br.Dispose();
                  }
                  if (border.Inset.Left == 1)
                  {
                    br = new SolidBrush(border.Left.Color);
                    g.FillRectangle(br, currentRect.X, currentRect.Y, 1, currentRect.Height);
                    br.Dispose();
                  }
                  if (border.Inset.Right == 1)
                  {
                    br = new SolidBrush(border.Right.Color);
                    g.FillRectangle(br, currentRect.Right - 1, currentRect.Y, 1, currentRect.Height);
                    br.Dispose();
                  }
                }
              }
            }
          }
          images.Add(styleName, image);
        }
        spreadTemp.Dispose();
        cachedTableStyleImage.Add(styleImageFlag, images);
      }
      Hashtable cachedImages = cachedTableStyleImage[styleImageFlag] as Hashtable;
      rglTableStyles.Items.Clear();
      foreach (string stName in cachedImages.Keys)
      {
        RibbonGalleryItem item = new RibbonGalleryItem(stName, cachedImages[stName] as Image);
        item.VerticalLayout = false;
        rglTableStyles.Items.Add(item);
        if (stName == cachedTableView.StyleName)
          rglTableStyles.SelectedIndex = rglTableStyles.Items.Count - 1;
      }
    }
    #endregion

    #region Cound Function
    private void ribbonButton1List_Click(object sender, EventArgs e)
    {
        CloudView frm = new CloudView(this);
        frm.Show(this);
    }

    public void ExcelOpenFile(string fileName)
    {
        this.FileOpen(fileName);
    }

    private void ribbonButton2SaveCloud_Click(object sender, EventArgs e)
    {
        if (this.FileSave(false) && !string.IsNullOrEmpty(glbFileName))
        {
            bool ret = QiniuWrapLib.mSingle.Upload(glbFileName);
            if (ret)
            {
                MessageBox.Show("云端存储文件成功");
            }
            else
            {
                MessageBox.Show("云端存储文件失败");
            }
        }
        else
        {
            MessageBox.Show("保存文件失败");
        }
    }

    #endregion

  }
}
