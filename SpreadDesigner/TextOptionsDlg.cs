﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SpreadDesigner
{
  public partial class TextOptionsDlg : Form
  {

    public TextOptionsDlg()
    {
      base.Load += new EventHandler(this.TextOptionsDlg_Load);
      base.KeyDown += new KeyEventHandler(this.TextOptionsDlg_KeyDown);
      this.colDefault = "";
      this.rowDefault = "";
      this.cellDefault = "";
      this.InitializeComponent();
    }
    
    private void Apply_Click(object sender, EventArgs e)
    {
      string colDefault = "";
      if (this.SpecialChars.SelectedIndex == 0)
      {
        if (((Button)sender).Tag.Equals("Column"))
        {
          colDefault = this.colDefault;
        }
        else if (((Button)sender).Tag.Equals("Row"))
        {
          colDefault = this.rowDefault;
        }
        else if (((Button)sender).Tag.Equals("Cell"))
        {
          colDefault = this.cellDefault;
        }
        else
        {
          colDefault = null;
        }
      }
      else if (this.SpecialChars.SelectedIndex == 1)
      {
        colDefault = "<tab>";
      }
      else if (this.SpecialChars.SelectedIndex == 2)
      {
        colDefault = "<cr>";
      }
      else if (this.SpecialChars.SelectedIndex == 3)
      {
        colDefault = "<lf>";
      }
      else if (this.SpecialChars.SelectedIndex == 4)
      {
        colDefault = "<cr><lf>";
      }
      if (((Button)sender).Tag.Equals("Column"))
      {
        this.ColumnDelim.Text = colDefault;
      }
      else if (((Button)sender).Tag.Equals("Row"))
      {
        this.RowDelim.Text = colDefault;
      }
      else if (((Button)sender).Tag.Equals("Cell"))
      {
        this.CellDelim.Text = colDefault;
      }
    }
    
    private void TextOptionsCancelBtn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    private void TextOptionsDlg_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.F1)
      {
        e.Handled = true;
        common.DisplayHelp(this, "SDCustomFileOptions.html");
      }
    }

    private void TextOptionsDlg_Load(object sender, EventArgs e)
    {
      if (common.rm != null)
      {
        this.Text = common.rm.GetString("TextOptionsDlg");
        this.TextOptionsLabel1.Text = common.rm.GetString("TextOptionsLabel1");
        this.TextOptionsLabel2.Text = common.rm.GetString("TextOptionsLabel2");
        this.TextOptionsLabel3.Text = common.rm.GetString("TextOptionsLabel3");
        this.TextOptionsGroupBox1.Text = common.rm.GetString("TextOptionsGroupBox1");
        this.RowApply.Text = common.rm.GetString("RowApply");
        this.ColumnApply.Text = common.rm.GetString("ColumnApply");
        this.CellApply.Text = common.rm.GetString("CellApply");
        this.TextOptionsGroupBox2.Text = common.rm.GetString("TextOptionsGroupBox2");
        this.IncludeRowHdr.Text = common.rm.GetString("IncludeRowHdr");
        this.IncludeColumnHdr.Text = common.rm.GetString("IncludeColumnHdr");
        this.TextOptionsOKBtn.Text = common.rm.GetString("TextOptionsOKBtn");
        this.TextOptionsCancelBtn.Text = common.rm.GetString("TextOptionsCancelBtn");
        this.TextOptionsFormatted.Text = common.rm.GetString("TextOptionsFormatted");
        this.SpecialChars.Items.Clear();
        string item = common.rm.GetString("SpecialChars1");
        this.SpecialChars.Items.Add(item);
        item = common.rm.GetString("SpecialChars2");
        this.SpecialChars.Items.Add(item);
        item = common.rm.GetString("SpecialChars3");
        this.SpecialChars.Items.Add(item);
        item = common.rm.GetString("SpecialChars4");
        this.SpecialChars.Items.Add(item);
        item = common.rm.GetString("SpecialChars5");
        this.SpecialChars.Items.Add(item);
      }
      this.SpecialChars.SelectedIndex = 0;
      this.colDefault = this.ColumnDelim.Text;
      this.rowDefault = this.RowDelim.Text;
      this.cellDefault = this.CellDelim.Text;
      if (this.Owner != null)
      {
        this.Icon = this.Owner.Icon;
      }
    }

    private void TextOptionsOKBtn_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.OK;
      this.Close();
    }
  }
}
