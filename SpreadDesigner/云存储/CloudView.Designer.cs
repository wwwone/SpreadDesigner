﻿namespace SpreadDesigner
{
    partial class CloudView
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1List = new System.Windows.Forms.Button();
            this.button2Download = new System.Windows.Forms.Button();
            this.button3Delete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(592, 332);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "文件名";
            this.columnHeader1.Width = 283;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "mimeType";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 130;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "最后更新时间";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 92;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "文件大小";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1List
            // 
            this.button1List.Location = new System.Drawing.Point(308, 357);
            this.button1List.Name = "button1List";
            this.button1List.Size = new System.Drawing.Size(75, 23);
            this.button1List.TabIndex = 1;
            this.button1List.Text = "List";
            this.button1List.UseVisualStyleBackColor = true;
            this.button1List.Click += new System.EventHandler(this.button1List_Click);
            // 
            // button2Download
            // 
            this.button2Download.Location = new System.Drawing.Point(404, 357);
            this.button2Download.Name = "button2Download";
            this.button2Download.Size = new System.Drawing.Size(75, 23);
            this.button2Download.TabIndex = 1;
            this.button2Download.Text = "Download";
            this.button2Download.UseVisualStyleBackColor = true;
            this.button2Download.Click += new System.EventHandler(this.button2Download_Click);
            // 
            // button3Delete
            // 
            this.button3Delete.Location = new System.Drawing.Point(503, 357);
            this.button3Delete.Name = "button3Delete";
            this.button3Delete.Size = new System.Drawing.Size(75, 23);
            this.button3Delete.TabIndex = 1;
            this.button3Delete.Text = "Delete";
            this.button3Delete.UseVisualStyleBackColor = true;
            this.button3Delete.Click += new System.EventHandler(this.button3Delete_Click);
            // 
            // CloudView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 392);
            this.Controls.Add(this.button3Delete);
            this.Controls.Add(this.button2Download);
            this.Controls.Add(this.button1List);
            this.Controls.Add(this.listView1);
            this.Name = "CloudView";
            this.Text = "远端管理视图";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1List;
        private System.Windows.Forms.Button button2Download;
        private System.Windows.Forms.Button button3Delete;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader1;
    }
}

